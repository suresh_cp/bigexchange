package com.fnz.bigexchange.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fnz.bigexchange.domains.AccountInstruction;
import com.fnz.bigexchange.domains.AccountRoleInstruction;
import com.fnz.bigexchange.domains.AccountTypeIndividualCustomerRoleTypes;
import com.fnz.bigexchange.domains.CustomerRoleInstruction;
import com.fnz.bigexchange.domains.CustomerRoleTypeContract;
import com.fnz.bigexchange.enums.AccountType;
import com.fnz.bigexchange.service.BERestTemplateService;
import com.fnz.bigexchange.service.MappingHelper;
import com.fnz.bigexchange.service.SessionService;
import com.fnz.bigexchange.service.SessionServiceImpl;

@RestController
@RequestMapping("/api")
public class BERegistrationController {

	private static Logger logger = LoggerFactory.getLogger(BERegistrationController.class);

	@Autowired
	private BERestTemplateService beService;
	@Autowired
	private SessionServiceImpl sessionService;
	public MappingHelper mappingHelper = new MappingHelper();

	public BERegistrationController() {
		super();
	}

	/** once account is selected */
	@GetMapping("/registration/apply/registrationInstructions")
	public String getRegistrationInstructions() {
		{

			final String token = sessionService.randomString(63);
			logger.info(token);
			AccountInstruction account = beService.genericPost("/accountInstructions", mappingHelper.createAccount(),
					AccountInstruction.class);

			sessionService.setSessionValue(token, SessionService.ADVISER_HIERARCHY_ID, account.getParentHierarchyId());
			logger.info(sessionService.getSessionValue(token, SessionService.ADVISER_HIERARCHY_ID));

			sessionService.setSessionValue(token, SessionService.ACCOUNT_ID, account.getHierarchyId());

			String customerId = beService.genericPostForString("/customerInstructions", mappingHelper.createCustomer(),
					String.class);
			logger.info(customerId);
			sessionService.setSessionValue(token, SessionService.CUSTOMER_ID, customerId);

			AccountRoleInstruction accountRole = beService.genericPost("/accountRoleInstructions",
					mappingHelper.createAccountRole(account.getHierarchyId()), AccountRoleInstruction.class);

			sessionService.setSessionValue(token, SessionService.ACCOUNT_ROLE_ID, accountRole.getId().toString());

			String type = AccountType.Individual.toString();
			AccountTypeIndividualCustomerRoleTypes customerRoleTypes = beService.genericGet(
					"/accountTypes/" + type + "/individualCustomerRoleTypes",
					AccountTypeIndividualCustomerRoleTypes.class);
			List<CustomerRoleTypeContract> customerRoletype = customerRoleTypes.getResults();
			Long customerRoleTypeValue = 0L;
			for (CustomerRoleTypeContract crt : customerRoletype) {
				if (crt.getName().equals(type)) {
					customerRoleTypeValue = crt.getId();
				}
			}
			System.out.println(sessionService.getSessionValue(token, SessionService.ACCOUNT_ROLE_ID));

			CustomerRoleInstruction customerRole = beService.genericPost(
					"/accountRoleInstructions/" + sessionService.getSessionValue(token, SessionService.ACCOUNT_ROLE_ID)
							+ "/customerRoles",
					mappingHelper.createCustomerRole(customerId, customerRoleTypeValue), CustomerRoleInstruction.class);

			sessionService.setSessionValue(token, SessionService.CUSTOMER_ROLE_ID,
					customerRole.getCustomerHierarchyId());

			String subAccount = beService.genericPostForString("/subAccountInstructions",
					mappingHelper.createSubAccount(account.getHierarchyId()), String.class);

			sessionService.setSessionValue(token, SessionService.SUBACCOUNT_ID, subAccount);

			// D2CRegistrationInstruction register = mappingHelper.registerAccount(account,
			// customerId);

			return token;
		}
	}
}
