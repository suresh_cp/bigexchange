package com.fnz.bigexchange.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fnz.bigexchange.BEExceptionHandler.BEInvalidTokenException;
import com.fnz.bigexchange.domains.BuyInstruction;
import com.fnz.bigexchange.domains.BuyInstructions;
import com.fnz.bigexchange.domains.Fund;
import com.fnz.bigexchange.domains.Funds;
import com.fnz.bigexchange.domains.ProductDocuments;
import com.fnz.bigexchange.domains.ProductWrapperAllowance;
import com.fnz.bigexchange.domains.ProductWrapperContributionLimits;
import com.fnz.bigexchange.domains.RegularBuyInstruction;
import com.fnz.bigexchange.domains.RegularBuyInstructionCollection;
import com.fnz.bigexchange.enums.DividendReinvestment;
import com.fnz.bigexchange.enums.HistoricalPerformancePeriod;
import com.fnz.bigexchange.enums.ObsrRating;
import com.fnz.bigexchange.enums.WrapperType;
import com.fnz.bigexchange.service.BERestTemplateServiceImp;
import com.fnz.bigexchange.service.MappingHelper;
import com.fnz.bigexchange.service.SessionService;

@RestController
@RequestMapping("/api")
public class BEFundDetailsController {
	private static Logger logger = LoggerFactory.getLogger(BEFundDetailsController.class);

	@Autowired
	private BERestTemplateServiceImp beService;
	@Autowired
	private SessionService sessionService;

	public MappingHelper mappingHelper = new MappingHelper();

	public BEFundDetailsController() {
		super();
	}

	@GetMapping("/subAccountInstruction/buyInstruction/get")
	public BuyInstructions getBuys(@RequestHeader(value = "token", required = false) String token) {

		if (sessionService.isTokenValid(token)) {
			BuyInstructions buysCollection = beService.genericGet("/instructions/buys?hierarchyId="
					+ sessionService.getSessionValue(token, SessionService.SUBACCOUNT_ID)
					+ "&start=-2147483646&count=2147483647", BuyInstructions.class);
			logger.info(buysCollection.toString());
			return buysCollection;
		}
		throw new BEInvalidTokenException(SessionService.invalidToken);
	}

	@GetMapping("/subAccountInstruction/regularBuyInstruction/get")
	public RegularBuyInstructionCollection getRegularBuys(
			@RequestHeader(value = "token", required = false) String token) {
		if (sessionService.isTokenValid(token)) {
			RegularBuyInstructionCollection regularBuycollection = beService
					.genericGet("/instructions/regularBuys?hierarchyId="
							+ sessionService.getSessionValue(token, SessionService.SUBACCOUNT_ID)
							+ "&start=-2147483646&count=2147483647", RegularBuyInstructionCollection.class);
			logger.info(regularBuycollection.toString());
			return regularBuycollection;
		}
		throw new BEInvalidTokenException(SessionService.invalidToken);
	}

	@GetMapping("/contributionLimit/contributionLimits")
	public ProductWrapperContributionLimits getContributionLimits(
			@RequestHeader(value = "token", required = false) String token) {
		if (sessionService.isTokenValid(token)) {
			ProductWrapperContributionLimits productWrapperContributionLimits = beService.genericGet("/accounts/"
					+ sessionService.getSessionValue(token, SessionService.ACCOUNT_ID) + "/productwrappers/"
					+ sessionService.getSessionValue(token, SessionService.SUBACCOUNT_ID) + "/contributionlimits",
					ProductWrapperContributionLimits.class);
			logger.info(productWrapperContributionLimits.toString());
			return productWrapperContributionLimits;
		}
		throw new BEInvalidTokenException("invalidToken");
	}

	@GetMapping("/allowance/productWrapperAllowance")
	public ProductWrapperAllowance getProductWrapperAllowance(
			@RequestHeader(value = "token", required = false) String token) {
		if (sessionService.isTokenValid(token)) {
			ProductWrapperAllowance productWrapperAllowance = beService.genericGet("/productwrapper/"
					+ sessionService.getSessionValue(token, SessionService.SUBACCOUNT_ID) + "/allowance/Current",
					ProductWrapperAllowance.class);
			logger.info(productWrapperAllowance.toString());
			return productWrapperAllowance;
		}
		throw new BEInvalidTokenException(SessionService.invalidToken);
	}

	// returning funds for now but need to confirm as productid's are unknown
	@GetMapping("/instrumentSearch/instrumentSelection")
	public Funds getInstrumentSelection(@RequestHeader(value = "token", required = false) String token) {
		if (sessionService.isTokenValid(token)) {
			String productWrapperType = WrapperType.StocksIsa.toString();
			boolean buyable = true;
			String obsrRating = ObsrRating.NotSpecified.toString();
			String dividendReinvestment = DividendReinvestment.NotSpecified.toString();
			String historicalPerformancePeriod = HistoricalPerformancePeriod.NotSpecified.toString();
			Long start = 1L;
			Long count = 200L;
			Funds funds = beService.genericGet("/funds?accountId="
					+ sessionService.getSessionValue(token, SessionService.ADVISER_HIERARCHY_ID)
					+ "&productwrappertype=" + productWrapperType + "&buyable=" + buyable + "&obsrrating=" + obsrRating
					+ "&dividendreinvestment=" + dividendReinvestment + "&historicalperformanceperiod="
					+ historicalPerformancePeriod + "&start=" + start + "&count=" + count, Funds.class);
			logger.info(funds.toString());
			List<Fund> fundResults = funds.getPageOfResults();
			List<Long> productIds = new ArrayList<>();
			for (Fund f : fundResults) {
				productIds.add(f.getProductId());
			}
			ProductDocuments productDocuments = beService.genericGet(
					"/products/documents?productids=" + productIds.toString().replace("[", "").replace("]", ""),
					ProductDocuments.class);
			logger.info(productDocuments.toString());
			// mappingHelper.instrumentSelection(funds ,productDocuments);

			return funds;
		}
		throw new BEInvalidTokenException(SessionService.invalidToken);
	}

	@PostMapping("/subAccountInstruction/buyInstruction/post")
	public String postBuys(@RequestBody BuyInstruction buys,
			@RequestHeader(value = "token", required = false) String token) {
		if (sessionService.isTokenValid(token)) {
			System.out.println("post");
			System.out.println(buys);
			String productId = beService.genericPostForString("/instructions/buys", buys, String.class);
			return productId; // 567
		}
		throw new BEInvalidTokenException(SessionService.invalidToken);
	}

	@PostMapping("subAccountInstruction/regularBuyInstruction/post")
	public String postRegularBuys(@RequestBody RegularBuyInstruction regularBuys,
			@RequestHeader(value = "token", required = false) String token) {
		if (sessionService.isTokenValid(token)) {
			String productId = beService.genericPostForString("/instructions/regularbuys", regularBuys, String.class);
			return productId;
		}
		throw new BEInvalidTokenException(SessionService.invalidToken);
	}
}
