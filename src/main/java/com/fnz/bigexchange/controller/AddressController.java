package com.fnz.bigexchange.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fnz.bigexchange.BEExceptionHandler.BEAddressException;
import com.fnz.bigexchange.BEstubDomains.AddressPopulated;
import com.fnz.bigexchange.BEstubDomains.ApiAddress;
import com.fnz.bigexchange.BEstubDomains.ApiAddresses;
import com.fnz.bigexchange.BEstubDomains.SpecifcAddresses;
import com.fnz.bigexchange.domains.Nationality;
import com.fnz.bigexchange.service.BERestTemplateServiceImp;

@RestController
@RequestMapping("/api")
public class AddressController {

	@Autowired
	private BERestTemplateServiceImp beService;

	public AddressController() {
		super();
	}

	@Value("${address_key}")
	private String key;
	@Value("${findAddress}")
	private String findAddress;
	@Value("${retrieveAddress}")
	private String retrieveAddress;

	@GetMapping("/address/get")
	public ApiAddresses getAddress(@RequestParam String text) throws BEAddressException {
		ApiAddresses postalAddress = beService.addressGet(findAddress + key + "&text=" + text, ApiAddresses.class);
		if (postalAddress.getItems().size() > 1) {
			throw new BEAddressException(" enter proper postal code");
		} else {
			ApiAddresses postalAddresses = beService.addressGet(
					findAddress + key + "&text=" + text + "&container=" + postalAddress.getItems().get(0).getId(),
					ApiAddresses.class);
			return postalAddresses;
		}
	}

//Key=UW22-KY95-CE53-EC75
	@PostMapping("/selectAddress")
	public AddressPopulated getSelectAddress(@RequestBody ApiAddress address) {

		SpecifcAddresses specificAddress = beService.addressGet(retrieveAddress + key + "&Id=" + address.getId(),
				SpecifcAddresses.class);

		AddressPopulated addressPopulated = new AddressPopulated();
		addressPopulated.setAddressLine1(specificAddress.getItems().get(0).getLine1());
		addressPopulated.setAddressLine2(specificAddress.getItems().get(0).getLine2());
		addressPopulated.setCity(specificAddress.getItems().get(0).getCity());
		addressPopulated.setCountry(specificAddress.getItems().get(0).getCountryName());
		return addressPopulated;
	}

	@GetMapping("/nationality")
	public List<Nationality> getNationality() {
		List<Nationality> nationalityList = new ArrayList<>();
		nationalityList.add(new Nationality("IND", "India"));
		nationalityList.add(new Nationality("USA", "United States"));
		nationalityList.add(new Nationality("AUS", "Australia"));
		nationalityList.add(new Nationality("CAD", "Canada"));
		nationalityList.add(new Nationality("DEN", "Denmark"));

		return nationalityList;
	}
}
