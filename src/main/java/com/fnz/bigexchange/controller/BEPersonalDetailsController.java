package com.fnz.bigexchange.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fnz.bigexchange.BEExceptionHandler.BEInvalidTokenException;
import com.fnz.bigexchange.BEstubDomains.D2CUserDetails;
import com.fnz.bigexchange.domains.CustomerInstruction;
import com.fnz.bigexchange.domains.CustomerInstructionAddressBook;
import com.fnz.bigexchange.domains.CustomerInstructionAddressBookEntry;
import com.fnz.bigexchange.domains.CustomerRoleInstructionAddress;
import com.fnz.bigexchange.domains.CustomerTitles;
import com.fnz.bigexchange.domains.DocumentType;
import com.fnz.bigexchange.domains.InstructionDocumentRequests;
import com.fnz.bigexchange.domains.InstructionDocumentTypeRequestTriggers;
import com.fnz.bigexchange.domains.InstructionDocumentTypes;
import com.fnz.bigexchange.domains.ProductWrapperDocument;
import com.fnz.bigexchange.domains.UserCredentials;
import com.fnz.bigexchange.domains.UserInstruction;
import com.fnz.bigexchange.domains.UserInstructionPassword;
import com.fnz.bigexchange.domains.UserInstructions;
import com.fnz.bigexchange.enums.CustomerRoleAddressType;
import com.fnz.bigexchange.enums.InstructionStatus;
import com.fnz.bigexchange.enums.SingleSignOnEnabled;
import com.fnz.bigexchange.enums.WrapperType;
import com.fnz.bigexchange.service.BERestTemplateServiceImp;
import com.fnz.bigexchange.service.MappingHelper;
import com.fnz.bigexchange.service.SessionService;
import com.fnz.bigexchange.service.SessionServiceImpl;

@RestController
@RequestMapping("/api")
public class BEPersonalDetailsController {
	private static Logger logger = LoggerFactory.getLogger(BEPersonalDetailsController.class);

	@Autowired
	private BERestTemplateServiceImp beService;

	@Autowired
	private SessionService sessionService;

	public MappingHelper mappingHelper = new MappingHelper();

	public BEPersonalDetailsController() {
		super();
	}

	/** Loading of Personal Details Page */

	// re-called again in customer addressbook
	@GetMapping("registration/shared/customerInstructionAddressbook")
	public CustomerInstructionAddressBook getCustomerInstructionAddressbook(
			@RequestHeader(value = "token", required = false) String token) {
		if (sessionService.isTokenValid(token)) {
			CustomerInstructionAddressBook customerInstructionAddressBook = beService.genericGet(
					"/customerinstructions/" + sessionService.getSessionValue(token, SessionServiceImpl.CUSTOMER_ID)
							+ "/addressbook",
					CustomerInstructionAddressBook.class);
			return customerInstructionAddressBook;
		}
		throw new BEInvalidTokenException(SessionService.invalidToken);
	}

	// no need to pass subaccount id , date to get titles --> check it
	@GetMapping("/customer/availableTitles")
	public CustomerTitles getCustomerTitles(@RequestHeader(value = "token", required = false) String token) {
		if (sessionService.isTokenValid(token)) {
			CustomerTitles customerTitles = beService.genericGet("/customertitles", CustomerTitles.class);
			return customerTitles;
		}
		throw new BEInvalidTokenException(SessionService.invalidToken);
	}

	/** updating customer personal details */
	@PutMapping("/registration/shared/customerInstruction/put")
	public void putCustomerDetatils(@RequestBody CustomerInstruction customerDetails,
			@RequestHeader(value = "token", required = false) String token) {
		if (sessionService.isTokenValid(token)) {
			beService.genericPut("/customerinstructions", customerDetails);
			logger.info("update success " + customerDetails.toString());
		} else {
			throw new BEInvalidTokenException(SessionService.invalidToken);
		}
	}

	/**
	 * This resource represents address book linked to a customer instruction.
	 * Address book is composed of up to 256 address book entries that could be
	 * linked to any customer-account role
	 */
	@PostMapping("registration/shared/customerInstructionAddressbook")
	public CustomerInstructionAddressBook postCustomerInstructionAddressbook(
			@RequestBody CustomerInstructionAddressBookEntry customerAddressBook,
			@RequestHeader(value = "token", required = false) String token) {
		// calling to get number of results
		if (sessionService.isTokenValid(token)) {
			CustomerInstructionAddressBook customerInstructionAddressBook = beService.genericGet(
					"/customerinstructions/" + sessionService.getSessionValue(token, SessionService.CUSTOMER_ID)
							+ "/addressbook",
					CustomerInstructionAddressBook.class);

			CustomerInstructionAddressBookEntry customerAddressBookEntry = null;
			if (null == customerInstructionAddressBook.getResults().get(0)) {
				customerAddressBookEntry = beService.genericPost("customerinstructions/"
						+ sessionService.getSessionValue(token, SessionService.CUSTOMER_ID) + "/addressbook",
						customerAddressBook, CustomerInstructionAddressBookEntry.class);
				logger.info(customerAddressBookEntry.toString() + "post customer address");
			} else {
				System.out.println("else");
				beService.genericPut("customerinstructions/"
						+ sessionService.getSessionValue(token, SessionService.CUSTOMER_ID) + "/addressbook",
						CustomerInstructionAddressBookEntry.class);
				logger.info(" put request customerAddressBookEntry");
			}

			// calling again to get if results are posted
			customerInstructionAddressBook = beService.genericGet("/customerinstructions/"
					+ sessionService.getSessionValue(token, SessionService.CUSTOMER_ID) + "/addressbook",
					CustomerInstructionAddressBook.class);

			CustomerRoleInstructionAddress customerRoleAddress = new CustomerRoleInstructionAddress();
			customerRoleAddress.setAddressBookInstructionId(customerAddressBookEntry.getAddressBookEntryId());
			customerRoleAddress.setType(CustomerRoleAddressType.Residential);

			beService.genericPost("customerroleinstructions/"
					+ sessionService.getSessionValue(token, SessionService.CUSTOMER_ROLE_ID) + "/addresses",
					customerRoleAddress, CustomerRoleInstructionAddress.class);
			return customerInstructionAddressBook;
		}
		throw new BEInvalidTokenException(SessionService.invalidToken);
	}

	/** get details of a logged user based on ownerhierarchyid */
	@GetMapping("/registration/shared/userInstruction/get")
	public UserInstructions getUserDetails(@RequestParam String ownerhierarchyid,
			@RequestHeader(value = "token", required = false) String token) {
		if (sessionService.isTokenValid(token)) {
			UserInstructions account = beService.genericGet("/userinstructions?ownerhierarchyid=" + ownerhierarchyid,
					UserInstructions.class);
			logger.info(account.toString());
			return account;
		}
		throw new BEInvalidTokenException(SessionService.invalidToken);
	}

	/**
	 * created new D2CUserDetails to get object from UI if username is same then 422
	 * Unprocessable Entity
	 */
	@PostMapping("registration/shared/userInstruction/put")
	public UserCredentials postUserDetails(@RequestBody D2CUserDetails userDetail,
			@RequestHeader(value = "token", required = false) String token) {
		System.out.println();
		if (sessionService.isTokenValid(token)) {
			UserInstruction user = new UserInstruction();
			user.setStatus(InstructionStatus.Pending);
			user.setOwnerHierarchyId(userDetail.getCustomerId());
			user.setSingleSignOn(SingleSignOnEnabled.DirectSignOnOnly);

			UserCredentials userCredentials = userDetail.getUserCredentials();
			user.setUserName(userCredentials.getUserName());

			UserInstruction userDetails = beService.genericPost("/userinstructions", user, UserInstruction.class);

			UserInstructionPassword userInstructionPassword = new UserInstructionPassword();
			userInstructionPassword.setPassword(userCredentials.getPassword());
			sessionService.setSessionValue(token, SessionServiceImpl.USER_ID, userDetails.getId().toString());

			// userId = userDetails.getId();
			beService.genericPut("/userinstructions/"
					+ sessionService.getSessionValue(token, SessionServiceImpl.USER_ID) + "/password", userCredentials);
			return userCredentials;
		}
		throw new BEInvalidTokenException(SessionService.invalidToken);
	}

	/**
	 * terms and conditions documents referred from excel still under working
	 */
	@PostMapping("documents/instructionDocumentTypeRequestTriggers")
	public InstructionDocumentTypeRequestTriggers postTriggersForRequest(@RequestParam String customerId,
			@RequestHeader(value = "token", required = false) String token) {
		if (sessionService.isTokenValid(token)) {
			InstructionDocumentTypes documentTypes = beService.genericGet(
					"/instructions/" + sessionService.getSessionValue(token, SessionService.USER_ID) + "/documenttypes",
					InstructionDocumentTypes.class);

			// Long instructionId = userId;
			Long documentTypeId = 0L;
			for (DocumentType documentType : documentTypes.getResults()) {

			}

			InstructionDocumentTypeRequestTriggers postNewDocument = beService
					.genericPost(
							"/instructions/" + sessionService.getSessionValue(token, SessionService.USER_ID)
									+ "/documenttypes/" + documentTypeId + "/requesttriggers",
							null, InstructionDocumentTypeRequestTriggers.class);
			logger.info(postNewDocument.toString());
			return postNewDocument;
		}
		throw new BEInvalidTokenException(SessionService.invalidToken);
	}

	/**
	 * Retrieves all the document requests held under specified instruction
	 * satisfying query parameters
	 */
	@GetMapping("/documents/instructionDocumentRequests")
	public InstructionDocumentRequests getInstructionDocumentRequests(
			@RequestHeader(value = "token", required = false) String token) {
		if (sessionService.isTokenValid(token)) {
			// Long instructionId = userId;// 205766L;
			InstructionDocumentRequests documentRequests = beService.genericGet("/instructions/"
					+ sessionService.getSessionValue(token, SessionService.USER_ID) + "/documentrequests",
					InstructionDocumentRequests.class);
			logger.info(documentRequests.toString());
			return documentRequests;
		}
		throw new BEInvalidTokenException(SessionService.invalidToken);
	}

	/** Gets the collection of static documents associated with specified wrapper */
	@GetMapping("/documents/subaccountDocuments")
	public ProductWrapperDocument getsubaccountDocuments(
			@RequestHeader(value = "token", required = false) String token) {
		if (sessionService.isTokenValid(token)) {
			WrapperType wrapperType = WrapperType.StocksIsa;
			ProductWrapperDocument subaccountDocuments = beService
					.genericGet("/productwrappers/" + wrapperType + "/documents", ProductWrapperDocument.class);
			logger.info(subaccountDocuments.toString());
			return subaccountDocuments;
		}
		throw new BEInvalidTokenException(SessionService.invalidToken);
	}
}
