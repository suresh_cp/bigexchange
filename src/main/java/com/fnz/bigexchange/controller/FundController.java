package com.fnz.bigexchange.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fnz.bigexchange.domain.FundDetail;

@RestController
public class FundController {

	private static Logger logger = LoggerFactory.getLogger(FundController.class);



	public FundController() {
		super();
	}

	@GetMapping(path = "/funds")
	public ResponseEntity<String> getFunds() {
		List<FundDetail> funds = new ArrayList<FundDetail>();
		funds.add(new FundDetail(1L, "HDFC001", "HDFC Top 200", 4, "Equity", "HDFC Mutual"));
		funds.add(new FundDetail(2L, "HDFC002", "HDFC Equity", 3, "Equity", "HDFC Mutual"));
		funds.add(new FundDetail(3L, "SBIN001", "SBI Magnum Contra", 4, "Equity", "SBI Mutual"));
		funds.add(new FundDetail(4L, "SBIN002", "SBI Hybrid", 4, "Hybrid", "SBI Mutual"));
		funds.add(new FundDetail(5L, "HSBC001", "HSBC Equity", 4, "Equity", "HSBC Mutual"));
		funds.add(new FundDetail(6L, "HSBC002", "HSBC Debt", 1, "Equity", "HSBC Mutual"));
		return ResponseEntity.ok(getJsonResponse(funds));
	}

	@PostMapping(path = "/fund/save", consumes = { "application/json" })
	public ResponseEntity<String> save(@RequestBody FundDetail fund) {
		fund.setId(System.currentTimeMillis());
		return ResponseEntity.ok(getJsonResponse(fund));
	}
	
	protected String getJsonResponse(Object object) {
		String response = "";
		try {
			if (object == null) {
				return "";
			}

			response = new ObjectMapper().writeValueAsString(object);
		} catch (JsonProcessingException e) {
			logger.error("Exception in getJsonResponse() ", e);
		}
		return response;
	}


}
