package com.fnz.bigexchange.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class BigExchangeController {
	
	@Value("${registration.apply.registrationInstructions}")
	private String registrationInstructions;
	
	@GetMapping("/registration/apply/registrationInstructions")
	public ResponseEntity<String> getRegistrationInstructions() {
		return new ResponseEntity<>(registrationInstructions, HttpStatus.OK);
	}

	@Value("${documents.staticDocuments}")
	private String staticDocuments;
	
	@GetMapping("/documents/staticDocuments")
	public ResponseEntity<String> getStaticDocuments() {

		return new ResponseEntity<>(staticDocuments, HttpStatus.OK);
	}

	/**
	 * @Value("${distribution.v3.staticDocuments}") private String staticDocument;
	 * 
	 * @GetMapping("/distribution/v3/staticDocuments") public ResponseEntity<String>
	 * getDistributionStaticDocuments(@RequestParam(value = "id", required = false)
	 * Long id,
	 * 
	 * @RequestParam(value = "categories", required = false) String Categories,
	 * @RequestParam(value = "start", required = false) Long start,
	 * @RequestParam(value = "count", required = false) Long count) { return new
	 *                     ResponseEntity<>(staticDocument, HttpStatus.OK); }
	 * 
	 * 					@Value("${distribution.v3.productWrappers.{type}.documents}")
	 *                     private String productWrappers;
	 * 
	 * 					@GetMapping("/distribution/v3/productWrappers/{type}/documents")
	 *                     public ResponseEntity<String> getProductWrappers() {
	 *                     return new ResponseEntity<>(productWrappers,
	 *                     HttpStatus.OK); }
	 */
	@Value("${documents.subAccountDocuments}")
	private String subAccountDocuments;

	@GetMapping("/documents/subAccountDocuments")
	public ResponseEntity<String> getSubaccountDocuments(@RequestParam(value = "id", required = false) Long id) {
		return new ResponseEntity<>(subAccountDocuments, HttpStatus.OK);
	}

	@Value("${registration.shared.contractualTerms}")
	private String contractualTerms;

	@PostMapping("/registration/shared/contractualTerms")
	public ResponseEntity<String> saveContractualTerms(@RequestBody String dummy) {
		return new ResponseEntity<>(contractualTerms, HttpStatus.OK);
	}

	@Value("${subAccountInstruction.buyInstruction}")
	private String buyInstruction;

	@GetMapping("/subaccountInstruction/buyInstruction")
	public ResponseEntity<String> getBuyInstruction(@RequestParam(value = "id", required = false) Long id) {
		return new ResponseEntity<>(buyInstruction, HttpStatus.OK);
	}

	@Value("${distribution.v3.instructions.regularBuys}")
	private String regularbuys;

	@GetMapping("/distribution/v3/instructions/regularBuys")
	public ResponseEntity<String> getRegularBuys(@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "count", required = false) Long count) {
		return new ResponseEntity<>(regularbuys, HttpStatus.OK);
	}

	@Value("${subAccountInstruction.regularBuyInstruction}")
	private String regularBuyInstruction;

	@GetMapping("/subAccountInstruction/regularBuyInstruction")
	public ResponseEntity<String> getRegularBuyInstruction(@RequestParam(value = "id", required = false) Long id) {
		return new ResponseEntity<>(regularBuyInstruction, HttpStatus.OK);
	}

	@Value("${distribution.v3.instructions.buys}")
	private String buys;

	@GetMapping("/distribution/v3/instructions/buys")
	public ResponseEntity<String> getbuys(@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "count", required = false) Long count) {
		return new ResponseEntity<>(buys, HttpStatus.OK);
	}

	@Value("${contributionLimit.contributionLimits}")
	private String contributionLimits;

	@GetMapping("/contributionLimit/contributionLimits")
	public ResponseEntity<String> getContributionLimits(@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "subAccountType", required = false) Long subAccountType) {
		return new ResponseEntity<>(contributionLimits, HttpStatus.OK);
	}

	/*
	 * @Value(
	 * "${distribution.v3.accounts.002-0000028089.productWrappers.StocksIsa.contributionLimits}")
	 * private String stocksIsaContributionLimits;
	 * 
	 * @GetMapping(
	 * "/distribution/v3/accounts/002-0000028089/productWrappers/stocksIsa/contributionLimits")
	 * public ResponseEntity<String> getStocksIsaContributionLimits() { return new
	 * ResponseEntity<>(stocksIsaContributionLimits, HttpStatus.OK); }
	 */

	@Value("${allowance.productWrapperAllowance}")
	private String productWrapperAllowance;

	@GetMapping("/allowance/productWrapperAllowance")
	public ResponseEntity<String> getProductWrapperAllowance(@RequestParam(value = "id", required = false) Long id) {
		return new ResponseEntity<>(productWrapperAllowance, HttpStatus.OK);
	}

	/*
	 * @Value("${distribution.v3.productWrapper.stocksIsa.allowance.current}")
	 * private String currentAllowance;
	 * 
	 * @GetMapping("/distribution/v3/productWrapper/stocksIsa/allowance/current")
	 * public ResponseEntity<String> getCurrentAllowance() { return new
	 * ResponseEntity<>(currentAllowance, HttpStatus.OK); }
	 */
	@Value("${instrumentSearch.instrumentSelection}")
	private String instrumentSelection;

	@GetMapping("/instrumentSearch/instrumentSelection")
	public ResponseEntity<String> getInstrumentSelection(@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "subAccountType", required = false) Long subAccountType) {
		return new ResponseEntity<>(instrumentSelection, HttpStatus.OK);
	}

	/**
	 * @Value("${distribution.v3.funds}") private String funds;
	 * 
	 * @GetMapping("/distribution/v3/funds") public ResponseEntity<String>
	 * getFunds(@RequestParam(value = "id", required = false) Long id,
	 * 
	 * @RequestParam(value = "productwrappertype", required = false) String
	 *                     productwrappertype,
	 * @RequestParam(value = "buyable", required = false) Boolean buyable,
	 * @RequestParam(value = "obsrrating", required = false) String obsrrating,
	 * @RequestParam(value = "dividendReinvestment", required = false) String
	 *                     dividendReinvestment,
	 * @RequestParam(value = "historicalperformanceperiod", required = false) String
	 *                     historicalperformanceperiod,
	 * @RequestParam(value = "start", required = false) Long start,
	 * @RequestParam(value = "count", required = false) Long count) { return new
	 *                     ResponseEntity<>(productDocuments, HttpStatus.OK); }
	 * 
	 * 					@Value("${distribution.v3.products.documents}") private
	 *                     String productDocuments;
	 * 
	 * 					@GetMapping("/distribution/v3/products/documents") public
	 *                     ResponseEntity<String>
	 *                     getProductDocuments(@RequestParam(value = "id", required
	 *                     = false) Long id) { return new
	 *                     ResponseEntity<>(productDocuments, HttpStatus.OK); }
	 */
	@Value("${basketSharing.basketSharingInfo}")
	private String basketSharingInfo;

	@GetMapping("/basketSharing/basketSharingInfo")
	public ResponseEntity<String> getBasketSharingInfo() {
		return new ResponseEntity<>(basketSharingInfo, HttpStatus.OK);
	}

	@Value("${subAccountInstruction.buyInstruction}")
	private String saveBuyInstruction;

	@PostMapping("/subAccountInstruction/buyInstruction")
	public ResponseEntity<String> saveBuyInstruction(@RequestBody String dummy) {
		return new ResponseEntity<>(saveBuyInstruction, HttpStatus.OK);
	}

	@Value("${subAccountInstruction.regularBuyInstruction}")
	private String saveRegularBuyInstruction;

	@PostMapping("/subAccountInstruction/regularBuyInstruction")
	public ResponseEntity<String> saveRegularBuyInstruction(@RequestBody String dummy) {
		return new ResponseEntity<>(saveRegularBuyInstruction, HttpStatus.OK);
	}

	@Value("${registration.shared.customerInstructionAddressbook}")
	private String customerInstructionAddressbook;

	@GetMapping("/registration/shared/customerInstructionAddressbook")
	public ResponseEntity<String> getCustomerInstructionAddressbook(
			@RequestParam(value = "id", required = false) Long id) {
		return new ResponseEntity<>(customerInstructionAddressbook, HttpStatus.OK);
	}

	/*
	 * @Value("${distribution.v3.customerInstructions.030-0000205764.addressbook}")
	 * private String addressbook;
	 * 
	 * @GetMapping(
	 * "/distribution/v3/customerInstructions/030-0000205764/addressbook") public
	 * ResponseEntity<String> getAddressbook() { return new
	 * ResponseEntity<>(addressbook, HttpStatus.OK); }
	 */

	@Value("${customer.availableTitles}")
	private String availableTitles;

	@GetMapping("/customer/availableTitles")
	public ResponseEntity<String> getAvailableTitles() {
		return new ResponseEntity<>(availableTitles, HttpStatus.OK);
	}

	@Value("${distribution.v3.customerTitles}")
	private String customerTitles;

	@GetMapping("/distribution/v3/customerTitles")
	public ResponseEntity<String> getCustomertitles(@RequestParam(value = "date", required = false) Date date,
			@RequestParam(value = "gender", required = false) Character gender) {
		return new ResponseEntity<>(customerTitles, HttpStatus.OK);
	}

	@Value("${address.findAddresses}")
	private String findAddresses;

	@GetMapping("/address/findAddresses")
	public ResponseEntity<String> getFindAddresses(@RequestParam(value = "term", required = false) String term,
			@RequestParam(value = "code", required = false) Long countryCode) {
		return new ResponseEntity<>(findAddresses, HttpStatus.OK);
	}

	@Value("${registration.shared.customerInstruction}")
	private String updateCustomerInstruction;

	@PostMapping("/registration/shared/customerInstruction")
	public ResponseEntity<String> updateCustomerInstruction(@RequestBody String dummy) {
		return new ResponseEntity<>(updateCustomerInstruction, HttpStatus.OK);
	}

	@Value("${registration.shared.customerInstructionAddressbook}")
	private String saveCustomerInstructionAddressbook;

	@PostMapping("/registration/shared/customerInstructionAddressbook")
	public ResponseEntity<String> saveCustomerInstructionAddressbook(@RequestBody String dummy) {
		return new ResponseEntity<>(saveCustomerInstructionAddressbook, HttpStatus.OK);
	}

	/*
	 * @Value("${distribution.v3.customerInstructions.030-0000205764.addressbook}")
	 * private String saveAddressBook;
	 * 
	 * @PostMapping(
	 * "/distribution/v3/customerInstructions/030-0000205764/addressbook") public
	 * ResponseEntity<String> saveAddressbook(@RequestBody String dummy) { return
	 * new ResponseEntity<>(saveAddressBook, HttpStatus.OK); }
	 * 
	 * @Value("${distribution.v3.accountRoleInstructions.205765.customerroles}")
	 * private String customerRoles;
	 * 
	 * @GetMapping("/distribution/v3/accountRoleInstructions/205765/customerroles")
	 * public ResponseEntity<String> getCustomerroles() { return new
	 * ResponseEntity<>(customerRoles, HttpStatus.OK); }
	 * 
	 * @Value("${distribution.v3.customerRoleInstructions.27744.addresses}") private
	 * String saveAddresses;
	 * 
	 * @PostMapping("/distribution/v3/customerRoleInstructions/27744/addresses")
	 * public ResponseEntity<String> saveAddresses(@RequestBody String dummy) {
	 * return new ResponseEntity<>(saveAddresses, HttpStatus.OK); }
	 * 
	 * @Value("${distribution.v3.userInstructions.205768.password}") private String
	 * updatePassword;
	 * 
	 * @PutMapping("distribution/v3/userInstructions/205768/password") public
	 * ResponseEntity<String> updatePassword(@RequestBody String dummy) { return new
	 * ResponseEntity<>("", HttpStatus.OK); }
	 */
	@Value("${distribution.v3.customerInstructions}")
	private String saveCustomerInstructions;

	@PostMapping("/distribution/v3/customerInstructions")
	public ResponseEntity<String> saveCustomerInstructions(@RequestBody String dummy) {
		return new ResponseEntity<>(saveCustomerInstructions, HttpStatus.OK);
	}

	@Value("${registration.shared.userInstruction}")
	private String sharedUserInstruction;

	@GetMapping("/registration/shared/userInstruction")
	public ResponseEntity<String> getSharedUserInstruction(@RequestParam("date") Date date) {
		return new ResponseEntity<>(sharedUserInstruction, HttpStatus.OK);
	}

	@Value("${distribution.v3.userInstructions}")
	private String userInstruction;

	@GetMapping("/distribution/v3/userInstructions")
	public ResponseEntity<String> getUserInstruction(@RequestParam(value = "id", required = false) Long id) {
		return new ResponseEntity<>(sharedUserInstruction, HttpStatus.OK);
	}

	@Value("${registration.shared.userInstruction}")
	private String saveSharedUserInstruction;

	@PostMapping("/registration/shared/userInstruction")
	public ResponseEntity<String> saveSharedUserInstruction(@RequestBody String dummy) {
		return new ResponseEntity<>(saveSharedUserInstruction, HttpStatus.OK);
	}

	@Value("${distribution.v3.userInstructions}")
	private String saveUserInstructions;

	@PostMapping("/distribution/v3/userInstructions")
	public ResponseEntity<String> saveUserInstruction(@RequestBody String dummy) {
		return new ResponseEntity<>(saveUserInstructions, HttpStatus.OK);
	}

	@Value("${payments.availableDepositMethods}")
	private String availableDepositMethods;

	@GetMapping("/payments/availableDepositMethods")
	public ResponseEntity<String> getAvailableDepositMethods() {
		return new ResponseEntity<>(availableDepositMethods, HttpStatus.OK);
	}

	@Value("${process.shared.depositMethodLimits}")
	private String depositMethodLimits;

	@GetMapping("/process/shared/depositMethodLimits")
	public ResponseEntity<String> getDepositMethodLimits(@RequestParam(value = "id", required = false) Long id) {
		return new ResponseEntity<>(depositMethodLimits, HttpStatus.OK);
	}

	@Value("${payments.availableRegularDepositMethods}")
	private String saveAvailableRegularDepositMethods;

	@PostMapping("/payments/availableRegularDepositMethods")
	public ResponseEntity<String> saveAvailableRegularDepositMethods(@RequestBody String dummy) {
		return new ResponseEntity<>(saveAvailableRegularDepositMethods, HttpStatus.OK);
	}

	@Value("${documents.instructionDocumentTypeRequestTriggers}")
	private String saveInstructionDocumentTypeRequestTriggers;

	@PostMapping("/documents/instructionDocumentTypeRequestTriggers")
	public ResponseEntity<String> saveInstructionDocumentTypeRequestTriggers(@RequestBody String dummy) {
		return new ResponseEntity<>(saveInstructionDocumentTypeRequestTriggers, HttpStatus.OK);
	}

	/*
	 * @Value(
	 * "${distribution.v3.instructions.205766.documentTypes.49.requestTriggers}")
	 * private String requestTriggers;
	 * 
	 * @GetMapping(
	 * "/distribution/v3/instructions/205766/documentTypes/49/requestTriggers")
	 * public ResponseEntity<String> getRequesttriggers() { return new
	 * ResponseEntity<>(requestTriggers, HttpStatus.OK); }
	 */

	@Value("${documents.instructionDocumentRequests}")
	private String instructionDocumentRequests;

	@GetMapping("/documents/instructionDocumentRequests")
	public ResponseEntity<String> getInstructionDocumentRequests(
			@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "instructionDocumentRequestId", required = false) Long instructionDocumentRequestId) {
		return new ResponseEntity<>(instructionDocumentRequests, HttpStatus.OK);
	}

	/*
	 * @Value("${distribution.v3.instructions.205766.documentRequests}") private
	 * String documentRequests;
	 * 
	 * @GetMapping("/distribution/v3/instructions/205766/documentRequests") public
	 * ResponseEntity<String> getdocumentrequests() { return new
	 * ResponseEntity<>(documentRequests, HttpStatus.OK); }
	 */

	@Value("${common.businessDays}")
	private String businessDays;

	@GetMapping("/common/businessDays")
	public ResponseEntity<String> getBusinessDays() {
		return new ResponseEntity<>(businessDays, HttpStatus.OK);
	}

	@Value("${bankAccount.bankAccountInstruction}")
	private String saveBankAccountInstruction;

	@PostMapping("/bankAccount/bankAccountInstruction")
	public ResponseEntity<String> saveBankAccountInstruction(@RequestBody String dummy) {
		return new ResponseEntity<>(saveBankAccountInstruction, HttpStatus.OK);
	}

}
