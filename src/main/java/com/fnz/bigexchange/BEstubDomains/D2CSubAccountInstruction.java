package com.fnz.bigexchange.BEstubDomains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.WrapperType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class D2CSubAccountInstruction {

	private String hierarchyId;
	private WrapperType type;

	public D2CSubAccountInstruction() {

	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public WrapperType getType() {
		return type;
	}

	public void setType(WrapperType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "D2CSubAccountInstruction [hierarchyId=" + hierarchyId + ", type=" + type + "]";
	}

}
