package com.fnz.bigexchange.BEstubDomains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.ProductDocumentType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class D2CInstrument {

	private Long productId;
	private String name;
	private String keyInvestorDocumentUrl;
	private Boolean isincomefund;
	List<String> groups;
	private Double annualCharge;
	private Double dilutionlevy;
	private String sedol;
	private ProductDocumentType type;

	public D2CInstrument() {

	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKeyinvestordocumenturl() {
		return keyInvestorDocumentUrl;
	}

	public void setKeyinvestordocumenturl(String keyinvestordocumenturl) {
		this.keyInvestorDocumentUrl = keyinvestordocumenturl;
	}

	public Boolean getIsincomefund() {
		return isincomefund;
	}

	public void setIsincomefund(Boolean isincomefund) {
		this.isincomefund = isincomefund;
	}

	public List<String> getGroups() {
		return groups;
	}

	public void setGroups(List<String> groups) {
		this.groups = groups;
	}

	public Double getAnnualCharge() {
		return annualCharge;
	}

	public void setAnnualCharge(Double annualCharge) {
		this.annualCharge = annualCharge;
	}

	public Double getDilutionlevy() {
		return dilutionlevy;
	}

	public void setDilutionlevy(Double dilutionlevy) {
		this.dilutionlevy = dilutionlevy;
	}

	public String getSedol() {
		return sedol;
	}

	public void setSedol(String sedol) {
		this.sedol = sedol;
	}

	public ProductDocumentType getType() {
		return type;
	}

	public void setType(ProductDocumentType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "D2CInstrument [productId=" + productId + ", name=" + name + ", keyinvestordocumenturl="
				+ keyInvestorDocumentUrl + ", isincomefund=" + isincomefund + ", groups=" + groups + ", annualCharge="
				+ annualCharge + ", dilutionlevy=" + dilutionlevy + ", sedol=" + sedol + ", type=" + type + "]";
	}

}
