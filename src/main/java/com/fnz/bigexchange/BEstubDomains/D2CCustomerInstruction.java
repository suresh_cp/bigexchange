package com.fnz.bigexchange.BEstubDomains;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.domains.CustomerIdentifiers;
import com.fnz.bigexchange.enums.CountryCode;
import com.fnz.bigexchange.enums.GovernmentIdentifierType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class D2CCustomerInstruction {

	private String hierarchyId;
	private String title;
	private String firstName;
	private String middleName;
	private String lastName;
	private String email;
	private Date dateOfBirth;
	private GovernmentIdentifierType nationalInsuranceNumber;
	private String mobilePhoneNumber;
	private String landlinePhoneNumber;
	private CountryCode nationality;
	private CustomerIdentifiers customerIdentifiers;

	public D2CCustomerInstruction() {

	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public GovernmentIdentifierType getNationalInsuranceNumber() {
		return nationalInsuranceNumber;
	}

	public void setNationalInsuranceNumber(GovernmentIdentifierType nationalInsuranceNumber) {
		this.nationalInsuranceNumber = nationalInsuranceNumber;
	}

	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	public String getLandlinePhoneNumber() {
		return landlinePhoneNumber;
	}

	public void setLandlinePhoneNumber(String landlinePhoneNumber) {
		this.landlinePhoneNumber = landlinePhoneNumber;
	}

	public CountryCode getNationality() {
		return nationality;
	}

	public void setNationality(CountryCode nationality) {
		this.nationality = nationality;
	}

	public CustomerIdentifiers getCustomerIdentifiers() {
		return customerIdentifiers;
	}

	public void setCustomerIdentifiers(CustomerIdentifiers customerIdentifiers) {
		this.customerIdentifiers = customerIdentifiers;
	}

	@Override
	public String toString() {
		return "D2CCustomerInstruction [hierarchyId=" + hierarchyId + ", title=" + title + ", firstName=" + firstName
				+ ", middleName=" + middleName + ", lastName=" + lastName + ", email=" + email + ", dateOfBirth="
				+ dateOfBirth + ", nationalInsuranceNumber=" + nationalInsuranceNumber + ", mobilePhoneNumber="
				+ mobilePhoneNumber + ", landlinePhoneNumber=" + landlinePhoneNumber + ", nationality=" + nationality
				+ ", customerIdentifiers=" + customerIdentifiers + "]";
	}

}
