package com.fnz.bigexchange.BEstubDomains;

public class D2CRegistrationInstructions {

	private D2CRegistrationInstruction result;

	public D2CRegistrationInstructions() {

	}

	public D2CRegistrationInstruction getResult() {
		return result;
	}

	public void setResult(D2CRegistrationInstruction result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "D2CRegistrationInstructions [result=" + result + "]";
	}

}
