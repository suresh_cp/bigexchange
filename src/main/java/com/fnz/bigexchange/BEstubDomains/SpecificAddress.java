package com.fnz.bigexchange.BEstubDomains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SpecificAddress {

	private String id;
	private String domesticId;
	private String language;
	private String languageAlternatives;
	private String department;
	private String company;
	private String subBuilding;
	private String buildingNumber;
	private String buildingName;
	private String secondaryStreet;
	private String street;
	private String block;
	private String neighbourhood;
	private String district;
	private String city;
	private String line1;
	private String line2;
	private String line3;
	private String line4;
	private String line5;
	private String adminAreaName;
	private String adminAreaCode;
	private String province;
	private String provinceName;
	private String provinceCode;
	private String postalCode;
	private String countryName;
	private String countryIso2;
	private String countryIso3;
	private Double countryIsoNumber;
	private String sortingNumber1;
	private String sortingNumber2;
	private String barcode;
	private String pOBoxNumber;
	private String label;
	private String type;
	private String dataLevel;
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	private String field11;
	private String field12;
	private String field13;
	private String field14;
	private String field15;
	private String field16;
	private String field17;
	private String field18;
	private String field19;
	private String field20;

	public SpecificAddress() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDomesticId() {
		return domesticId;
	}

	public void setDomesticId(String domesticId) {
		this.domesticId = domesticId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLanguageAlternatives() {
		return languageAlternatives;
	}

	public void setLanguageAlternatives(String languageAlternatives) {
		this.languageAlternatives = languageAlternatives;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getSubBuilding() {
		return subBuilding;
	}

	public void setSubBuilding(String subBuilding) {
		this.subBuilding = subBuilding;
	}

	public String getBuildingNumber() {
		return buildingNumber;
	}

	public void setBuildingNumber(String buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getSecondaryStreet() {
		return secondaryStreet;
	}

	public void setSecondaryStreet(String secondaryStreet) {
		this.secondaryStreet = secondaryStreet;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getNeighbourhood() {
		return neighbourhood;
	}

	public void setNeighbourhood(String neighbourhood) {
		this.neighbourhood = neighbourhood;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getLine3() {
		return line3;
	}

	public void setLine3(String line3) {
		this.line3 = line3;
	}

	public String getLine4() {
		return line4;
	}

	public void setLine4(String line4) {
		this.line4 = line4;
	}

	public String getLine5() {
		return line5;
	}

	public void setLine5(String line5) {
		this.line5 = line5;
	}

	public String getAdminAreaName() {
		return adminAreaName;
	}

	public void setAdminAreaName(String adminAreaName) {
		this.adminAreaName = adminAreaName;
	}

	public String getAdminAreaCode() {
		return adminAreaCode;
	}

	public void setAdminAreaCode(String adminAreaCode) {
		this.adminAreaCode = adminAreaCode;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryIso2() {
		return countryIso2;
	}

	public void setCountryIso2(String countryIso2) {
		this.countryIso2 = countryIso2;
	}

	public String getCountryIso3() {
		return countryIso3;
	}

	public void setCountryIso3(String countryIso3) {
		this.countryIso3 = countryIso3;
	}

	public Double getCountryIsoNumber() {
		return countryIsoNumber;
	}

	public void setCountryIsoNumber(Double countryIsoNumber) {
		this.countryIsoNumber = countryIsoNumber;
	}

	public String getSortingNumber1() {
		return sortingNumber1;
	}

	public void setSortingNumber1(String sortingNumber1) {
		this.sortingNumber1 = sortingNumber1;
	}

	public String getSortingNumber2() {
		return sortingNumber2;
	}

	public void setSortingNumber2(String sortingNumber2) {
		this.sortingNumber2 = sortingNumber2;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getpOBoxNumber() {
		return pOBoxNumber;
	}

	public void setpOBoxNumber(String pOBoxNumber) {
		this.pOBoxNumber = pOBoxNumber;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDataLevel() {
		return dataLevel;
	}

	public void setDataLevel(String dataLevel) {
		this.dataLevel = dataLevel;
	}

	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}

	public String getField4() {
		return field4;
	}

	public void setField4(String field4) {
		this.field4 = field4;
	}

	public String getField5() {
		return field5;
	}

	public void setField5(String field5) {
		this.field5 = field5;
	}

	public String getField6() {
		return field6;
	}

	public void setField6(String field6) {
		this.field6 = field6;
	}

	public String getField7() {
		return field7;
	}

	public void setField7(String field7) {
		this.field7 = field7;
	}

	public String getField8() {
		return field8;
	}

	public void setField8(String field8) {
		this.field8 = field8;
	}

	public String getField9() {
		return field9;
	}

	public void setField9(String field9) {
		this.field9 = field9;
	}

	public String getField10() {
		return field10;
	}

	public void setField10(String field10) {
		this.field10 = field10;
	}

	public String getField11() {
		return field11;
	}

	public void setField11(String field11) {
		this.field11 = field11;
	}

	public String getField12() {
		return field12;
	}

	public void setField12(String field12) {
		this.field12 = field12;
	}

	public String getField13() {
		return field13;
	}

	public void setField13(String field13) {
		this.field13 = field13;
	}

	public String getField14() {
		return field14;
	}

	public void setField14(String field14) {
		this.field14 = field14;
	}

	public String getField15() {
		return field15;
	}

	public void setField15(String field15) {
		this.field15 = field15;
	}

	public String getField16() {
		return field16;
	}

	public void setField16(String field16) {
		this.field16 = field16;
	}

	public String getField17() {
		return field17;
	}

	public void setField17(String field17) {
		this.field17 = field17;
	}

	public String getField18() {
		return field18;
	}

	public void setField18(String field18) {
		this.field18 = field18;
	}

	public String getField19() {
		return field19;
	}

	public void setField19(String field19) {
		this.field19 = field19;
	}

	public String getField20() {
		return field20;
	}

	public void setField20(String field20) {
		this.field20 = field20;
	}

	@Override
	public String toString() {
		return "SpecificAddress [id=" + id + ", domesticId=" + domesticId + ", language=" + language
				+ ", languageAlternatives=" + languageAlternatives + ", department=" + department + ", company="
				+ company + ", subBuilding=" + subBuilding + ", buildingNumber=" + buildingNumber + ", buildingName="
				+ buildingName + ", secondaryStreet=" + secondaryStreet + ", street=" + street + ", block=" + block
				+ ", neighbourhood=" + neighbourhood + ", district=" + district + ", city=" + city + ", line1=" + line1
				+ ", line2=" + line2 + ", line3=" + line3 + ", line4=" + line4 + ", line5=" + line5 + ", adminAreaName="
				+ adminAreaName + ", adminAreaCode=" + adminAreaCode + ", province=" + province + ", provinceName="
				+ provinceName + ", provinceCode=" + provinceCode + ", postalCode=" + postalCode + ", countryName="
				+ countryName + ", countryIso2=" + countryIso2 + ", countryIso3=" + countryIso3 + ", countryIsoNumber="
				+ countryIsoNumber + ", sortingNumber1=" + sortingNumber1 + ", sortingNumber2=" + sortingNumber2
				+ ", barcode=" + barcode + ", pOBoxNumber=" + pOBoxNumber + ", label=" + label + ", type=" + type
				+ ", dataLevel=" + dataLevel + ", field1=" + field1 + ", field2=" + field2 + ", field3=" + field3
				+ ", field4=" + field4 + ", field5=" + field5 + ", field6=" + field6 + ", field7=" + field7
				+ ", field8=" + field8 + ", field9=" + field9 + ", field10=" + field10 + ", field11=" + field11
				+ ", field12=" + field12 + ", field13=" + field13 + ", field14=" + field14 + ", field15=" + field15
				+ ", field16=" + field16 + ", field17=" + field17 + ", field18=" + field18 + ", field19=" + field19
				+ ", field20=" + field20 + "]";
	}

}
