package com.fnz.bigexchange.BEstubDomains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.domains.UserCredentials;

@JsonIgnoreProperties(ignoreUnknown = true)
public class D2CUserDetails {

	private String customerId;
	private UserCredentials userCredentials;

	public D2CUserDetails() {

	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public UserCredentials getUserCredentials() {
		return userCredentials;
	}

	public void setUserCredentials(UserCredentials userCredentials) {
		this.userCredentials = userCredentials;
	}

	@Override
	public String toString() {
		return "D2CUserDetails [customerId=" + customerId + ", userCredentials=" + userCredentials + "]";
	}

}
