package com.fnz.bigexchange.BEstubDomains;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiAddresses {

	private List<ApiAddress> items;

	public ApiAddresses() {

	}

	public List<ApiAddress> getItems() {
		return items;
	}

	public void setItems(List<ApiAddress> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "ApiAddresses [items=" + items + "]";
	}

}
