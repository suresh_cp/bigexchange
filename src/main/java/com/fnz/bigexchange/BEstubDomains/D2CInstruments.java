package com.fnz.bigexchange.BEstubDomains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class D2CInstruments {

	List<D2CInstrument> instruments;

	public D2CInstruments() {

	}

	public List<D2CInstrument> getInstruments() {
		return instruments;
	}

	public void setInstruments(List<D2CInstrument> instruments) {
		this.instruments = instruments;
	}

	@Override
	public String toString() {
		return "D2CInstruments [instruments=" + instruments + "]";
	}

}
