package com.fnz.bigexchange.BEstubDomains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.domains.AcceptedContractualTerms;

@JsonIgnoreProperties(ignoreUnknown = true)
public class D2CAccountInstruction {

	private String hierarchyId;
	private List<AcceptedContractualTerms> acceptedContractualTerms;

	public D2CAccountInstruction() {

	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public List<AcceptedContractualTerms> getAcceptedContractualTerms() {
		return acceptedContractualTerms;
	}

	public void setAcceptedContractualTerms(List<AcceptedContractualTerms> acceptedContractualTerms) {
		this.acceptedContractualTerms = acceptedContractualTerms;
	}

	@Override
	public String toString() {
		return "D2CAccountInstruction [hierarchyId=" + hierarchyId + ", acceptedContractualTerms="
				+ acceptedContractualTerms + "]";
	}

}
