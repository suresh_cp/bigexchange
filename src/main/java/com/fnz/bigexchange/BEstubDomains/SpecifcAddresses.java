package com.fnz.bigexchange.BEstubDomains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SpecifcAddresses {

	private List<SpecificAddress> items;

	public SpecifcAddresses() {

	}

	public List<SpecificAddress> getItems() {
		return items;
	}

	public void setItems(List<SpecificAddress> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "SpecifcAddresses [items=" + items + "]";
	}

}
