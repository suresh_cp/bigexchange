package com.fnz.bigexchange.BEstubDomains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class D2CRegistrationInstruction {

	// used fnz domains instead of custom domains to avoid confusion
	// also we can populate only required
	private String parentHierarchyId; // AdviseryHeiraricalId
	private D2CAccountInstruction AccountInstruction;

	private String ChildAccountInstruction;
	private D2CCustomerInstruction CustomerInstruction;

	private String ChildCustomerInstruction;
	private D2CSubAccountInstruction SubAccountInstruction;

	private String JisaTransferInstruction;

	public D2CRegistrationInstruction() {

	}

	public String getParentHierarchyId() {
		return parentHierarchyId;
	}

	public void setParentHierarchyId(String parentHierarchyId) {
		this.parentHierarchyId = parentHierarchyId;
	}

	public D2CAccountInstruction getAccountInstruction() {
		return AccountInstruction;
	}

	public void setAccountInstruction(D2CAccountInstruction accountInstruction) {
		AccountInstruction = accountInstruction;
	}

	public String getChildAccountInstruction() {
		return ChildAccountInstruction;
	}

	public void setChildAccountInstruction(String childAccountInstruction) {
		ChildAccountInstruction = childAccountInstruction;
	}

	public D2CCustomerInstruction getCustomerInstruction() {
		return CustomerInstruction;
	}

	public void setCustomerInstruction(D2CCustomerInstruction customerInstruction) {
		CustomerInstruction = customerInstruction;
	}

	public String getChildCustomerInstruction() {
		return ChildCustomerInstruction;
	}

	public void setChildCustomerInstruction(String childCustomerInstruction) {
		ChildCustomerInstruction = childCustomerInstruction;
	}

	public D2CSubAccountInstruction getSubAccountInstruction() {
		return SubAccountInstruction;
	}

	public void setSubAccountInstruction(D2CSubAccountInstruction subAccountInstruction) {
		SubAccountInstruction = subAccountInstruction;
	}

	public String getJisaTransferInstruction() {
		return JisaTransferInstruction;
	}

	public void setJisaTransferInstruction(String jisaTransferInstruction) {
		JisaTransferInstruction = jisaTransferInstruction;
	}

	@Override
	public String toString() {
		return "D2CRegistrationInstruction [parentHierarchyId=" + parentHierarchyId + ", AccountInstruction="
				+ AccountInstruction + ", ChildAccountInstruction=" + ChildAccountInstruction + ", CustomerInstruction="
				+ CustomerInstruction + ", ChildCustomerInstruction=" + ChildCustomerInstruction
				+ ", SubAccountInstruction=" + SubAccountInstruction + ", JisaTransferInstruction="
				+ JisaTransferInstruction + "]";
	}

}
