package com.fnz.bigexchange.service;

import com.fnz.bigexchange.BEstubDomains.D2CAccountInstruction;
import com.fnz.bigexchange.BEstubDomains.D2CCustomerInstruction;
import com.fnz.bigexchange.BEstubDomains.D2CInstrument;
import com.fnz.bigexchange.BEstubDomains.D2CInstruments;
import com.fnz.bigexchange.BEstubDomains.D2CRegistrationInstruction;
import com.fnz.bigexchange.domains.AccountInstruction;
import com.fnz.bigexchange.domains.AccountRoleInstruction;
import com.fnz.bigexchange.domains.CustomerInstruction;
import com.fnz.bigexchange.domains.CustomerRoleInstruction;
import com.fnz.bigexchange.domains.Fund;
import com.fnz.bigexchange.domains.Funds;
import com.fnz.bigexchange.domains.NameSubAccountInstruction;
import com.fnz.bigexchange.domains.ProductDocuments;
import com.fnz.bigexchange.domains.SubAccountInstruction;
import com.fnz.bigexchange.enums.AccountType;
import com.fnz.bigexchange.enums.BankAccountType;
import com.fnz.bigexchange.enums.DistributionDividendPreference;
import com.fnz.bigexchange.enums.DistributionFrequency;
import com.fnz.bigexchange.enums.InstructionStatus;
import com.fnz.bigexchange.enums.ProductDocumentType;
import com.fnz.bigexchange.enums.WrapperType;

public class MappingHelper {

	// @Value("${parentHierarchyId}")
	// TO CHECK :: not able to get retrieve value from application.properties as it
	// is not configurable
	private String parentId = "002-0000028089";

	public AccountInstruction createAccount() {
		System.out.println(parentId);
		AccountInstruction createAccount = new AccountInstruction();
		NameSubAccountInstruction accountName = new NameSubAccountInstruction();
		accountName.setFull("Individual");
		createAccount.setStatus(InstructionStatus.Pending);
		createAccount.setType(AccountType.Individual);
		createAccount.setParentHierarchyId(parentId);
		createAccount.setAccountName(accountName);
		return createAccount;
	}

	public CustomerInstruction createCustomer() {
		CustomerInstruction createCustomer = new CustomerInstruction();
		createCustomer.setStatus(InstructionStatus.Pending);
		createCustomer.setAntiMoneyLaunderingVetted(false);
		createCustomer.setCreateUser(true);
		createCustomer.setParentHierarchyid(parentId);
		return createCustomer;
	}

	public AccountRoleInstruction createAccountRole(String accountId) {
		AccountRoleInstruction createAccountRole = new AccountRoleInstruction();
		createAccountRole.setStatus(InstructionStatus.Pending);
		createAccountRole.setAccountHierarchyId(accountId);
		return createAccountRole;

	}

	public CustomerRoleInstruction createCustomerRole(String customerId , Long customerRoleTypeValue) {
		CustomerRoleInstruction createCustomerRole = new CustomerRoleInstruction();
	
		
		createCustomerRole.setCustomerHierarchyId(customerId);
		createCustomerRole.setPrimaryRole(true);
		createCustomerRole.setRoleTypeId(customerRoleTypeValue);
		return createCustomerRole;
	}

	public SubAccountInstruction createSubAccount(String accountId) {
		SubAccountInstruction createSubAccount = new SubAccountInstruction();
		createSubAccount.setOwnerHierarchyId(accountId);
		createSubAccount.setInstructionStatus(InstructionStatus.Pending);
		createSubAccount.setType(WrapperType.StocksIsa);
		createSubAccount.setDistributionDividendPreference(DistributionDividendPreference.NotSpecified);
		createSubAccount.setExternalPaymentFrequency(DistributionFrequency.NotSpecified);
		createSubAccount.setBankAccountIdType(BankAccountType.NotSpecified);
		createSubAccount.setDividendReinvestmentPlanApplied(false);
		return createSubAccount;

	}

	public D2CRegistrationInstruction registerAccount(AccountInstruction account, String customerId) {
		D2CRegistrationInstruction register = new D2CRegistrationInstruction();

		register.setParentHierarchyId(parentId);

		D2CAccountInstruction d2cAccount = new D2CAccountInstruction();

		d2cAccount.setHierarchyId(account.getHierarchyId());
		register.setAccountInstruction(d2cAccount);

		D2CCustomerInstruction d2cCustomer = new D2CCustomerInstruction();
		d2cCustomer.setHierarchyId(customerId);
		register.setCustomerInstruction(d2cCustomer);
		return register;
	}
	
	// need to confirm as product id's are not checked  
	public D2CInstruments instrumentSelection(Funds funds , ProductDocuments productDocuments ) {
		D2CInstruments instruments = new D2CInstruments();
	
		// need to check if no of products of both objects are same 
		for(Fund f :funds.getPageOfResults()) {
			D2CInstrument instrument = new D2CInstrument();
			instrument.setType(ProductDocumentType.NotSpecified);
			instrument.setIsincomefund(false);	
		}	
		return instruments;
	}
	
}
