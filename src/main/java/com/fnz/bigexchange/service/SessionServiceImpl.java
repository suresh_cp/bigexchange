package com.fnz.bigexchange.service;

import java.security.SecureRandom;
import java.util.HashMap;

import org.springframework.stereotype.Service;

import com.fnz.bigexchange.BEExceptionHandler.BEInvalidTokenException;

@Service
public class SessionServiceImpl implements SessionService {

	private static final String VALID_TOKEN_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	private static SecureRandom secureRandom = new SecureRandom();

	private HashMap<String, HashMap<String, String>> session = new HashMap<String, HashMap<String, String>>();

	/*
	 * @Value("${invalid.token.message:Invalid Token}") public String invalidToken;
	 */

	public String randomString(int length) {
		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++)
			sb.append(VALID_TOKEN_CHARS.charAt(secureRandom.nextInt(VALID_TOKEN_CHARS.length())));
		return sb.toString();
	}

	@Override
	public void setSessionValue(String token, String key, String value) {
		if (session.get(token) == null) {
			session.put(token, new HashMap<String, String>());
		}
		session.get(token).put(key, value);
	}

	@Override
	public boolean isTokenValid(String token) {
		if (token == null) {
			throw new BEInvalidTokenException("please provide token in headers");
		}
		return session.get(token) != null;
	}

	@Override
	public String getSessionValue(String token, String key) {
		if (session.get(token) != null) {
			return session.get(token).get(key);
		}
		return null;
	}

}
