package com.fnz.bigexchange.service;

public interface BERestTemplateService {

	public <T> T genericPost(String path, T instance, Class<T> t);

	public <T> String genericPostForString(String path, T instance, Class<String> s);

	public <T> T genericGet(String path, Class<T> t);

	public <T> void genericPut(String path, T instance);

	public <T> T addressGet(String path, Class<T> t);
}
