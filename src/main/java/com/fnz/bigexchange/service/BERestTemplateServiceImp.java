package com.fnz.bigexchange.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fnz.bigexchange.util.Headers;

@Service
public class BERestTemplateServiceImp implements BERestTemplateService{

	private static final Logger logger = LoggerFactory.getLogger(BERestTemplateServiceImp.class);

	@Value("${api}")
	private String resource;

	@Autowired
	private RestTemplate restTemplate;

	public void register() {

	}

	public <T> T genericPost(String path, T instance, Class<T> t) {
		HttpEntity<T> entity = new HttpEntity<T>(instance, Headers.createHttpHeaders());
		T responsepost = null;
		try {
			responsepost = (T) restTemplate.postForObject(resource + path, entity, t);
		} catch (Exception e) {
			logger.error("error:  " + e.getMessage(), e);
		}
		if (responsepost != null)
			logger.info(responsepost.toString());
		return responsepost;
	}

	public <T> String genericPostForString(String path, T instance, Class<String> s) {
		HttpEntity<T> entity = new HttpEntity<T>(instance, Headers.createHttpHeaders());
		String responsepost = null;
		try {
			responsepost = (String) restTemplate.postForObject(resource + path, entity, s);
		} catch (Exception e) {
			logger.error("error:  " + e.getMessage(), e);
		}
		if (responsepost != null)
			logger.info(responsepost.toString());
		return responsepost;
	}

	public <T> T genericGet(String path, Class<T> t) {
		HttpEntity<?> entity = new HttpEntity<>(Headers.createHttpHeaders());
		ResponseEntity<T> responsepost = null;
		try {
			responsepost = restTemplate.exchange(resource + path, HttpMethod.GET, entity, t);
		} catch (Exception e) {
			logger.error("error:  " + e.getMessage(), e);
		}
		if (responsepost != null)
			logger.info(responsepost.toString());
		return responsepost.getBody();
	}

	public <T> void genericPut(String path, T instance ) {
		HttpEntity<T> entity = new HttpEntity<T>(instance, Headers.createHttpHeaders());

		try {
			restTemplate.put(resource + path, entity );
			//restTemplate.exchange(resource + path, HttpMethod.PUT, entity);
			logger.info("update success" );
		} catch (Exception e) {
			logger.error("error:  " + e.getMessage(), e);
		}
	}

	
	public <T> T addressGet(String path, Class<T> t) {
		HttpEntity<?> entity = new HttpEntity<>(Headers.createHttpHeaders());
		ResponseEntity<T> responsepost = null;
		try {
			responsepost = restTemplate.exchange( path, HttpMethod.GET, entity, t);
		} catch (Exception e) {
			logger.error("error:  " + e.getMessage(), e);
		}
		if (responsepost != null)
			logger.info(responsepost.toString());
		return responsepost.getBody();
	}
}
