package com.fnz.bigexchange.service;

public interface SessionService {
	
	public final static String ADVISER_HIERARCHY_ID = "AdviserHierarchyId";
	public final static String ACCOUNT_ID = "AccountId";
	public final static String CUSTOMER_ID = "CustomerId";
	public final static String ACCOUNT_ROLE_ID = "AccountRoleId";
	public final static String CUSTOMER_ROLE_ID = "CustomerRoleId";
	public final static String SUBACCOUNT_ID = "SubAccountId";
	public final static String USER_ID = "userId";
	public final static String invalidToken = "Invalid Token";
	
	void setSessionValue(String token, String key, String value);

	boolean isTokenValid(String token);

	String getSessionValue(String token, String key);

}