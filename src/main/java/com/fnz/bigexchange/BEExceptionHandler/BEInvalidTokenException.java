package com.fnz.bigexchange.BEExceptionHandler;

public class BEInvalidTokenException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public BEInvalidTokenException(String exception) {
		super(exception);
	}
}