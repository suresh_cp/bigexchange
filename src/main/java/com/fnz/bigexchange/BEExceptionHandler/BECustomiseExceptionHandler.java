package com.fnz.bigexchange.BEExceptionHandler;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fnz.bigexchange.domains.ErrorDetails;

@ControllerAdvice
@RestController
public class BECustomiseExceptionHandler extends ResponseEntityExceptionHandler {

	  @ExceptionHandler(BEInvalidTokenException.class)
	  public final ResponseEntity<ErrorDetails> handleAllExceptions(BEInvalidTokenException ex, WebRequest request) {
		  ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),
		      request.getDescription(false));
		  return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	  
	  @ExceptionHandler(BEPlatformException.class)
	  public final ResponseEntity<ErrorDetails> handleAllExceptions(BEPlatformException ex, WebRequest request) {
		  ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),
		      request.getDescription(false));
		  return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	  
	  @ExceptionHandler(BEAddressException.class)
	  public final ResponseEntity<ErrorDetails> handleAllExceptions(BEAddressException ex, WebRequest request) {
		  ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),
		      request.getDescription(false));
		  return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}

}