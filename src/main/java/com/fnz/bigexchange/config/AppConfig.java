package com.fnz.bigexchange.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;

@Configuration
@ComponentScan("com.fnz.bigexchange")
public class AppConfig {


	@Bean
	public PropertyNamingStrategy propertyNamingStrategy() {
		return new UpperCamelCaseStrategy(); 
	}

	@Bean
	public ObjectMapper jacksonObjectMapper() {
		return new ObjectMapper().setPropertyNamingStrategy(propertyNamingStrategy());
	}

	@Bean
	RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();


		 List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
	       MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
	       jsonMessageConverter.setObjectMapper(jacksonObjectMapper());
	       messageConverters.add(jsonMessageConverter);
	       messageConverters.add(new StringHttpMessageConverter());
	       restTemplate.setMessageConverters(messageConverters); 
		return restTemplate;
	}

	
	
	
}
