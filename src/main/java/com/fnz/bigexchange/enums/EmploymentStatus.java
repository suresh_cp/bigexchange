package com.fnz.bigexchange.enums;

public enum EmploymentStatus {
	NotSpecified,
    Employed,
    SelfEmployed,
    Retired,
    CaringForAPersonUnder16,
    CaringForAPersonOver16,
    ChildUnder16,
    FullTimeEducation,
    Unemployed,
    Partner,
    Other
}
