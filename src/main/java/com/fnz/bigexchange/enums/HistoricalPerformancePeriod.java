package com.fnz.bigexchange.enums;

public enum HistoricalPerformancePeriod {
	    NotSpecified,
	    OneMonth,
	    ThreeMonths,
	    SixMonths,
	    TwelveMonths,
	    TwentyFourMonths,
	    ThirtySixMonths,
	    FortyEightMonths,
	    SixtyMonths,
	    EightyFourMonths,
	    OneHundredAndTwentyMonths
}
