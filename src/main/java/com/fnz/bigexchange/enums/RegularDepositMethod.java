package com.fnz.bigexchange.enums;

public enum RegularDepositMethod {
	 NotSpecified,
     DirectDebit,
     Cheque,
     CashSubAccount,
     DirectCredit,
     ProductWrapperCash,
     SpecifiedSubAccount,
     BACS,
     CHAPS,
     Faster
}
