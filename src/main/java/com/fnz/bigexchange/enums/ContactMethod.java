package com.fnz.bigexchange.enums;

public enum ContactMethod {
	 NotSpecified,
     Mail,
     Phone,
     Email,
     Sms,
     Mobile,
     Fax
}
