package com.fnz.bigexchange.enums;

public enum RegulationType {
	Fatca,
    UkFatca,
    GibraltarFatca,
    GuernseyFatca,
    IsleOfManFatca,
    JerseyFatca,
    Crs
}
