package com.fnz.bigexchange.enums;

public enum UserType {
	NotSpecified,
    WrapProvider,
    Adviser,
    Customer,
    Network,
    Company,
    Branch
}
