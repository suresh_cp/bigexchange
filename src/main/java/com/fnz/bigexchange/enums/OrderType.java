package com.fnz.bigexchange.enums;

public enum OrderType {
	  NotSpecified,
      Value,
      Quantity,
      Percentage
}
