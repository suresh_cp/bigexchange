package com.fnz.bigexchange.enums;

public enum PaymentSource {
	 NotSpecified,
     Employee,
     Employer
}
