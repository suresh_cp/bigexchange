package com.fnz.bigexchange.enums;

public enum PaymentStatus {
	 NotSpecified,
     Authorised,
     Created,
     Deleted,
     Completed,
     Confirmed,
     Placed,
     Cancelled
}
