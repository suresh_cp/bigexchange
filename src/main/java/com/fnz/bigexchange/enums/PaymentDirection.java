package com.fnz.bigexchange.enums;

public enum PaymentDirection {
	  NotSpecified,
      Deposit,
      Withdrawal
}
