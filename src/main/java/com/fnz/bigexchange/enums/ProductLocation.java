package com.fnz.bigexchange.enums;

public enum ProductLocation {
	NotSpecified,
    BrokerTransferAccount,
    CustodyTransferIn,
    CustodyTransferOut,
    Custody,
    External,
    Registry
}
