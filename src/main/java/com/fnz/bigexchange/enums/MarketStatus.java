package com.fnz.bigexchange.enums;

public enum MarketStatus {
	  NotSpecified,
      NotApplicable,
      PreOpen,
      Open,
      PostTrading,
      Closed
}
