package com.fnz.bigexchange.enums;

public enum FileExtension {
	 NotSpecified,
     Pdf,
     Doc,
     Xls,
     Docx,
     Xlsx,
     Txt,
     Csv
}
