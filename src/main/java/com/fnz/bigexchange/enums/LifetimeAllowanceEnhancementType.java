package com.fnz.bigexchange.enums;

public enum LifetimeAllowanceEnhancementType {
	 NotSpecified,
     OverseasIndividual,
     OverseasPensionScheme,
     ReceiptOfPensionCredit
}
