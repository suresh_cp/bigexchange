package com.fnz.bigexchange.enums;

public enum CustomerCountryType {
	 NotSpecified,
     CountryOfBirth,
     CountryOfCitizenship,
     SupplementaryCountryOfCitizenship,
     TaxCountryOfResidence,
     SupplementaryTaxCountryOfResidence
}
