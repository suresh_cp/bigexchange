package com.fnz.bigexchange.enums;

/** these are noted from from - end side */
public enum StaticDocumentCategory {
	NotSpecified, 
	KeyFeatures, 
	TermsAndConditions, 
	Brochures,
	FundsImportantInformation, 
	FeesChargesAndInterestRates,
	OrganisationForms, 
	ModelPortfolios,
	Other
}
