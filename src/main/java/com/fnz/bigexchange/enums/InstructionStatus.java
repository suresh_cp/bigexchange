package com.fnz.bigexchange.enums;

public enum InstructionStatus {
	NotSpecified,
	Pending,
	Submitted,
	Cancelled,
	Active,
	Complete,
	AwaitingAuthorisation
}
