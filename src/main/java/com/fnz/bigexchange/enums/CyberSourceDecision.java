package com.fnz.bigexchange.enums;

public enum CyberSourceDecision {
	NotSpecified,	
	Accept,
	Decline,
	Review,
	Error,
	Cancel
}
