package com.fnz.bigexchange.enums;

public enum ExternalTransferType {
	 NotSpecified,
     InSpecie,
     Cash,
     Mixed
}
