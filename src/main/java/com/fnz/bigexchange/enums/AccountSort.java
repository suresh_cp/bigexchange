package com.fnz.bigexchange.enums;

public enum AccountSort {	
	NotSpecified,	
	AccountName	,
	AccountStatus,	
	AccountType
}
