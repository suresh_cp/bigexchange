package com.fnz.bigexchange.enums;

public enum OrderStatus {
	  Created,
      Authorised,
      Placed,
      PartFilled,
      Completed,
      Cancelled,
      Expired,
      Rejected,
      Pooled,
      AuthorisedSwitch,
      Confirmed,
      Processing,
      Failed,
      PendingCancel
}
