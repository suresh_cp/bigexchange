package com.fnz.bigexchange.enums;

public enum ProductPriceType {
	 NotSpecified,
     Live,
     Delayed,
     EndOfDay
}
