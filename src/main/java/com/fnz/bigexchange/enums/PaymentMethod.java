package com.fnz.bigexchange.enums;

public enum PaymentMethod {
	 NotSpecified,
     DebitCard,
     DirectDebit,
     DirectCredit,
     Cheque,
     Bacs,
     InternalTransfer,
     TransferredSubscription,
     Chaps,
     RemovedSubscription,
     Faster
}
