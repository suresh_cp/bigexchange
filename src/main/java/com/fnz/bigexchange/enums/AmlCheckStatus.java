package com.fnz.bigexchange.enums;

public enum AmlCheckStatus {
	  NotSpecified,
      Failed,
      Refer,
      Pass
}
