package com.fnz.bigexchange.enums;

public enum TaxIdentifierType {
	 NotSpecified,
     EIN,
     ITIN,
     IRDNO,
     NINO,
     NRIC,
     ATIN,
     SSN,
     TFN,
     TIN,
     FIN,
     TN,
     FC
}
