package com.fnz.bigexchange.enums;

public enum DividendReinvestment {
	 NotSpecified,
     Income,
     Accumulation
}
