package com.fnz.bigexchange.enums;

public enum Status {
	NotSpecified,
	Cheque,
	CashSubAccount,
	SpecifiedSubAccount,
	ProductWrapperCash,
	DebitCard,
	DirectCredit,
	DirectDebit,
	BACS,
	CHAPS,
	Faster,
	OffPlatform
}
