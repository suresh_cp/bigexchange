package com.fnz.bigexchange.enums;

public enum CustomerRoleType {
	NotSpecified,
    Individual,
    Joint,
    Trustee,
    PowerOfAttorney,
    RegisteredContact,
    OrganisationAccountHolder,
    Director,
    Settlor,
    Beneficiary,
    Protector,
    Manager,
    Shareholder,
    Owner,
    Designation,
    Dependant,
    Nominee,
    Successor,
    Employer,
    Spouse,
    LOAs,
    ControllingPerson
}
