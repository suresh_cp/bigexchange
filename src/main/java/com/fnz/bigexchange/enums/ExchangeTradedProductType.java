package com.fnz.bigexchange.enums;

public enum ExchangeTradedProductType {
	 NotSpecified,
     Stock,
     InvestmentTrust,
     RealEstateInvestmentTrust,
     ExchangeTradedFund,
     EquityOption,
     IndexOption,
     Right,
     StapledSecurity,
     Warrant,
     CoveredWarrant	
}
