package com.fnz.bigexchange.enums;

public enum ProductDocumentType {
	 NotSpecified,
     KeyInvestorInformationDocument,
     FactSheet,
     LongProspectus,
     StockNote
}
