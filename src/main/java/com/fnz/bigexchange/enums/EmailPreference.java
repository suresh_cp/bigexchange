package com.fnz.bigexchange.enums;

public enum EmailPreference {
	NotSpecified,
    Home,
    Work
}
