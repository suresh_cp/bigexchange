package com.fnz.bigexchange.enums;

public enum Gender {
	NotSpecified,
    Male,
    Female
}
