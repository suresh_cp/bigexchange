package com.fnz.bigexchange.enums;

public enum DepositMethod {
	NotSpecified,
	Cheque,
	CashSubAccount,
	SpecifiedSubAccount,
	ProductWrapperCash,
	DebitCard,
	DirectCredit,
	DirectDebit,
	BACS,
	CHAPS,
	Faster,
	OffPlatform
}
