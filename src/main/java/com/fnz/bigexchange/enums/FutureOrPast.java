package com.fnz.bigexchange.enums;

public enum FutureOrPast {
	NotSpecified,	
	Future,	
	Past
}
