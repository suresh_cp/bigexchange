package com.fnz.bigexchange.enums;

public enum CurrentOrNextTaxYear {
	NotSpecified,
	Current,
	Next
}
