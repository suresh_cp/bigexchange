package com.fnz.bigexchange.enums;

public enum BankAccountOwner {
	 NotSpecified,
     Individual,
     Joint,
     Employer,
     Partner,
     ThirdParty,
     ThirdPartyOut,
     Trust
}
