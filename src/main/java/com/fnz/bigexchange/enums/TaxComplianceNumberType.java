package com.fnz.bigexchange.enums;

public enum TaxComplianceNumberType {
	 NotSpecified,
     GeneralTaxpayerIdentificationNumber,
     SocialSecurityNumber,
     EmployerIdentificationNumber,
     IndividualTaxpayerIdentificationNumber,
     GlobalIntermediaryIdentificationNumber
}
