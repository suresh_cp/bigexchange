package com.fnz.bigexchange.enums;

public enum DocumentOriginType {
	NotSpecified,
     UserInput,
     UserRequest,
     SystemInput,
     SystemRequest
}
