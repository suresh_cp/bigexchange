package com.fnz.bigexchange.enums;

public enum CustomerStatus {
	 Inactive,
     Active,
     Deceased
}
