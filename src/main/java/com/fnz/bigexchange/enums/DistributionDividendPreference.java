package com.fnz.bigexchange.enums;

public enum DistributionDividendPreference {
	NotSpecified ,
	Reinvest,
	ProductCash,
	WrapCash,
	BankAccount,
	SubAccountTransfer,
	External
}
