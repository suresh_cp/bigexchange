package com.fnz.bigexchange.enums;

public enum PhoneNumberType {
	 NotSpecified,
     Work,
     Home,
     Mobile
}
