package com.fnz.bigexchange.enums;

public enum ContractualTermType {
	NotSpecified,
    IsaDeclaration,
    TermsAndConditions,
    TermsAndConditionsNeo,
    TermsAndConditionsAccum,
    TermsAndConditionsPension,
    SmallDeviceRestrictions
}
