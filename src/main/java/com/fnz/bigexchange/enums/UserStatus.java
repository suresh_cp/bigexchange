package com.fnz.bigexchange.enums;

public enum UserStatus {
	Locked,
	Unlocked
}
