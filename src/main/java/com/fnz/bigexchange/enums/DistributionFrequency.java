package com.fnz.bigexchange.enums;

public enum DistributionFrequency {
	NotSpecified,
	MinimumAmount,
	Immediately,
	Monthly,
	Quarterly,
	HalfYearly,
	Annually
}
