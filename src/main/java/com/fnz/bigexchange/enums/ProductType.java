package com.fnz.bigexchange.enums;

public enum ProductType {
	 NotSpecified,
     Cash,
     Equity,
     ManagedFund,
     Bond,
     PlatformFund,
     ExternalAsset,
     ModelPortfolio,
     ForeignExchange,
     Deposits,
     SyntheticFund,
     Lifestyle,
     CreditingRateFunds,
     TermDeposit,
     StructuredDeposit,
     InsuredFund,
     StructuredProduct,
     CommercialProperty,
     AdviserModelPortfolio,
     ModelAccount,
     Gilt
}
