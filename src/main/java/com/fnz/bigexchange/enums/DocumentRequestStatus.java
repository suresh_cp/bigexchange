package com.fnz.bigexchange.enums;

public enum DocumentRequestStatus {
	 NotSpecified,
     Pending,
     Cancelled,
     Completed,
     Failed
}
