package com.fnz.bigexchange.enums;

public enum PaymentType {
    NotSpecified,
    Single,
    Regular
}
