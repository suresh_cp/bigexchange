package com.fnz.bigexchange.enums;

public enum ObsrRating {
    NotSpecified,
    A,
    AA,
    AAA,
    AAS,
    AAAS,
    AS,
    NR,
    Gold,
    Silver,
    Bronze
}
