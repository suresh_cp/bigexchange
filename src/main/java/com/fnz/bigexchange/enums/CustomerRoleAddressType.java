package com.fnz.bigexchange.enums;

public enum CustomerRoleAddressType {
	Postal, 
	Residential
}
