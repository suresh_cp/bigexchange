package com.fnz.bigexchange.enums;

public enum MaritalStatus {
	NotSpecified,
	Married,
	Separated,
	Divorced,
	Widowed,
	Single,
	Partner,
	CivilPartner
}
