package com.fnz.bigexchange.enums;

public enum SubAccountStatus {
	 NotSpecified,
     Inactive,
     Submitted,
     Active,
     PendingClosure,
     ClosureRequested,
     Closed
}
