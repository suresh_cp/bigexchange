package com.fnz.bigexchange.enums;

public enum AccountChargeStatus {
	NotSpecified,	
	Posted	,
	UnPosted
}
