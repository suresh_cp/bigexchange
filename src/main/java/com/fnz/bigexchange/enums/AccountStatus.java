package com.fnz.bigexchange.enums;

public enum AccountStatus {
	NotSpecified,
    Active,
    Inactive,
    Submitted,
    Deceased,
    DeceasedNotified,
    Closed,
    ClosureRequested,
    InProgress,
    Rejected,
    CancellationNotice,
    Deleted,
    Closing
}
