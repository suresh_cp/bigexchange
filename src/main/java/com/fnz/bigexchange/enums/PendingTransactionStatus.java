package com.fnz.bigexchange.enums;

public enum PendingTransactionStatus {	
	NotSpecified,	
	Authorised	,
	Pooled	,
	Placed	,
	Completed
}
