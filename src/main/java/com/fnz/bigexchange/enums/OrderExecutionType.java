package com.fnz.bigexchange.enums;

public enum OrderExecutionType {
	  NotSpecified,
      Broker,
      AtBest,
      QuoteAndDeal,
      Single,
      Regular,
      InitialPublicOffering
}
