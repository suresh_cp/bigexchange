package com.fnz.bigexchange.enums;

public enum SingleSignOnEnabled {
	 NotSpecified,
     SingleSignOnOnly,
     DirectSignOnOnly
}
