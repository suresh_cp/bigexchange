package com.fnz.bigexchange.enums;

public enum PensionArrangementType {
	 NotSpecified,
     Accumulation,
     Crystallised
}
