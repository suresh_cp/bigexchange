package com.fnz.bigexchange.enums;

public enum ThreadType {
	NotSpecified,
	Message,
	Alert,
	Information,
	ImportantInformation
}
