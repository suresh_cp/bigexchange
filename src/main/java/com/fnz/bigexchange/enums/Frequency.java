package com.fnz.bigexchange.enums;

public enum Frequency {
	NotSpecified,
	Daily,
	Weekly,
	Fortnightly,
	Monthly,
	Quarterly,
	HalfYearly,
	Yearly
}
