package com.fnz.bigexchange.enums;

public enum CrownRating {
	 NotSpecified,
     One,
     Two,
     Three,
     Four,
     Five
}
