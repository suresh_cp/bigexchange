package com.fnz.bigexchange.enums;

public enum GovernmentIdentifierType {
	NotSpecified,
    HongKongId,
    SingaporeFin,
    Nino,
    PassportId,
    NationalIdentificationNumber,
    NationalTaxIdentificationNumber,
    NationalInsuranceNumber,
    SocialSecurityNumber,
    TaxIdentificationNumber,
    Other
}
