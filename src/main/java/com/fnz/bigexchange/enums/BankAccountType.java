package com.fnz.bigexchange.enums;

public enum BankAccountType {
	NotSpecified, 
	BankAccount,
	BankAccountInstruction
}
