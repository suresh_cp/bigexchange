package com.fnz.bigexchange.enums;

public enum SectorProviderType {
	 NotSpecified,
     FinEx,
     MorningStar
}
