package com.fnz.bigexchange.enums;

public enum OrderDirection {
    NotSpecified,
    Buy,
    Sell
}
