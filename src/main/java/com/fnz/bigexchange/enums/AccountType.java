package com.fnz.bigexchange.enums;

public enum AccountType {
	 NotSpecified,
     Individual,
     Joint,
     Trust,
     Company,
     Charity,
     Group
}
