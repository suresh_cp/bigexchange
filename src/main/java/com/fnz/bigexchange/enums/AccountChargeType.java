package com.fnz.bigexchange.enums;

public enum AccountChargeType {
	NotSpecified,
	AdviserAdHocCharge, 
	DfmOngoingCharge,
	DfmInitialCharge,
	DfmTieredCharge, 
	AdviserDrawdownInitialCharge,
	PlatformDrawdownInitialCharge,
	PlatformDrawdownOngoingCharge,
	AdviserInitialCharge, 
	AdviserOngoingCharge,
	PlatformPensionOngoingCharge,
	PlatformPensionOpeningCharge,
	RecurringInitialAdviserCharge,
	AdviserSwitchCharge,
	PlatformAnnualManagementCharge,
	AdhocCharge,
	BuyTransactionCharge,
	ChapsCharge,
	OfflineDocumentPrinting,
	OngoingManagementCharge,
	SellTransactionCharge,
	Suric,
	DimOngoingCharge,
	DimTransactionalCharge,
	PaperCharge
}
