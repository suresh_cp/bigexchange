package com.fnz.bigexchange.enums;

public enum RatingType {
	 NotSpecified,
     Crown,
     Obsr,
     Morningstar
}
