package com.fnz.bigexchange.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EntityScan({ "com.fnz.bigexchange.domains", "com.fnz.bigexchange.domains.instruction" })
@ComponentScan("com.fnz.bigexchange")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
