package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

public class InstructionDocumentTypes {

	private List<DocumentType> results;
	private List<Link> links;

	public InstructionDocumentTypes() {

	}

	public List<DocumentType> getResults() {
		return results;
	}

	public void setResults(List<DocumentType> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "InstructionDocumentTypes [results=" + results + ", links=" + links + "]";
	}

}
