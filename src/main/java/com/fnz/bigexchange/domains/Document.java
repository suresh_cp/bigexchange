package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)

public class Document {
	private Long id;
	private Date addedOn;
	private String displayName;
	private String fileName;
	private Long fileSize;
	private DocumentFormat documentFormat;
	private List<Link> links;
	public Document() {
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getAddedOn() {
		return addedOn;
	}
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Long getFileSize() {
		return fileSize;
	}
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
	public DocumentFormat getDocumentFormat() {
		return documentFormat;
	}
	public void setDocumentFormat(DocumentFormat documentFormat) {
		this.documentFormat = documentFormat;
	}
	public List<Link> getLinks() {
		return links;
	}
	public void setLinks(List<Link> links) {
		this.links = links;
	}
	@Override
	public String toString() {
		return "Document [id=" + id + ", addedOn=" + addedOn + ", displayName=" + displayName + ", fileName=" + fileName
				+ ", fileSize=" + fileSize + ", documentFormat=" + documentFormat + ", links=" + links + "]";
	}

	


}
