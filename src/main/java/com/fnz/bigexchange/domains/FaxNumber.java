package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.CountryCode;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FaxNumber {

	private CountryCode country;
	private String number;
	private String areaCode;

	public FaxNumber() {

	}

	public CountryCode getCountry() {
		return country;
	}

	public void setCountry(CountryCode country) {
		this.country = country;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	@Override
	public String toString() {
		return "FaxNumber [country=" + country + ", number=" + number + ", areaCode=" + areaCode + "]";
	}

}
