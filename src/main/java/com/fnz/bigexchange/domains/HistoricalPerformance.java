package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoricalPerformance {

	private Double oneYear;
	private Double twoYear;
	private Double threeYear;
	private Double fourYear;
	private Double fiveYear;

	public HistoricalPerformance() {

	}

	public Double getOneYear() {
		return oneYear;
	}

	public void setOneYear(Double oneYear) {
		this.oneYear = oneYear;
	}

	public Double getTwoYear() {
		return twoYear;
	}

	public void setTwoYear(Double twoYear) {
		this.twoYear = twoYear;
	}

	public Double getThreeYear() {
		return threeYear;
	}

	public void setThreeYear(Double threeYear) {
		this.threeYear = threeYear;
	}

	public Double getFourYear() {
		return fourYear;
	}

	public void setFourYear(Double fourYear) {
		this.fourYear = fourYear;
	}

	public Double getFiveYear() {
		return fiveYear;
	}

	public void setFiveYear(Double fiveYear) {
		this.fiveYear = fiveYear;
	}

	@Override
	public String toString() {
		return "PastPerformance [oneYear=" + oneYear + ", twoYear=" + twoYear + ", threeYear=" + threeYear
				+ ", fourYear=" + fourYear + ", fiveYear=" + fiveYear + "]";
	}

}
