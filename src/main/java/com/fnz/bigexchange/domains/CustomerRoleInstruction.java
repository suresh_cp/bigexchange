package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerRoleInstruction {

	private Long id;
	private String customerHierarchyId;
	private Boolean isPrimaryRole;
	private Long roleTypeId;
	private List<Link> links;

	public CustomerRoleInstruction() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerHierarchyId() {
		return customerHierarchyId;
	}

	public void setCustomerHierarchyId(String customerHierarchyId) {
		this.customerHierarchyId = customerHierarchyId;
	}

	public boolean isPrimaryRole() {
		return isPrimaryRole;
	}

	public void setPrimaryRole(Boolean isPrimaryRole) {
		this.isPrimaryRole = isPrimaryRole;
	}

	public Long getRoleTypeId() {
		return roleTypeId;
	}

	public void setRoleTypeId(Long roleTypeId) {
		this.roleTypeId = roleTypeId;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "AccountRoleInstructionCustomerRole [id=" + id + ", customerHierarchyId=" + customerHierarchyId
				+ ", isPrimaryRole=" + isPrimaryRole + ", roleTypeId=" + roleTypeId + ", links=" + links + "]";
	}

}
