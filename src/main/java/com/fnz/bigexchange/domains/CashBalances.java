package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CashBalances {

	private List<CashBalance> results;
	private List<Link> links;

	public CashBalances() {

	}

	public List<CashBalance> getResults() {
		return results;
	}

	public void setResults(List<CashBalance> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "CashBalances [results=" + results + ", links=" + links + "]";
	}

}
