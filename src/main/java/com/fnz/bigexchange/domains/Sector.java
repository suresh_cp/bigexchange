package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.SectorProviderType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Sector {

	private Long Id;
	private String Code;
	private String Name;
	private SectorProviderType SectorProvider;

	public Sector() {

	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public SectorProviderType getSectorProvider() {
		return SectorProvider;
	}

	public void setSectorProvider(SectorProviderType sectorProvider) {
		SectorProvider = sectorProvider;
	}

	@Override
	public String toString() {
		return "Sector [Id=" + Id + ", Code=" + Code + ", Name=" + Name + ", SectorProvider=" + SectorProvider + "]";
	}

}
