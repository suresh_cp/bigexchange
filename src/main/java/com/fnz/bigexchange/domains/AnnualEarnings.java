package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AnnualEarnings {

	private String currency;
	private Double value;

	public AnnualEarnings() {

	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "AnnualEarnings [currency=" + currency + ", value=" + value + "]";
	}

}
