package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.InstructionStatus;
import com.fnz.bigexchange.enums.SingleSignOnEnabled;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInstruction {

	private Long id;
	private InstructionStatus status;
	private String ownerHierarchyId;
	private String userName;
	private Boolean readOnly;
	private SingleSignOnEnabled singleSignOn;
	private List<Link> links;

	public UserInstruction() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public InstructionStatus getStatus() {
		return status;
	}

	public void setStatus(InstructionStatus status) {
		this.status = status;
	}

	public String getOwnerHierarchyId() {
		return ownerHierarchyId;
	}

	public void setOwnerHierarchyId(String ownerHierarchyId) {
		this.ownerHierarchyId = ownerHierarchyId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Boolean getReadOnly() {
		return readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	public SingleSignOnEnabled getSingleSignOn() {
		return singleSignOn;
	}

	public void setSingleSignOn(SingleSignOnEnabled singleSignOn) {
		this.singleSignOn = singleSignOn;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "UserInstruction [id=" + id + ", status=" + status + ", ownerHierarchyId=" + ownerHierarchyId
				+ ", userName=" + userName + ", readOnly=" + readOnly + ", singleSignOn=" + singleSignOn + ", links="
				+ links + "]";
	}

}
