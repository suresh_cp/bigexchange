package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.CountryCode;
import com.fnz.bigexchange.enums.TaxIdentifierType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TaxIdentifier {

	private CountryCode country;
	private TaxIdentifierType type;
	private String value;
	private String expirationDate;

	public TaxIdentifier() {

	}

	public CountryCode getCountry() {
		return country;
	}

	public void setCountry(CountryCode country) {
		this.country = country;
	}

	public TaxIdentifierType getType() {
		return type;
	}

	public void setType(TaxIdentifierType type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	@Override
	public String toString() {
		return "TaxIdentifier [country=" + country + ", type=" + type + ", value=" + value + ", expirationDate="
				+ expirationDate + "]";
	}

}
