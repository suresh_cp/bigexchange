package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.CustomerRoleType;

@JsonIgnoreProperties(ignoreUnknown = true)

public class AccountRole {

	private Boolean isPrimary;
	private Customer customer;
	private CustomerRoleType Role;
	private List<Link> links;

	public AccountRole() {

	}

	public Boolean getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public CustomerRoleType getRole() {
		return Role;
	}

	public void setRole(CustomerRoleType role) {
		Role = role;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "AccountRole [isPrimary=" + isPrimary + ", customer=" + customer + ", Role=" + Role + ", links=" + links
				+ "]";
	}

}
