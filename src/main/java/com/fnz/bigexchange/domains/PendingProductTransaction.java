package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.OrderDirection;
import com.fnz.bigexchange.enums.PendingTransactionStatus;
import com.fnz.bigexchange.enums.ProductTransactionType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PendingProductTransaction {

	private SubAccount subAccount;
	private Product product;
	private Double quantity;
	private String narrative;
	private MoneyContract cost;
	private MoneyContract fXCost;
	private ProductTransactionType transactionType;
	private Date transactionDate;
	private String transferProvider;
	private Long referenceNumber;
	private MoneyContract value;
	private OrderDirection orderDirection;
	private PendingTransactionStatus status;
	private List<Link> links;

	public PendingProductTransaction() {

	}

	public SubAccount getSubAccount() {
		return subAccount;
	}

	public void setSubAccount(SubAccount subAccount) {
		this.subAccount = subAccount;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getNarrative() {
		return narrative;
	}

	public void setNarrative(String narrative) {
		this.narrative = narrative;
	}

	public MoneyContract getCost() {
		return cost;
	}

	public void setCost(MoneyContract cost) {
		this.cost = cost;
	}

	public MoneyContract getfXCost() {
		return fXCost;
	}

	public void setfXCost(MoneyContract fXCost) {
		this.fXCost = fXCost;
	}

	public ProductTransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(ProductTransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransferProvider() {
		return transferProvider;
	}

	public void setTransferProvider(String transferProvider) {
		this.transferProvider = transferProvider;
	}

	public Long getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(Long referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public MoneyContract getValue() {
		return value;
	}

	public void setValue(MoneyContract value) {
		this.value = value;
	}

	public OrderDirection getOrderDirection() {
		return orderDirection;
	}

	public void setOrderDirection(OrderDirection orderDirection) {
		this.orderDirection = orderDirection;
	}

	public PendingTransactionStatus getStatus() {
		return status;
	}

	public void setStatus(PendingTransactionStatus status) {
		this.status = status;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "PendingProductTransaction [subAccount=" + subAccount + ", product=" + product + ", quantity=" + quantity
				+ ", narrative=" + narrative + ", cost=" + cost + ", fXCost=" + fXCost + ", transactionType="
				+ transactionType + ", transactionDate=" + transactionDate + ", transferProvider=" + transferProvider
				+ ", referenceNumber=" + referenceNumber + ", value=" + value + ", orderDirection=" + orderDirection
				+ ", status=" + status + ", links=" + links + "]";
	}

}
