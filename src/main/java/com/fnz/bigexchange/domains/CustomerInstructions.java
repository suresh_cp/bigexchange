package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerInstructions {

	private List<CustomerInstruction> results;
	private List<Link> links;

	public CustomerInstructions() {

	}

	public List<CustomerInstruction> getResults() {
		return results;
	}

	public void setResults(List<CustomerInstruction> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "CustomerInstructions [results=" + results + ", links=" + links + "]";
	}

}
