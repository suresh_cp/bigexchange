package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Allocations {

	private Long id;
	private Long productId;
	private Double quantity;
	private Double percentage;
	private MoneyContract value;
	
	public Allocations() {
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getPercentage() {
		return percentage;
	}
	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}
	public MoneyContract getValue() {
		return value;
	}
	public void setValue(MoneyContract value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "Allocations [id=" + id + ", productId=" + productId + ", quantity=" + quantity + ", percentage="
				+ percentage + ", value=" + value + "]";
	}


	

}
