package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountPayment {

	private Long id;
	private SubAccount subAccount;
	private String bankAccount;
	private MoneyContract amount;
	private String method;
	private String paymentSource;
	private String paymentType;
	private String direction;
	private String status;
	private String createdOn;
	private String postedOn;
	private boolean taxRelief;
	private MoneyContract taxReliefAmount;
	private String narrative;
	private List<Link> links;

	public AccountPayment() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SubAccount getSubAccount() {
		return subAccount;
	}

	public void setSubAccount(SubAccount subAccount) {
		this.subAccount = subAccount;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public MoneyContract getAmount() {
		return amount;
	}

	public void setAmount(MoneyContract amount) {
		this.amount = amount;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getPaymentSource() {
		return paymentSource;
	}

	public void setPaymentSource(String paymentSource) {
		this.paymentSource = paymentSource;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getPostedOn() {
		return postedOn;
	}

	public void setPostedOn(String postedOn) {
		this.postedOn = postedOn;
	}

	public boolean isTaxRelief() {
		return taxRelief;
	}

	public void setTaxRelief(boolean taxRelief) {
		this.taxRelief = taxRelief;
	}

	public MoneyContract getTaxReliefAmount() {
		return taxReliefAmount;
	}

	public void setTaxReliefAmount(MoneyContract taxReliefAmount) {
		this.taxReliefAmount = taxReliefAmount;
	}

	public String getNarrative() {
		return narrative;
	}

	public void setNarrative(String narrative) {
		this.narrative = narrative;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "AccountPayment [id=" + id + ", subAccount=" + subAccount + ", bankAccount=" + bankAccount + ", amount="
				+ amount + ", method=" + method + ", paymentSource=" + paymentSource + ", paymentType=" + paymentType
				+ ", direction=" + direction + ", status=" + status + ", createdOn=" + createdOn + ", postedOn="
				+ postedOn + ", taxRelief=" + taxRelief + ", taxReliefAmount=" + taxReliefAmount + ", narrative="
				+ narrative + ", links=" + links + "]";
	}

}
