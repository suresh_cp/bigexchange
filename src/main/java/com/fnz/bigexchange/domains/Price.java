package com.fnz.bigexchange.domains;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Price {

	private Date valuationDate;
	private MoneyContract value;

	public Price() {

	}

	public Date getValuationDate() {
		return valuationDate;
	}

	public void setValuationDate(Date valuationDate) {
		this.valuationDate = valuationDate;
	}

	public MoneyContract getValue() {
		return value;
	}

	public void setValue(MoneyContract value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Price [valuationDate=" + valuationDate + ", value=" + value + "]";
	}

}
