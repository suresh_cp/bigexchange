package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.ContactMethod;
import com.fnz.bigexchange.enums.EmailPreference;
import com.fnz.bigexchange.enums.PhoneNumberType;

//to 
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerContactDetails {

	private HomePhone homePhone;
	private WorkPhone workPhone;
	private MobilePhone mobilePhone;
	private FaxNumber faxNumber;
	private Email emailAddress;
	private Email alternateEmailAddress;
	private ContactMethod preferredContactMethod;
	private EmailPreference preferredEmailMethod;
	private PhoneNumberType preferredPhoneNumber;
	private Boolean allowMarketing;
	private Boolean paperlessCommunication;
	private List<String> allowableContactMethods;
	private Boolean GoneAway;
	private List<Link> links;

	public CustomerContactDetails() {

	}

	public HomePhone getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(HomePhone homePhone) {
		this.homePhone = homePhone;
	}

	public WorkPhone getWorkPhone() {
		return workPhone;
	}

	public void setWorkPhone(WorkPhone workPhone) {
		this.workPhone = workPhone;
	}

	public MobilePhone getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(MobilePhone mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public FaxNumber getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(FaxNumber faxNumber) {
		this.faxNumber = faxNumber;
	}

	public Email getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(Email emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Email getAlternateEmailAddress() {
		return alternateEmailAddress;
	}

	public void setAlternateEmailAddress(Email alternateEmailAddress) {
		this.alternateEmailAddress = alternateEmailAddress;
	}

	public ContactMethod getPreferredContactMethod() {
		return preferredContactMethod;
	}

	public void setPreferredContactMethod(ContactMethod preferredContactMethod) {
		this.preferredContactMethod = preferredContactMethod;
	}

	public EmailPreference getPreferredEmailMethod() {
		return preferredEmailMethod;
	}

	public void setPreferredEmailMethod(EmailPreference preferredEmailMethod) {
		this.preferredEmailMethod = preferredEmailMethod;
	}

	public PhoneNumberType getPreferredPhoneNumber() {
		return preferredPhoneNumber;
	}

	public void setPreferredPhoneNumber(PhoneNumberType preferredPhoneNumber) {
		this.preferredPhoneNumber = preferredPhoneNumber;
	}

	public Boolean getAllowMarketing() {
		return allowMarketing;
	}

	public void setAllowMarketing(Boolean allowMarketing) {
		this.allowMarketing = allowMarketing;
	}

	public Boolean getPaperlessCommunication() {
		return paperlessCommunication;
	}

	public void setPaperlessCommunication(Boolean paperlessCommunication) {
		this.paperlessCommunication = paperlessCommunication;
	}

	public List<String> getAllowableContactMethods() {
		return allowableContactMethods;
	}

	public void setAllowableContactMethods(List<String> allowableContactMethods) {
		this.allowableContactMethods = allowableContactMethods;
	}

	public Boolean getGoneAway() {
		return GoneAway;
	}

	public void setGoneAway(Boolean goneAway) {
		GoneAway = goneAway;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "CustomerContactDetails [homePhone=" + homePhone + ", workPhone=" + workPhone + ", mobilePhone="
				+ mobilePhone + ", faxNumber=" + faxNumber + ", emailAddress=" + emailAddress
				+ ", alternateEmailAddress=" + alternateEmailAddress + ", preferredContactMethod="
				+ preferredContactMethod + ", preferredEmailMethod=" + preferredEmailMethod + ", preferredPhoneNumber="
				+ preferredPhoneNumber + ", allowMarketing=" + allowMarketing + ", paperlessCommunication="
				+ paperlessCommunication + ", allowableContactMethods=" + allowableContactMethods + ", GoneAway="
				+ GoneAway + ", links=" + links + "]";
	}

}