package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountRoleInstructionCustomerRoles {

	private List<CustomerRoleInstruction> results;
	private List<Link> links;

	public AccountRoleInstructionCustomerRoles() {

	}

	public List<CustomerRoleInstruction> getResults() {
		return results;
	}

	public void setResults(List<CustomerRoleInstruction> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "AccountRoleInstructionCustomerRoles [results=" + results + ", links=" + links + "]";
	}

}
