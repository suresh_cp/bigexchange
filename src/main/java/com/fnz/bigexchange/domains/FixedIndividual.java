package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FixedIndividual {

	private Boolean hasProtection;
	private Long year;
	private Double entitlement;
	private String certificateNumber;

	public FixedIndividual() {

	}

	public Boolean isHasProtection() {
		return hasProtection;
	}

	public void setHasProtection(Boolean hasProtection) {
		this.hasProtection = hasProtection;
	}

	public Long getYear() {
		return year;
	}

	public void setYear(Long year) {
		this.year = year;
	}

	public Double getEntitlement() {
		return entitlement;
	}

	public void setEntitlement(Double entitlement) {
		this.entitlement = entitlement;
	}

	public String getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	@Override
	public String toString() {
		return "Fixed [hasProtection=" + hasProtection + ", year=" + year + ", entitlement=" + entitlement
				+ ", certificateNumber=" + certificateNumber + "]";
	}

}
