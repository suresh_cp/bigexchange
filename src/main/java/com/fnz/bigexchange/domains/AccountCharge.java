package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.AccountChargeStatus;
import com.fnz.bigexchange.enums.AccountChargeType;
import com.fnz.bigexchange.enums.DepositMethod;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountCharge {

	private Long id;
	private String hierarchyId;
	private MoneyContract amount;
	private Date transactionDate;
	private String narrative;
	private AccountChargeType chargeType;
	private AccountChargeStatus status;
	private Date fromDate;
	private String toDate;
	private DepositMethod depositMethod;
	private List<Link> links;

	public AccountCharge() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public MoneyContract getAmount() {
		return amount;
	}

	public void setAmount(MoneyContract amount) {
		this.amount = amount;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getNarrative() {
		return narrative;
	}

	public void setNarrative(String narrative) {
		this.narrative = narrative;
	}

	public AccountChargeType getChargeType() {
		return chargeType;
	}

	public void setChargeType(AccountChargeType chargeType) {
		this.chargeType = chargeType;
	}

	public AccountChargeStatus getStatus() {
		return status;
	}

	public void setStatus(AccountChargeStatus status) {
		this.status = status;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public DepositMethod getDepositMethod() {
		return depositMethod;
	}

	public void setDepositMethod(DepositMethod depositMethod) {
		this.depositMethod = depositMethod;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "AccountCharge [id=" + id + ", hierarchyId=" + hierarchyId + ", amount=" + amount + ", transactionDate="
				+ transactionDate + ", narrative=" + narrative + ", chargeType=" + chargeType + ", status=" + status
				+ ", fromDate=" + fromDate + ", toDate=" + toDate + ", depositMethod=" + depositMethod + ", links="
				+ links + "]";
	}

}
