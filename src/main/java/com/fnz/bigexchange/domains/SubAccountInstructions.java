package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown =true)
public class SubAccountInstructions {

	private List<SubAccountInstruction> results;
	private List<Link> links;

	public SubAccountInstructions() {
		
	}
	public List<SubAccountInstruction> getResults() {
		return results;
	}

	public void setResults(List<SubAccountInstruction> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "SubAccountInstructions [results=" + results + ", links=" + links + "]";
	}

}
