package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegularBuyInstructionCollection {

	private List<RegularBuyInstruction> results;
	private List<Link> links;

	public RegularBuyInstructionCollection() {

	}

	public List<RegularBuyInstruction> getResults() {
		return results;
	}

	public void setResults(List<RegularBuyInstruction> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "RegularBuyInstructionCollection [results=" + results + ", links=" + links + "]";
	}

}
