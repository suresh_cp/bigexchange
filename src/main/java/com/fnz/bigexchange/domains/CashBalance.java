package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CashBalance {

	private String hierarchyId;
	private MoneyContract availableBalanceForTrading;
	private MoneyContract availableBalanceForWithdrawal;
	private MoneyContract availableBalance;
	private MoneyContract balance;
	private List<Link> links;

	public CashBalance() {

	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public MoneyContract getAvailableBalanceForTrading() {
		return availableBalanceForTrading;
	}

	public void setAvailableBalanceForTrading(MoneyContract availableBalanceForTrading) {
		this.availableBalanceForTrading = availableBalanceForTrading;
	}

	public MoneyContract getAvailableBalanceForWithdrawal() {
		return availableBalanceForWithdrawal;
	}

	public void setAvailableBalanceForWithdrawal(
			MoneyContract availableBalanceForWithdrawal) {
		this.availableBalanceForWithdrawal = availableBalanceForWithdrawal;
	}

	public MoneyContract getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(MoneyContract availableBalance) {
		this.availableBalance = availableBalance;
	}

	public MoneyContract getBalance() {
		return balance;
	}

	public void setBalance(MoneyContract balance) {
		this.balance = balance;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "CashBalances [hierarchyId=" + hierarchyId + ", availableBalanceForTrading=" + availableBalanceForTrading
				+ ", availableBalanceForWithdrawal=" + availableBalanceForWithdrawal + ", availableBalance="
				+ availableBalance + ", balance=" + balance + ", links=" + links + "]";
	}

}
