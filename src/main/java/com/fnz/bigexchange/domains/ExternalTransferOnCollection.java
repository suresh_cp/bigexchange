package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalTransferOnCollection {

	private List<ExternalTransferOn> pageOfResults;
	private Long totalNumberOfResults;
	private List<Link> links;

	public ExternalTransferOnCollection() {

	}

	public List<ExternalTransferOn> getPageOfResults() {
		return pageOfResults;
	}

	public void setPageOfResults(List<ExternalTransferOn> pageOfResults) {
		this.pageOfResults = pageOfResults;
	}

	public Long getTotalNumberOfResults() {
		return totalNumberOfResults;
	}

	public void setTotalNumberOfResults(Long totalNumberOfResults) {
		this.totalNumberOfResults = totalNumberOfResults;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "ExternalTransferOnCollection [pageOfResults=" + pageOfResults + ", totalNumberOfResults="
				+ totalNumberOfResults + ", links=" + links + "]";
	}

}
