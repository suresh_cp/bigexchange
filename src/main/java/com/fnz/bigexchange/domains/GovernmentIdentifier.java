package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.CountryCode;
import com.fnz.bigexchange.enums.GovernmentIdentifierType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GovernmentIdentifier {

	private String value;
	private GovernmentIdentifierType type;
	private CountryCode country;

	public GovernmentIdentifier() {

	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public GovernmentIdentifierType getType() {
		return type;
	}

	public void setType(GovernmentIdentifierType type) {
		this.type = type;
	}

	public CountryCode getCountry() {
		return country;
	}

	public void setCountry(CountryCode country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "GovernmentIdentifier [value=" + value + ", type=" + type + ", country=" + country + "]";
	}

}
