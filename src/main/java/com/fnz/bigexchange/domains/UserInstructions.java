package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInstructions {

	private List<UserInstruction> results;
	private List<Link> links;

	public UserInstructions() {

	}

	public List<UserInstruction> getResults() {
		return results;
	}

	public void setResults(List<UserInstruction> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "UserInstructions [results=" + results + ", links=" + links + "]";
	}

}
