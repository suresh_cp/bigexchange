package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Funds {

	private List<Fund> pageOfResults;
	private Long totalNumberOfResults;
	private List<Link> links;

	public Funds() {

	}

	public List<Fund> getPageOfResults() {
		return pageOfResults;
	}

	public void setPageOfResults(List<Fund> pageOfResults) {
		this.pageOfResults = pageOfResults;
	}

	public Long getTotalNumberOfResults() {
		return totalNumberOfResults;
	}

	public void setTotalNumberOfResults(Long totalNumberOfResults) {
		this.totalNumberOfResults = totalNumberOfResults;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "Funds [pageOfResults=" + pageOfResults + ", totalNumberOfResults=" + totalNumberOfResults + ", links="
				+ links + "]";
	}

}
