package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductValuations {

	private List<ProductValuation> results;
	private List<Link> links;

	public ProductValuations() {

	}

	public List<ProductValuation> getResults() {
		return results;
	}

	public void setResults(List<ProductValuation> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "ProductValuations [results=" + results + ", links=" + links + "]";
	}

}
