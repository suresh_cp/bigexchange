package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductWrapperDocuments {

	private List<ProductWrapperDocument> results;
	private List<Link> links;

	public ProductWrapperDocuments() {

	}

	public List<ProductWrapperDocument> getResults() {
		return results;
	}

	public void setResults(List<ProductWrapperDocument> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

}
