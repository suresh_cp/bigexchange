package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FundIdentifiers {

	private String ProductCode;
	private String Sedol;
	private String Mex;
	private String CitiCode;
	private String Isin;

	public FundIdentifiers() {

	}

	public String getProductCode() {
		return ProductCode;
	}

	public void setProductCode(String productCode) {
		ProductCode = productCode;
	}

	public String getSedol() {
		return Sedol;
	}

	public void setSedol(String sedol) {
		Sedol = sedol;
	}

	public String getMex() {
		return Mex;
	}

	public void setMex(String mex) {
		Mex = mex;
	}

	public String getCitiCode() {
		return CitiCode;
	}

	public void setCitiCode(String citiCode) {
		CitiCode = citiCode;
	}

	public String getIsin() {
		return Isin;
	}

	public void setIsin(String isin) {
		Isin = isin;
	}

	@Override
	public String toString() {
		return "FundIdentifiers [ProductCode=" + ProductCode + ", Sedol=" + Sedol + ", Mex=" + Mex + ", CitiCode="
				+ CitiCode + ", Isin=" + Isin + "]";
	}

}
