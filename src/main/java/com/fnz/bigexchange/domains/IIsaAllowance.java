package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

public class IIsaAllowance {

	private TaxYearAllowance currentTaxYear;
	private TaxYearAllowance nextTaxYear;
	private List<Link> links;

	public IIsaAllowance() {

	}

	public TaxYearAllowance getCurrentTaxYear() {
		return currentTaxYear;
	}

	public void setCurrentTaxYear(TaxYearAllowance currentTaxYear) {
		this.currentTaxYear = currentTaxYear;
	}

	public TaxYearAllowance getNextTaxYear() {
		return nextTaxYear;
	}

	public void setNextTaxYear(TaxYearAllowance nextTaxYear) {
		this.nextTaxYear = nextTaxYear;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "IIsaAllowance [currentTaxYear=" + currentTaxYear + ", nextTaxYear=" + nextTaxYear + ", links=" + links
				+ "]";
	}

}
