package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.CyberSourceDecision;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CyberSourceCardPayments {

	private String authAmount;
	private String authAvsCode;
	private String authAvsCodeRaw;
	private String authCode;
	private String authCvResult;
	private String authResponse;
	private String authTime;
	private String authTransRefNo;
	private String billTransRefNo;
	private CyberSourceDecision decision;
	private String invalidFields;
	private String message;
	private String payerAuthenticationCavv;
	private String payerAuthenticationEci;
	private String payerAuthenticationProofXml;
	private String payerAuthenticationUad;
	private String payerAuthenticationUci;
	private String payerAuthenticationXid;
	private String paymentToken;
	private String reasonCode;
	private String reqAccessKey;
	private String reqAmount;
	private String reqBillPayment;
	private String reqBillToAddressCity;
	private String reqBillToAddressCountry;
	private String reqBillToAddressLine1;
	private String reqBillToAddressLine2;
	private String reqBillToAddressPostalCode;
	private String reqBillToCompanyName;
	private String reqBillToEmail;
	private String reqBillToForename;
	private String reqBillToPhone;
	private String reqBillToSurname;
	private String reqCardExpiryDate;
	private String reqCardNumber;
	private String reqCardType;
	private String reqCompanyTaxId;
	private String reqCompleteRoute;
	private String reqConsumerId;
	private String reqCurrency;
	private String reqCustomerIpAddress;
	private String reqDateOfBirth;
	private String reqDebitIndicator;
	private String reqDepatureTime;
	private String reqDeviceFingerprintId;
	private String reqPaymentToken;
	private String reqPaymentTokenComments;
	private String reqTransactionType;
	private String reqTransactionUuid;
	private String reqProfileId;
	private String transactionId;
	private List<Link> links;

	public CyberSourceCardPayments() {

	}

	public String getAuthAmount() {
		return authAmount;
	}

	public void setAuthAmount(String authAmount) {
		this.authAmount = authAmount;
	}

	public String getAuthAvsCode() {
		return authAvsCode;
	}

	public void setAuthAvsCode(String authAvsCode) {
		this.authAvsCode = authAvsCode;
	}

	public String getAuthAvsCodeRaw() {
		return authAvsCodeRaw;
	}

	public void setAuthAvsCodeRaw(String authAvsCodeRaw) {
		this.authAvsCodeRaw = authAvsCodeRaw;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getAuthCvResult() {
		return authCvResult;
	}

	public void setAuthCvResult(String authCvResult) {
		this.authCvResult = authCvResult;
	}

	public String getAuthResponse() {
		return authResponse;
	}

	public void setAuthResponse(String authResponse) {
		this.authResponse = authResponse;
	}

	public String getAuthTime() {
		return authTime;
	}

	public void setAuthTime(String authTime) {
		this.authTime = authTime;
	}

	public String getAuthTransRefNo() {
		return authTransRefNo;
	}

	public void setAuthTransRefNo(String authTransRefNo) {
		this.authTransRefNo = authTransRefNo;
	}

	public String getBillTransRefNo() {
		return billTransRefNo;
	}

	public void setBillTransRefNo(String billTransRefNo) {
		this.billTransRefNo = billTransRefNo;
	}

	public CyberSourceDecision getDecision() {
		return decision;
	}

	public void setDecision(CyberSourceDecision decision) {
		this.decision = decision;
	}

	public String getInvalidFields() {
		return invalidFields;
	}

	public void setInvalidFields(String invalidFields) {
		this.invalidFields = invalidFields;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPayerAuthenticationCavv() {
		return payerAuthenticationCavv;
	}

	public void setPayerAuthenticationCavv(String payerAuthenticationCavv) {
		this.payerAuthenticationCavv = payerAuthenticationCavv;
	}

	public String getPayerAuthenticationEci() {
		return payerAuthenticationEci;
	}

	public void setPayerAuthenticationEci(String payerAuthenticationEci) {
		this.payerAuthenticationEci = payerAuthenticationEci;
	}

	public String getPayerAuthenticationProofXml() {
		return payerAuthenticationProofXml;
	}

	public void setPayerAuthenticationProofXml(String payerAuthenticationProofXml) {
		this.payerAuthenticationProofXml = payerAuthenticationProofXml;
	}

	public String getPayerAuthenticationUad() {
		return payerAuthenticationUad;
	}

	public void setPayerAuthenticationUad(String payerAuthenticationUad) {
		this.payerAuthenticationUad = payerAuthenticationUad;
	}

	public String getPayerAuthenticationUci() {
		return payerAuthenticationUci;
	}

	public void setPayerAuthenticationUci(String payerAuthenticationUci) {
		this.payerAuthenticationUci = payerAuthenticationUci;
	}

	public String getPayerAuthenticationXid() {
		return payerAuthenticationXid;
	}

	public void setPayerAuthenticationXid(String payerAuthenticationXid) {
		this.payerAuthenticationXid = payerAuthenticationXid;
	}

	public String getPaymentToken() {
		return paymentToken;
	}

	public void setPaymentToken(String paymentToken) {
		this.paymentToken = paymentToken;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getReqAccessKey() {
		return reqAccessKey;
	}

	public void setReqAccessKey(String reqAccessKey) {
		this.reqAccessKey = reqAccessKey;
	}

	public String getReqAmount() {
		return reqAmount;
	}

	public void setReqAmount(String reqAmount) {
		this.reqAmount = reqAmount;
	}

	public String getReqBillPayment() {
		return reqBillPayment;
	}

	public void setReqBillPayment(String reqBillPayment) {
		this.reqBillPayment = reqBillPayment;
	}

	public String getReqBillToAddressCity() {
		return reqBillToAddressCity;
	}

	public void setReqBillToAddressCity(String reqBillToAddressCity) {
		this.reqBillToAddressCity = reqBillToAddressCity;
	}

	public String getReqBillToAddressCountry() {
		return reqBillToAddressCountry;
	}

	public void setReqBillToAddressCountry(String reqBillToAddressCountry) {
		this.reqBillToAddressCountry = reqBillToAddressCountry;
	}

	public String getReqBillToAddressLine1() {
		return reqBillToAddressLine1;
	}

	public void setReqBillToAddressLine1(String reqBillToAddressLine1) {
		this.reqBillToAddressLine1 = reqBillToAddressLine1;
	}

	public String getReqBillToAddressLine2() {
		return reqBillToAddressLine2;
	}

	public void setReqBillToAddressLine2(String reqBillToAddressLine2) {
		this.reqBillToAddressLine2 = reqBillToAddressLine2;
	}

	public String getReqBillToAddressPostalCode() {
		return reqBillToAddressPostalCode;
	}

	public void setReqBillToAddressPostalCode(String reqBillToAddressPostalCode) {
		this.reqBillToAddressPostalCode = reqBillToAddressPostalCode;
	}

	public String getReqBillToCompanyName() {
		return reqBillToCompanyName;
	}

	public void setReqBillToCompanyName(String reqBillToCompanyName) {
		this.reqBillToCompanyName = reqBillToCompanyName;
	}

	public String getReqBillToEmail() {
		return reqBillToEmail;
	}

	public void setReqBillToEmail(String reqBillToEmail) {
		this.reqBillToEmail = reqBillToEmail;
	}

	public String getReqBillToForename() {
		return reqBillToForename;
	}

	public void setReqBillToForename(String reqBillToForename) {
		this.reqBillToForename = reqBillToForename;
	}

	public String getReqBillToPhone() {
		return reqBillToPhone;
	}

	public void setReqBillToPhone(String reqBillToPhone) {
		this.reqBillToPhone = reqBillToPhone;
	}

	public String getReqBillToSurname() {
		return reqBillToSurname;
	}

	public void setReqBillToSurname(String reqBillToSurname) {
		this.reqBillToSurname = reqBillToSurname;
	}

	public String getReqCardExpiryDate() {
		return reqCardExpiryDate;
	}

	public void setReqCardExpiryDate(String reqCardExpiryDate) {
		this.reqCardExpiryDate = reqCardExpiryDate;
	}

	public String getReqCardNumber() {
		return reqCardNumber;
	}

	public void setReqCardNumber(String reqCardNumber) {
		this.reqCardNumber = reqCardNumber;
	}

	public String getReqCardType() {
		return reqCardType;
	}

	public void setReqCardType(String reqCardType) {
		this.reqCardType = reqCardType;
	}

	public String getReqCompanyTaxId() {
		return reqCompanyTaxId;
	}

	public void setReqCompanyTaxId(String reqCompanyTaxId) {
		this.reqCompanyTaxId = reqCompanyTaxId;
	}

	public String getReqCompleteRoute() {
		return reqCompleteRoute;
	}

	public void setReqCompleteRoute(String reqCompleteRoute) {
		this.reqCompleteRoute = reqCompleteRoute;
	}

	public String getReqConsumerId() {
		return reqConsumerId;
	}

	public void setReqConsumerId(String reqConsumerId) {
		this.reqConsumerId = reqConsumerId;
	}

	public String getReqCurrency() {
		return reqCurrency;
	}

	public void setReqCurrency(String reqCurrency) {
		this.reqCurrency = reqCurrency;
	}

	public String getReqCustomerIpAddress() {
		return reqCustomerIpAddress;
	}

	public void setReqCustomerIpAddress(String reqCustomerIpAddress) {
		this.reqCustomerIpAddress = reqCustomerIpAddress;
	}

	public String getReqDateOfBirth() {
		return reqDateOfBirth;
	}

	public void setReqDateOfBirth(String reqDateOfBirth) {
		this.reqDateOfBirth = reqDateOfBirth;
	}

	public String getReqDebitIndicator() {
		return reqDebitIndicator;
	}

	public void setReqDebitIndicator(String reqDebitIndicator) {
		this.reqDebitIndicator = reqDebitIndicator;
	}

	public String getReqDepatureTime() {
		return reqDepatureTime;
	}

	public void setReqDepatureTime(String reqDepatureTime) {
		this.reqDepatureTime = reqDepatureTime;
	}

	public String getReqDeviceFingerprintId() {
		return reqDeviceFingerprintId;
	}

	public void setReqDeviceFingerprintId(String reqDeviceFingerprintId) {
		this.reqDeviceFingerprintId = reqDeviceFingerprintId;
	}

	public String getReqPaymentToken() {
		return reqPaymentToken;
	}

	public void setReqPaymentToken(String reqPaymentToken) {
		this.reqPaymentToken = reqPaymentToken;
	}

	public String getReqPaymentTokenComments() {
		return reqPaymentTokenComments;
	}

	public void setReqPaymentTokenComments(String reqPaymentTokenComments) {
		this.reqPaymentTokenComments = reqPaymentTokenComments;
	}

	public String getReqTransactionType() {
		return reqTransactionType;
	}

	public void setReqTransactionType(String reqTransactionType) {
		this.reqTransactionType = reqTransactionType;
	}

	public String getReqTransactionUuid() {
		return reqTransactionUuid;
	}

	public void setReqTransactionUuid(String reqTransactionUuid) {
		this.reqTransactionUuid = reqTransactionUuid;
	}

	public String getReqProfileId() {
		return reqProfileId;
	}

	public void setReqProfileId(String reqProfileId) {
		this.reqProfileId = reqProfileId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "CyberSourceCardPayments [authAmount=" + authAmount + ", authAvsCode=" + authAvsCode
				+ ", authAvsCodeRaw=" + authAvsCodeRaw + ", authCode=" + authCode + ", authCvResult=" + authCvResult
				+ ", authResponse=" + authResponse + ", authTime=" + authTime + ", authTransRefNo=" + authTransRefNo
				+ ", billTransRefNo=" + billTransRefNo + ", decision=" + decision + ", invalidFields=" + invalidFields
				+ ", message=" + message + ", payerAuthenticationCavv=" + payerAuthenticationCavv
				+ ", payerAuthenticationEci=" + payerAuthenticationEci + ", payerAuthenticationProofXml="
				+ payerAuthenticationProofXml + ", payerAuthenticationUad=" + payerAuthenticationUad
				+ ", payerAuthenticationUci=" + payerAuthenticationUci + ", payerAuthenticationXid="
				+ payerAuthenticationXid + ", paymentToken=" + paymentToken + ", reasonCode=" + reasonCode
				+ ", reqAccessKey=" + reqAccessKey + ", reqAmount=" + reqAmount + ", reqBillPayment=" + reqBillPayment
				+ ", reqBillToAddressCity=" + reqBillToAddressCity + ", reqBillToAddressCountry="
				+ reqBillToAddressCountry + ", reqBillToAddressLine1=" + reqBillToAddressLine1
				+ ", reqBillToAddressLine2=" + reqBillToAddressLine2 + ", reqBillToAddressPostalCode="
				+ reqBillToAddressPostalCode + ", reqBillToCompanyName=" + reqBillToCompanyName + ", reqBillToEmail="
				+ reqBillToEmail + ", reqBillToForename=" + reqBillToForename + ", reqBillToPhone=" + reqBillToPhone
				+ ", reqBillToSurname=" + reqBillToSurname + ", reqCardExpiryDate=" + reqCardExpiryDate
				+ ", reqCardNumber=" + reqCardNumber + ", reqCardType=" + reqCardType + ", reqCompanyTaxId="
				+ reqCompanyTaxId + ", reqCompleteRoute=" + reqCompleteRoute + ", reqConsumerId=" + reqConsumerId
				+ ", reqCurrency=" + reqCurrency + ", reqCustomerIpAddress=" + reqCustomerIpAddress
				+ ", reqDateOfBirth=" + reqDateOfBirth + ", reqDebitIndicator=" + reqDebitIndicator
				+ ", reqDepatureTime=" + reqDepatureTime + ", reqDeviceFingerprintId=" + reqDeviceFingerprintId
				+ ", reqPaymentToken=" + reqPaymentToken + ", reqPaymentTokenComments=" + reqPaymentTokenComments
				+ ", reqTransactionType=" + reqTransactionType + ", reqTransactionUuid=" + reqTransactionUuid
				+ ", reqProfileId=" + reqProfileId + ", transactionId=" + transactionId + ", links=" + links + "]";
	}

}
