package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Products {

	private List<Product> pageOfResults;
	private Long totalNumberOfResults;
	private List<Link> links;

	public Products() {

	}

	public List<Product> getPageOfResults() {
		return pageOfResults;
	}

	public void setPageOfResults(List<Product> pageOfResults) {
		this.pageOfResults = pageOfResults;
	}

	public Long getTotalNumberOfResults() {
		return totalNumberOfResults;
	}

	public void setTotalNumberOfResults(Long totalNumberOfResults) {
		this.totalNumberOfResults = totalNumberOfResults;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "Products [pageOfResults=" + pageOfResults + ", totalNumberOfResults=" + totalNumberOfResults
				+ ", links=" + links + "]";
	}

}
