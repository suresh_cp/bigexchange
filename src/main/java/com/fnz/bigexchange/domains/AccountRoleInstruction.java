package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.InstructionStatus;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountRoleInstruction {

	private Long id;
	private Long workItemId;
	private InstructionStatus status;
	private String accountHierarchyId;
	private List<Link> links;

	public AccountRoleInstruction() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getWorkItemId() {
		return workItemId;
	}

	public void setWorkItemId(Long workItemId) {
		this.workItemId = workItemId;
	}

	public InstructionStatus getStatus() {
		return status;
	}

	public void setStatus(InstructionStatus status) {
		this.status = status;
	}

	public String getAccountHierarchyId() {
		return accountHierarchyId;
	}

	public void setAccountHierarchyId(String accountHierarchyId) {
		this.accountHierarchyId = accountHierarchyId;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "AccountRoleInstruction [id=" + id + ", workItemId=" + workItemId + ", status=" + status
				+ ", accountHierarchyId=" + accountHierarchyId + ", links=" + links + "]";
	}

}
