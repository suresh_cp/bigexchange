package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.Frequency;
import com.fnz.bigexchange.enums.InstructionStatus;
import com.fnz.bigexchange.enums.PaymentSource;
import com.fnz.bigexchange.enums.RegularDepositMethod;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegularDepositInstruction {

	private Long id;
	private MoneyContract amount;
	private Long bankAccountId;
	private String bankAccountIdType;
	private Date startDate;
	private Date endDate;
	private Frequency frequency;
	private InstructionStatus status;
	private String hierarchyId;
	private Boolean taxRelief;
	private RegularDepositMethod method;
	private Boolean notifyWhenFundsReceived;
	private PaymentSource paymentSource;
	private List<AcceptedContractualTerms> acceptedContractualTerms;
	private List<Link> links;

	public RegularDepositInstruction() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MoneyContract getAmount() {
		return amount;
	}

	public void setAmount(MoneyContract amount) {
		this.amount = amount;
	}

	public Long getBankAccountId() {
		return bankAccountId;
	}

	public void setBankAccountId(Long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}

	public String getBankAccountIdType() {
		return bankAccountIdType;
	}

	public void setBankAccountIdType(String bankAccountIdType) {
		this.bankAccountIdType = bankAccountIdType;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	public InstructionStatus getStatus() {
		return status;
	}

	public void setStatus(InstructionStatus status) {
		this.status = status;
	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public Boolean getTaxRelief() {
		return taxRelief;
	}

	public void setTaxRelief(Boolean taxRelief) {
		this.taxRelief = taxRelief;
	}

	public RegularDepositMethod getMethod() {
		return method;
	}

	public void setMethod(RegularDepositMethod method) {
		this.method = method;
	}

	public Boolean getNotifyWhenFundsReceived() {
		return notifyWhenFundsReceived;
	}

	public void setNotifyWhenFundsReceived(Boolean notifyWhenFundsReceived) {
		this.notifyWhenFundsReceived = notifyWhenFundsReceived;
	}

	public PaymentSource getPaymentSource() {
		return paymentSource;
	}

	public void setPaymentSource(PaymentSource paymentSource) {
		this.paymentSource = paymentSource;
	}

	public List<AcceptedContractualTerms> getAcceptedContractualTerms() {
		return acceptedContractualTerms;
	}

	public void setAcceptedContractualTerms(List<AcceptedContractualTerms> acceptedContractualTerms) {
		this.acceptedContractualTerms = acceptedContractualTerms;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "RegularDepositInstruction [id=" + id + ", amount=" + amount + ", bankAccountId=" + bankAccountId
				+ ", bankAccountIdType=" + bankAccountIdType + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", frequency=" + frequency + ", status=" + status + ", hierarchyId=" + hierarchyId + ", taxRelief="
				+ taxRelief + ", method=" + method + ", notifyWhenFundsReceived=" + notifyWhenFundsReceived
				+ ", paymentSource=" + paymentSource + ", acceptedContractualTerms=" + acceptedContractualTerms
				+ ", links=" + links + "]";
	}

}
