package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.ContractualTermType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ContractualTerm {

	private Long Id;
	private ContractualTermType Type;
	private String Language;
	private String Description;
	private Boolean Accepted;
	private Date DateAccepted;
	private Date ValidFrom;
	private Date ValidTo;
	private List<Link> Links;

	public ContractualTerm() {

	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public ContractualTermType getType() {
		return Type;
	}

	public void setType(ContractualTermType type) {
		Type = type;
	}

	public String getLanguage() {
		return Language;
	}

	public void setLanguage(String language) {
		Language = language;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public Boolean getAccepted() {
		return Accepted;
	}

	public void setAccepted(Boolean accepted) {
		Accepted = accepted;
	}

	public Date getDateAccepted() {
		return DateAccepted;
	}

	public void setDateAccepted(Date dateAccepted) {
		DateAccepted = dateAccepted;
	}

	public Date getValidFrom() {
		return ValidFrom;
	}

	public void setValidFrom(Date validFrom) {
		ValidFrom = validFrom;
	}

	public Date getValidTo() {
		return ValidTo;
	}

	public void setValidTo(Date validTo) {
		ValidTo = validTo;
	}

	public List<Link> getLinks() {
		return Links;
	}

	public void setLinks(List<Link> links) {
		Links = links;
	}

	@Override
	public String toString() {
		return "ContractualTerm [Id=" + Id + ", Type=" + Type + ", Language=" + Language + ", Description="
				+ Description + ", Accepted=" + Accepted + ", DateAccepted=" + DateAccepted + ", ValidFrom=" + ValidFrom
				+ ", ValidTo=" + ValidTo + ", Links=" + Links + "]";
	}

}
