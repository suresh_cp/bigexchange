package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerRoleInstructionAddresses {

	private List<CustomerRoleInstructionAddress> results;
	private List<Link> links;

	public CustomerRoleInstructionAddresses() {

	}

	public List<CustomerRoleInstructionAddress> getResults() {
		return results;
	}

	public void setResults(List<CustomerRoleInstructionAddress> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "CustomerRoleInstructionAddresses [results=" + results + ", links=" + links + "]";
	}

}
