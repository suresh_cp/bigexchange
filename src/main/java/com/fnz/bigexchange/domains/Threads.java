package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Threads {

	private List<Thread> pageOfResults;
	private Long totalNumberOfResults;
	private List<Link> links;

	public Threads() {

	}

	public List<Thread> getPageOfResults() {
		return pageOfResults;
	}

	public void setPageOfResults(List<Thread> pageOfResults) {
		this.pageOfResults = pageOfResults;
	}

	public Long getTotalNumberOfResults() {
		return totalNumberOfResults;
	}

	public void setTotalNumberOfResults(Long totalNumberOfResults) {
		this.totalNumberOfResults = totalNumberOfResults;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "Threads [pageOfResults=" + pageOfResults + ", totalNumberOfResults=" + totalNumberOfResults + ", links="
				+ links + "]";
	}

}
