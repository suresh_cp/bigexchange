package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.Frequency;
import com.fnz.bigexchange.enums.RegularDepositMethod;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegularBuyDeposit {

	private Long id;
	private MoneyContract amount;
	private Long bankAccountId;
	private Date startDate;
	private Date nextDate;
	private Date endDate;
	private Frequency frequency;
	private String hierarchyId;
	private Boolean taxRelief;
	private RegularDepositMethod method;
	private List<Link> links;

	public RegularBuyDeposit() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MoneyContract getAmount() {
		return amount;
	}

	public void setAmount(MoneyContract amount) {
		this.amount = amount;
	}

	public Long getBankAccountId() {
		return bankAccountId;
	}

	public void setBankAccountId(Long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getNextDate() {
		return nextDate;
	}

	public void setNextDate(Date nextDate) {
		this.nextDate = nextDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public Boolean getTaxRelief() {
		return taxRelief;
	}

	public void setTaxRelief(Boolean taxRelief) {
		this.taxRelief = taxRelief;
	}

	public RegularDepositMethod getMethod() {
		return method;
	}

	public void setMethod(RegularDepositMethod method) {
		this.method = method;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "RegularBuyDeposit [id=" + id + ", amount=" + amount + ", bankAccountId=" + bankAccountId
				+ ", startDate=" + startDate + ", nextDate=" + nextDate + ", endDate=" + endDate + ", frequency="
				+ frequency + ", hierarchyId=" + hierarchyId + ", taxRelief=" + taxRelief + ", method=" + method
				+ ", links=" + links + "]";
	}

}
