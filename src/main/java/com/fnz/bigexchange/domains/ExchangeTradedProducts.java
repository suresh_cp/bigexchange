package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ExchangeTradedProducts {

	private List<ExchangeTradedProduct> pageOfResults;
	private Long TotalNumberOfResults;
	private List<Link> Links;

	public ExchangeTradedProducts() {

	}

	public List<ExchangeTradedProduct> getPageOfResults() {
		return pageOfResults;
	}

	public void setPageOfResults(List<ExchangeTradedProduct> pageOfResults) {
		this.pageOfResults = pageOfResults;
	}

	public Long getTotalNumberOfResults() {
		return TotalNumberOfResults;
	}

	public void setTotalNumberOfResults(Long totalNumberOfResults) {
		TotalNumberOfResults = totalNumberOfResults;
	}

	public List<Link> getLinks() {
		return Links;
	}

	public void setLinks(List<Link> links) {
		Links = links;
	}

	@Override
	public String toString() {
		return "ExchangeTradedProducts [pageOfResults=" + pageOfResults + ", TotalNumberOfResults="
				+ TotalNumberOfResults + ", Links=" + Links + "]";
	}

}
