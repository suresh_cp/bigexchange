package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.ContactMethod;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AllowedContactMethod {

	private ContactMethod contactType1;
	private ContactMethod contactType2;

	public AllowedContactMethod() {

	}

	public ContactMethod getContactType1() {
		return contactType1;
	}

	public void setContactType1(ContactMethod contactType1) {
		this.contactType1 = contactType1;
	}

	public ContactMethod getContactType2() {
		return contactType2;
	}

	public void setContactType2(ContactMethod contactType2) {
		this.contactType2 = contactType2;
	}

	@Override
	public String toString() {
		return "AllowedContactMethod [contactType1=" + contactType1 + ", contactType2=" + contactType2 + "]";
	}

}
