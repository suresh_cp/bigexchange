package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Nationality {

	private String countryCode;
	private String nationality;

	public Nationality(String countryCode, String nationality) {
		super();
		this.countryCode = countryCode;
		this.nationality = nationality;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public String toString() {
		return "Nationality [countryCode=" + countryCode + ", nationality=" + nationality + "]";
	}

}
