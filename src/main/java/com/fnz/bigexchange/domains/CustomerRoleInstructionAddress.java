package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.CustomerRoleAddressType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerRoleInstructionAddress {

	private Long id;
	private Long addressBookEntryId;
	private Long addressBookInstructionId;
	private CustomerRoleAddressType type;
	private List<Link> links;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAddressBookEntryId() {
		return addressBookEntryId;
	}

	public void setAddressBookEntryId(Long addressBookEntryId) {
		this.addressBookEntryId = addressBookEntryId;
	}

	public Long getAddressBookInstructionId() {
		return addressBookInstructionId;
	}

	public void setAddressBookInstructionId(Long addressBookInstructionId) {
		this.addressBookInstructionId = addressBookInstructionId;
	}

	public CustomerRoleAddressType getType() {
		return type;
	}

	public void setType(CustomerRoleAddressType type) {
		this.type = type;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "CustomerRoleInstructionAddress [id=" + id + ", addressBookEntryId=" + addressBookEntryId
				+ ", addressBookInstructionId=" + addressBookInstructionId + ", type=" + type + ", links=" + links
				+ "]";
	}

}
