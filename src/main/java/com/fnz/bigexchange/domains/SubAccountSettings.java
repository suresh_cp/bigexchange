package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubAccountSettings {

	private Boolean sells;
	private Boolean regularSells;
	private Boolean buys;
	private Boolean regularBuys;
	private Boolean adHocDeposits;
	private Boolean regularDeposits;
	private Boolean regularWithdrawals;
	private Boolean adHocWithdrawals;
	private Boolean payrollContributions;
	private Boolean quoteAndDeal;
	private Boolean transfersOn;
	private List<Link> links;

	public SubAccountSettings() {

	}

	public Boolean getSells() {
		return sells;
	}

	public void setSells(Boolean sells) {
		this.sells = sells;
	}

	public Boolean getRegularSells() {
		return regularSells;
	}

	public void setRegularSells(Boolean regularSells) {
		this.regularSells = regularSells;
	}

	public Boolean getBuys() {
		return buys;
	}

	public void setBuys(Boolean buys) {
		this.buys = buys;
	}

	public Boolean getRegularBuys() {
		return regularBuys;
	}

	public void setRegularBuys(Boolean regularBuys) {
		this.regularBuys = regularBuys;
	}

	public Boolean getAdHocDeposits() {
		return adHocDeposits;
	}

	public void setAdHocDeposits(Boolean adHocDeposits) {
		this.adHocDeposits = adHocDeposits;
	}

	public Boolean getRegularDeposits() {
		return regularDeposits;
	}

	public void setRegularDeposits(Boolean regularDeposits) {
		this.regularDeposits = regularDeposits;
	}

	public Boolean getRegularWithdrawals() {
		return regularWithdrawals;
	}

	public void setRegularWithdrawals(Boolean regularWithdrawals) {
		this.regularWithdrawals = regularWithdrawals;
	}

	public Boolean getAdHocWithdrawals() {
		return adHocWithdrawals;
	}

	public void setAdHocWithdrawals(Boolean adHocWithdrawals) {
		this.adHocWithdrawals = adHocWithdrawals;
	}

	public Boolean getPayrollContributions() {
		return payrollContributions;
	}

	public void setPayrollContributions(Boolean payrollContributions) {
		this.payrollContributions = payrollContributions;
	}

	public Boolean getQuoteAndDeal() {
		return quoteAndDeal;
	}

	public void setQuoteAndDeal(Boolean quoteAndDeal) {
		this.quoteAndDeal = quoteAndDeal;
	}

	public Boolean getTransfersOn() {
		return transfersOn;
	}

	public void setTransfersOn(Boolean transfersOn) {
		this.transfersOn = transfersOn;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "SubAccountSettings [sells=" + sells + ", regularSells=" + regularSells + ", buys=" + buys
				+ ", regularBuys=" + regularBuys + ", adHocDeposits=" + adHocDeposits + ", regularDeposits="
				+ regularDeposits + ", regularWithdrawals=" + regularWithdrawals + ", adHocWithdrawals="
				+ adHocWithdrawals + ", payrollContributions=" + payrollContributions + ", quoteAndDeal=" + quoteAndDeal
				+ ", transfersOn=" + transfersOn + ", links=" + links + "]";
	}

}
