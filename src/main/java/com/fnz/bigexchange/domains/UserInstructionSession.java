package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInstructionSession {

	private String token; // type guide
	private Date startTime;
	private Date EndTime;
	private Boolean FirstTimeLogin;
	private List<Link> links;

	public UserInstructionSession() {

	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return EndTime;
	}

	public void setEndTime(Date endTime) {
		EndTime = endTime;
	}

	public Boolean getFirstTimeLogin() {
		return FirstTimeLogin;
	}

	public void setFirstTimeLogin(Boolean firstTimeLogin) {
		FirstTimeLogin = firstTimeLogin;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "UserInstructionSession [token=" + token + ", startTime=" + startTime + ", EndTime=" + EndTime
				+ ", FirstTimeLogin=" + FirstTimeLogin + ", links=" + links + "]";
	}

}
