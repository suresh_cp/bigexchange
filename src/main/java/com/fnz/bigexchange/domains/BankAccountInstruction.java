package com.fnz.bigexchange.domains;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.Currency;
import com.fnz.bigexchange.enums.InstructionStatus;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BankAccountInstruction {

	private Long id;
	private InstructionStatus status;
	private Long workItemId;
	private String ownerHierarchyId;
	private BankDetails bankDetails;
	private String accountName;
	private String accountNumber;
	private BankCodes bankCodes;
	private String iban;
	private String bic;
	private String rollNumber;
	private Currency currency;
	private String owner;
	private Boolean nominatedForWithdrawal;
	private Boolean nominatedForDirectDebit;
	private Boolean isProviderBankAccount;
	private List<Link> links;

	public BankAccountInstruction() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public InstructionStatus getStatus() {
		return status;
	}

	public void setStatus(InstructionStatus status) {
		this.status = status;
	}

	public Long getWorkItemId() {
		return workItemId;
	}

	public void setWorkItemId(Long workItemId) {
		this.workItemId = workItemId;
	}

	public String getOwnerHierarchyId() {
		return ownerHierarchyId;
	}

	public void setOwnerHierarchyId(String ownerHierarchyId) {
		this.ownerHierarchyId = ownerHierarchyId;
	}

	public BankDetails getBankDetails() {
		return bankDetails;
	}

	public void setBankDetails(BankDetails bankDetails) {
		this.bankDetails = bankDetails;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BankCodes getBankCodes() {
		return bankCodes;
	}

	public void setBankCodes(BankCodes bankCodes) {
		this.bankCodes = bankCodes;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Boolean getNominatedForWithdrawal() {
		return nominatedForWithdrawal;
	}

	public void setNominatedForWithdrawal(Boolean nominatedForWithdrawal) {
		this.nominatedForWithdrawal = nominatedForWithdrawal;
	}

	public Boolean getNominatedForDirectDebit() {
		return nominatedForDirectDebit;
	}

	public void setNominatedForDirectDebit(Boolean nominatedForDirectDebit) {
		this.nominatedForDirectDebit = nominatedForDirectDebit;
	}

	public Boolean getIsProviderBankAccount() {
		return isProviderBankAccount;
	}

	public void setIsProviderBankAccount(Boolean isProviderBankAccount) {
		this.isProviderBankAccount = isProviderBankAccount;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "BankAccountInstruction [id=" + id + ", status=" + status + ", workItemId=" + workItemId
				+ ", ownerHierarchyId=" + ownerHierarchyId + ", bankDetails=" + bankDetails + ", accountName="
				+ accountName + ", accountNumber=" + accountNumber + ", bankCodes=" + bankCodes + ", iban=" + iban
				+ ", bic=" + bic + ", rollNumber=" + rollNumber + ", currency=" + currency + ", owner=" + owner
				+ ", nominatedForWithdrawal=" + nominatedForWithdrawal + ", nominatedForDirectDebit="
				+ nominatedForDirectDebit + ", isProviderBankAccount=" + isProviderBankAccount + ", links=" + links
				+ "]";
	}

}
