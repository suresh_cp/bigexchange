package com.fnz.bigexchange.domains;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductWrapperContributionLimits {

	private MinimumSingleContribution minimumSingleContribution;
	private MinimumRegularContribution minimumRegularContribution;
	private MinimumRegularContributionsByFrequencyCollection minimumRegularContributionsByFrequency;
	private MinimumWithdrawalAmount minimumWithdrawalAmount;
	private List<Link> links;

	public ProductWrapperContributionLimits() {

	}

	public MinimumSingleContribution getMinimumSingleContribution() {
		return minimumSingleContribution;
	}

	public void setMinimumSingleContribution(MinimumSingleContribution minimumSingleContribution) {
		this.minimumSingleContribution = minimumSingleContribution;
	}

	public MinimumRegularContribution getMinimumRegularContribution() {
		return minimumRegularContribution;
	}

	public void setMinimumRegularContribution(MinimumRegularContribution minimumRegularContribution) {
		this.minimumRegularContribution = minimumRegularContribution;
	}

	public MinimumRegularContributionsByFrequencyCollection getMinimumRegularContributionsByFrequency() {
		return minimumRegularContributionsByFrequency;
	}

	public void setMinimumRegularContributionsByFrequency(
			MinimumRegularContributionsByFrequencyCollection minimumRegularContributionsByFrequency) {
		this.minimumRegularContributionsByFrequency = minimumRegularContributionsByFrequency;
	}

	public MinimumWithdrawalAmount getMinimumWithdrawalAmount() {
		return minimumWithdrawalAmount;
	}

	public void setMinimumWithdrawalAmount(MinimumWithdrawalAmount minimumWithdrawalAmount) {
		this.minimumWithdrawalAmount = minimumWithdrawalAmount;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "ProductWrapperContributionLimits [minimumSingleContribution=" + minimumSingleContribution
				+ ", minimumRegularContribution=" + minimumRegularContribution
				+ ", minimumRegularContributionsByFrequency=" + minimumRegularContributionsByFrequency
				+ ", minimumWithdrawalAmount=" + minimumWithdrawalAmount + ", links=" + links + "]";
	}

}
