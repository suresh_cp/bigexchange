package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionSettings {

	private Long defaultSessionTimeoutInMinutes;

	public SessionSettings() {

	}

	public Long getDefaultSessionTimeoutInMinutes() {
		return defaultSessionTimeoutInMinutes;
	}

	public void setDefaultSessionTimeoutInMinutes(Long defaultSessionTimeoutInMinutes) {
		this.defaultSessionTimeoutInMinutes = defaultSessionTimeoutInMinutes;
	}

	@Override
	public String toString() {
		return "SessionSettings [defaultSessionTimeoutInMinutes=" + defaultSessionTimeoutInMinutes + "]";
	}

}
