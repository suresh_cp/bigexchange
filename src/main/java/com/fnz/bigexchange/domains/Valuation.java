package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Valuation {

	private String label;
	private Double allocation;
	private MoneyContract value;
	private List<Link> links;

	public Valuation() {

	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Double getAllocation() {
		return allocation;
	}

	public void setAllocation(Double allocation) {
		this.allocation = allocation;
	}

	public MoneyContract getValue() {
		return value;
	}

	public void setValue(MoneyContract value) {
		this.value = value;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "Valuation [label=" + label + ", allocation=" + allocation + ", value=" + value + ", links=" + links
				+ "]";
	}

}
