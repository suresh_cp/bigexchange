package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.CrownRating;
import com.fnz.bigexchange.enums.Currency;
import com.fnz.bigexchange.enums.DividendReinvestment;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Fund {

	private Long productId;
	private String name;
	private String description;
	private Currency currency;
	private FundIdentifiers fundIdentifiers;
	private Price price;
	private Double yield;
	private Double totalExpenseRatio;
	private String AssetClass;
	private HistoricalPerformance historicalPerformance;
	private Boolean buyable;
	private Boolean sellable;
	private Boolean transferrable;
	private List<String> categories;
	private FundCharges charges;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
	private Date nextCutOffTime; // it is empty need to calculate
	private Date nextCutOffDate;
	private CrownRating crownRating;
	private Long FundManagerId;
	private List<FundRating> fundRatings;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "hh:mm:ss")
	private Date nextValuationTime; // need to be calculated
	private Date nextValuationDate;
	private List<InstrumentGroup> instrumentGroups;
	private DividendReinvestment dividendReinvestment;
	private List<Link> links;

	public Fund() {

	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public FundIdentifiers getFundIdentifiers() {
		return fundIdentifiers;
	}

	public void setFundIdentifiers(FundIdentifiers fundIdentifiers) {
		this.fundIdentifiers = fundIdentifiers;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public Double getYield() {
		return yield;
	}

	public void setYield(Double yield) {
		this.yield = yield;
	}

	public Double getTotalExpenseRatio() {
		return totalExpenseRatio;
	}

	public void setTotalExpenseRatio(Double totalExpenseRatio) {
		this.totalExpenseRatio = totalExpenseRatio;
	}

	public String getAssetClass() {
		return AssetClass;
	}

	public void setAssetClass(String assetClass) {
		AssetClass = assetClass;
	}

	public HistoricalPerformance getHistoricalPerformance() {
		return historicalPerformance;
	}

	public void setHistoricalPerformance(HistoricalPerformance historicalPerformance) {
		this.historicalPerformance = historicalPerformance;
	}

	public Boolean getBuyable() {
		return buyable;
	}

	public void setBuyable(Boolean buyable) {
		this.buyable = buyable;
	}

	public Boolean getSellable() {
		return sellable;
	}

	public void setSellable(Boolean sellable) {
		this.sellable = sellable;
	}

	public Boolean getTransferrable() {
		return transferrable;
	}

	public void setTransferrable(Boolean transferrable) {
		this.transferrable = transferrable;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public FundCharges getCharges() {
		return charges;
	}

	public void setCharges(FundCharges charges) {
		this.charges = charges;
	}

	public Date getNextCutOffTime() {
		return nextCutOffTime;
	}

	public void setNextCutOffTime(Date nextCutOffTime) {
		this.nextCutOffTime = nextCutOffTime;
	}

	public Date getNextCutOffDate() {
		return nextCutOffDate;
	}

	public void setNextCutOffDate(Date nextCutOffDate) {
		this.nextCutOffDate = nextCutOffDate;
	}

	public CrownRating getCrownRating() {
		return crownRating;
	}

	public void setCrownRating(CrownRating crownRating) {
		this.crownRating = crownRating;
	}

	public Long getFundManagerId() {
		return FundManagerId;
	}

	public void setFundManagerId(Long fundManagerId) {
		FundManagerId = fundManagerId;
	}

	public List<FundRating> getFundRatings() {
		return fundRatings;
	}

	public void setFundRatings(List<FundRating> fundRatings) {
		this.fundRatings = fundRatings;
	}

	public Date getNextValuationTime() {
		return nextValuationTime;
	}

	public void setNextValuationTime(Date nextValuationTime) {
		this.nextValuationTime = nextValuationTime;
	}

	public Date getNextValuationDate() {
		return nextValuationDate;
	}

	public void setNextValuationDate(Date nextValuationDate) {
		this.nextValuationDate = nextValuationDate;
	}

	public List<InstrumentGroup> getInstrumentGroups() {
		return instrumentGroups;
	}

	public void setInstrumentGroups(List<InstrumentGroup> instrumentGroups) {
		this.instrumentGroups = instrumentGroups;
	}

	public DividendReinvestment getDividendReinvestment() {
		return dividendReinvestment;
	}

	public void setDividendReinvestment(DividendReinvestment dividendReinvestment) {
		this.dividendReinvestment = dividendReinvestment;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "Fund [productId=" + productId + ", name=" + name + ", description=" + description + ", currency="
				+ currency + ", fundIdentifiers=" + fundIdentifiers + ", price=" + price + ", yield=" + yield
				+ ", totalExpenseRatio=" + totalExpenseRatio + ", AssetClass=" + AssetClass + ", historicalPerformance="
				+ historicalPerformance + ", buyable=" + buyable + ", sellable=" + sellable + ", transferrable="
				+ transferrable + ", categories=" + categories + ", charges=" + charges + ", nextCutOffTime="
				+ nextCutOffTime + ", nextCutOffDate=" + nextCutOffDate + ", crownRating=" + crownRating
				+ ", FundManagerId=" + FundManagerId + ", fundRatings=" + fundRatings + ", nextValuationTime="
				+ nextValuationTime + ", nextValuationDate=" + nextValuationDate + ", instrumentGroups="
				+ instrumentGroups + ", dividendReinvestment=" + dividendReinvestment + ", links=" + links + "]";
	}

}
