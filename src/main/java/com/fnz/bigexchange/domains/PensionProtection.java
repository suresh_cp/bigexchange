package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PensionProtection {

	private Enhanced enhanced;
	private Primary primary;
	private List<FixedIndividual> fixed;
	private List<FixedIndividual> individual;
	private List<LifetimeAllowanceEnhancements> lifetimeAllowanceEnhancements;
	private PensionSchemeSpecificProtection pensionSchemeSpecificProtection;

	public PensionProtection() {

	}

	public Enhanced getEnhancedObject() {
		return enhanced;
	}

	public void setEnhancedObject(Enhanced enhancedObject) {
		this.enhanced = enhancedObject;
	}

	public Primary getPrimaryObject() {
		return primary;
	}

	public void setPrimaryObject(Primary primaryObject) {
		this.primary = primaryObject;
	}

	public List<FixedIndividual> getFixed() {
		return fixed;
	}

	public void setFixed(List<FixedIndividual> fixed) {
		this.fixed = fixed;
	}

	public List<FixedIndividual> getIndividual() {
		return individual;
	}

	public void setIndividual(List<FixedIndividual> individual) {
		this.individual = individual;
	}

	public List<LifetimeAllowanceEnhancements> getLifetimeAllowanceEnhancements() {
		return lifetimeAllowanceEnhancements;
	}

	public void setLifetimeAllowanceEnhancements(List<LifetimeAllowanceEnhancements> lifetimeAllowanceEnhancements) {
		this.lifetimeAllowanceEnhancements = lifetimeAllowanceEnhancements;
	}

	public PensionSchemeSpecificProtection getPensionSchemeSpecificProtectionObject() {
		return pensionSchemeSpecificProtection;
	}

	public void setPensionSchemeSpecificProtectionObject(
			PensionSchemeSpecificProtection pensionSchemeSpecificProtectionObject) {
		this.pensionSchemeSpecificProtection = pensionSchemeSpecificProtectionObject;
	}

	@Override
	public String toString() {
		return "PensionProtection [enhancedObject=" + enhanced + ", primaryObject=" + primary + ", fixed=" + fixed
				+ ", individual=" + individual + ", lifetimeAllowanceEnhancements=" + lifetimeAllowanceEnhancements
				+ ", pensionSchemeSpecificProtectionObject=" + pensionSchemeSpecificProtection + "]";
	}

}
