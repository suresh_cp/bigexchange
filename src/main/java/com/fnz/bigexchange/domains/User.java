package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.UserStatus;
import com.fnz.bigexchange.enums.UserType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

	private Long id;
	private Name name;
	private UserStatus status;
	private String externalUserId;
	private Boolean isInternalUser;
	private UserType type;
	private List<Link> links;

	public User() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public UserStatus getStatus() {
		return status;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}

	public String getExternalUserId() {
		return externalUserId;
	}

	public void setExternalUserId(String externalUserId) {
		this.externalUserId = externalUserId;
	}

	public Boolean getIsInternalUser() {
		return isInternalUser;
	}

	public void setIsInternalUser(Boolean isInternalUser) {
		this.isInternalUser = isInternalUser;
	}

	public UserType getType() {
		return type;
	}

	public void setType(UserType type) {
		this.type = type;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", status=" + status + ", externalUserId=" + externalUserId
				+ ", isInternalUser=" + isInternalUser + ", type=" + type + ", links=" + links + "]";
	}

}
