package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.AccountStatus;
import com.fnz.bigexchange.enums.AccountType;
import com.fnz.bigexchange.enums.Currency;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Account {

	private String hierarchyId;
	private String accountId;
	private AccountName name;
	private AccountType type;
	private AccountStatus status;
	private Date dateCreated;
	private String externalAccountId;
	private Currency reportingCurrency;
	private List<Link> links;

	public Account() {

	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public AccountName getName() {
		return name;
	}

	public void setName(AccountName name) {
		this.name = name;
	}

	public AccountType getType() {
		return type;
	}

	public void setType(AccountType type) {
		this.type = type;
	}

	public AccountStatus getStatus() {
		return status;
	}

	public void setStatus(AccountStatus status) {
		this.status = status;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getExternalAccountId() {
		return externalAccountId;
	}

	public void setExternalAccountId(String externalAccountId) {
		this.externalAccountId = externalAccountId;
	}

	public Currency getReportingCurrency() {
		return reportingCurrency;
	}

	public void setReportingCurrency(Currency reportingCurrency) {
		this.reportingCurrency = reportingCurrency;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "Account [hierarchyId=" + hierarchyId + ", accountId=" + accountId + ", Name=" + name + ", type=" + type
				+ ", status=" + status + ", dateCreated=" + dateCreated + ", externalAccountId=" + externalAccountId
				+ ", reportingCurrency=" + reportingCurrency + ", Links=" + links + "]";
	}

}
