package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.Currency;
import com.fnz.bigexchange.enums.ExchangeTradedProductType;
import com.fnz.bigexchange.enums.ProductType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ExchangeTradedProduct {

	private String issuer;
	private ExchangeTradedProductType exchangeTradedProductType;
	private ExchangeTradedInstrumentPrices prices; // exchange tradede
	private String primaryExchangeCode;
	private Double volume;
	private String epic;
	private MorningstarRating morningstarRating;
	private Double ongoingChargesFigure;
	private HistoricalPerformance pastPerformance; // type historical performance
	private Long productId;
	private String name;
	private String sedol;
	private String isin;
	private String ric;
	private String cusip;
	private Currency currency;
	private String productCode;
	private String citiCode;
	private ProductType type;
	private String assetClass;
	private Sector sector;
	private Long riskLevelId;
	List<Link> links;

	public ExchangeTradedProduct() {

	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public ExchangeTradedProductType getExchangeTradedProductType() {
		return exchangeTradedProductType;
	}

	public void setExchangeTradedProductType(ExchangeTradedProductType exchangeTradedProductType) {
		this.exchangeTradedProductType = exchangeTradedProductType;
	}

	public ExchangeTradedInstrumentPrices getPrices() {
		return prices;
	}

	public void setPrices(ExchangeTradedInstrumentPrices prices) {
		this.prices = prices;
	}

	public String getPrimaryExchangeCode() {
		return primaryExchangeCode;
	}

	public void setPrimaryExchangeCode(String primaryExchangeCode) {
		this.primaryExchangeCode = primaryExchangeCode;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public String getEpic() {
		return epic;
	}

	public void setEpic(String epic) {
		this.epic = epic;
	}

	public MorningstarRating getMorningstarRating() {
		return morningstarRating;
	}

	public void setMorningstarRating(MorningstarRating morningstarRating) {
		this.morningstarRating = morningstarRating;
	}

	public Double getOngoingChargesFigure() {
		return ongoingChargesFigure;
	}

	public void setOngoingChargesFigure(Double ongoingChargesFigure) {
		this.ongoingChargesFigure = ongoingChargesFigure;
	}

	public HistoricalPerformance getPastPerformance() {
		return pastPerformance;
	}

	public void setPastPerformance(HistoricalPerformance pastPerformance) {
		this.pastPerformance = pastPerformance;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSedol() {
		return sedol;
	}

	public void setSedol(String sedol) {
		this.sedol = sedol;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin(String isin) {
		this.isin = isin;
	}

	public String getRic() {
		return ric;
	}

	public void setRic(String ric) {
		this.ric = ric;
	}

	public String getCusip() {
		return cusip;
	}

	public void setCusip(String cusip) {
		this.cusip = cusip;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getCitiCode() {
		return citiCode;
	}

	public void setCitiCode(String citiCode) {
		this.citiCode = citiCode;
	}

	public ProductType getType() {
		return type;
	}

	public void setType(ProductType type) {
		this.type = type;
	}

	public String getAssetClass() {
		return assetClass;
	}

	public void setAssetClass(String assetClass) {
		this.assetClass = assetClass;
	}

	public Sector getSector() {
		return sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

	public Long getRiskLevelId() {
		return riskLevelId;
	}

	public void setRiskLevelId(Long riskLevelId) {
		this.riskLevelId = riskLevelId;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "ExchangeTradedProduct [issuer=" + issuer + ", exchangeTradedProductType=" + exchangeTradedProductType
				+ ", prices=" + prices + ", primaryExchangeCode=" + primaryExchangeCode + ", volume=" + volume
				+ ", epic=" + epic + ", morningstarRating=" + morningstarRating + ", ongoingChargesFigure="
				+ ongoingChargesFigure + ", pastPerformance=" + pastPerformance + ", productId=" + productId + ", name="
				+ name + ", sedol=" + sedol + ", isin=" + isin + ", ric=" + ric + ", cusip=" + cusip + ", currency="
				+ currency + ", productCode=" + productCode + ", citiCode=" + citiCode + ", type=" + type
				+ ", assetClass=" + assetClass + ", sector=" + sector + ", riskLevelId=" + riskLevelId + ", links="
				+ links + "]";
	}

}
