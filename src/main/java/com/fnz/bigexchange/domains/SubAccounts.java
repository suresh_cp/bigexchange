package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubAccounts {

	private List<SubAccount> results;
	private List<Link> links;

	public SubAccounts() {

	}

	public List<SubAccount> getResults() {
		return results;
	}

	public void setResults(List<SubAccount> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "SubAccounts [results=" + results + ", links=" + links + "]";
	}

}
