package com.fnz.bigexchange.domains;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.ExternalTransferType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalTransferOn {

	private Long id;
	private SubAccount SubAccount;
	private String accountHierarchyId;
	private ProviderDetails ProviderDetails;
	private ExternalTransferType transferType;
	private String policyNumber;
	private Date requestedOn;
	private Date lastUpdatedOn;
	private String status;
	private MoneyContract value;
	private Boolean FullTransfer;
	private String notes;

	public ExternalTransferOn() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SubAccount getSubAccount() {
		return SubAccount;
	}

	public void setSubAccount(SubAccount subAccount) {
		SubAccount = subAccount;
	}

	public String getAccountHierarchyId() {
		return accountHierarchyId;
	}

	public void setAccountHierarchyId(String accountHierarchyId) {
		this.accountHierarchyId = accountHierarchyId;
	}

	public ProviderDetails getProviderDetails() {
		return ProviderDetails;
	}

	public void setProviderDetails(ProviderDetails providerDetails) {
		ProviderDetails = providerDetails;
	}

	public ExternalTransferType getTransferType() {
		return transferType;
	}

	public void setTransferType(ExternalTransferType transferType) {
		this.transferType = transferType;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public Date getRequestedOn() {
		return requestedOn;
	}

	public void setRequestedOn(Date requestedOn) {
		this.requestedOn = requestedOn;
	}

	public Date getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(Date lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public MoneyContract getValue() {
		return value;
	}

	public void setValue(MoneyContract value) {
		this.value = value;
	}

	public Boolean getFullTransfer() {
		return FullTransfer;
	}

	public void setFullTransfer(Boolean fullTransfer) {
		FullTransfer = fullTransfer;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Override
	public String toString() {
		return "ExternalTransferOn [id=" + id + ", SubAccount=" + SubAccount + ", accountHierarchyId="
				+ accountHierarchyId + ", ProviderDetails=" + ProviderDetails + ", transferType=" + transferType
				+ ", policyNumber=" + policyNumber + ", requestedOn=" + requestedOn + ", lastUpdatedOn=" + lastUpdatedOn
				+ ", status=" + status + ", value=" + value + ", FullTransfer=" + FullTransfer + ", notes=" + notes
				+ "]";
	}

}
