package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.CountryCode;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HomePhone {

	private CountryCode country;
	private String number;
	private String areaCode;

	public HomePhone() {

	}

	public CountryCode getCountry() {
		return country;
	}

	public void setCountry(CountryCode country) {
		this.country = country;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	@Override
	public String toString() {
		return "HomePhone [country=" + country + ", number=" + number + ", areaCode=" + areaCode + "]";
	}

}
