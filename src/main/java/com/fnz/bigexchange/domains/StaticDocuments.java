package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StaticDocuments {

	private List<StaticDocument> pageOfResults;
	private Long TotalNumberOfResults;
	private List<Link> links;

	public StaticDocuments() {

	}

	public List<StaticDocument> getPageOfResults() {
		return pageOfResults;
	}

	public void setPageOfResults(List<StaticDocument> pageOfResults) {
		this.pageOfResults = pageOfResults;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	public Long getTotalNumberOfResults() {
		return TotalNumberOfResults;
	}

	public void setTotalNumberOfResults(Long TotalNumberOfResults) {
		this.TotalNumberOfResults = TotalNumberOfResults;
	}

	@Override
	public String toString() {
		return "StaticDocuments [pageOfResults=" + pageOfResults + ", TotalNumberOfResults=" + TotalNumberOfResults
				+ ", links=" + links + "]";
	}

}
