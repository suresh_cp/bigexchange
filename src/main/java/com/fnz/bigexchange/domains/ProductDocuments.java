package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDocuments {

	private List<ProductDocument> results;
	private List<Link> links;

	public ProductDocuments() {

	}

	public List<ProductDocument> getResults() {
		return results;
	}

	public void setResults(List<ProductDocument> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "ProductDocuments [results=" + results + ", links=" + links + "]";
	}

}
