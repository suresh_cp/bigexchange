package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.CountryCode;
import com.fnz.bigexchange.enums.CustomerCountryType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerCountry {

	private CustomerCountryType type;
	private CountryCode country;
	private String taxNumber;

	public CustomerCountry() {

	}

	public CustomerCountryType getType() {
		return type;
	}

	public void setType(CustomerCountryType type) {
		this.type = type;
	}

	public CountryCode getCountry() {
		return country;
	}

	public void setCountry(CountryCode country) {
		this.country = country;
	}

	public String getTaxNumber() {
		return taxNumber;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	@Override
	public String toString() {
		return "CustomerCountry [type=" + type + ", country=" + country + ", taxNumber=" + taxNumber + "]";
	}

}
