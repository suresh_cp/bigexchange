package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

public class AccountTypeIndividualCustomerRoleTypes {

	private List<CustomerRoleTypeContract> results;
	private List<Link> links;

	public AccountTypeIndividualCustomerRoleTypes() {

	}

	public List<CustomerRoleTypeContract> getResults() {
		return results;
	}

	public void setResults(List<CustomerRoleTypeContract> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "AccountTypeIndividualCustomerRoleTypes [results=" + results + ", links=" + links + "]";
	}

}
