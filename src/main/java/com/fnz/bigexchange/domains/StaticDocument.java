package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.StaticDocumentCategory;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StaticDocument {

	private StaticDocumentCategory category;
	private Document document;
	private String externalUrl;

	public StaticDocument() {

	}

	public StaticDocumentCategory getCategory() {
		return category;
	}

	public void setCategory(StaticDocumentCategory category) {
		this.category = category;
	}

	public Document getDocumentObject() {
		return document;
	}

	public void setDocumentObject(Document documentObject) {
		this.document = documentObject;
	}

	public String getExternalUrl() {
		return externalUrl;
	}

	public void setExternalUrl(String externalUrl) {
		this.externalUrl = externalUrl;
	}

	@Override
	public String toString() {
		return "Codebeautify [category=" + category + ", documentObject=" + document + ", externalUrl=" + externalUrl
				+ "]";
	}

}
