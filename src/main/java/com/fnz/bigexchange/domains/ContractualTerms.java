package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ContractualTerms {

	private List<ContractualTerm> Results;
	private List<Link> Links;

	public ContractualTerms() {

	}

	public List<ContractualTerm> getResults() {
		return Results;
	}

	public void setResults(List<ContractualTerm> results) {
		Results = results;
	}

	public List<Link> getLinks() {
		return Links;
	}

	public void setLinks(List<Link> links) {
		Links = links;
	}

	@Override
	public String toString() {
		return "ContractualTerms [Results=" + Results + ", Links=" + Links + "]";
	}

}
