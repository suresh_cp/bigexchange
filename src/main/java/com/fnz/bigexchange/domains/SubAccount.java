package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.PensionArrangementType;
import com.fnz.bigexchange.enums.SubAccountStatus;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubAccount {

	private String subAccountId;
	private String hierarchyId;
	private AccountName name;
	private String type;
	private PensionArrangementType arrangementType;
	private Date inceptionDate;
	private Date dateCreated;
	private SubAccountStatus status;
	private String externalSubAccountId;
	private List<Link> links;

	public SubAccount() {

	}

	public String getSubAccountId() {
		return subAccountId;
	}

	public void setSubAccountId(String subAccountId) {
		this.subAccountId = subAccountId;
	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public AccountName getName() {
		return name;
	}

	public void setName(AccountName name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public PensionArrangementType getArrangementType() {
		return arrangementType;
	}

	public void setArrangementType(PensionArrangementType arrangementType) {
		this.arrangementType = arrangementType;
	}

	public Date getInceptionDate() {
		return inceptionDate;
	}

	public void setInceptionDate(Date inceptionDate) {
		this.inceptionDate = inceptionDate;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public SubAccountStatus getStatus() {
		return status;
	}

	public void setStatus(SubAccountStatus status) {
		this.status = status;
	}

	public String getExternalSubAccountId() {
		return externalSubAccountId;
	}

	public void setExternalSubAccountId(String externalSubAccountId) {
		this.externalSubAccountId = externalSubAccountId;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "SubAccount [subAccountId=" + subAccountId + ", hierarchyId=" + hierarchyId + ", name=" + name
				+ ", type=" + type + ", arrangementType=" + arrangementType + ", inceptionDate=" + inceptionDate
				+ ", dateCreated=" + dateCreated + ", status=" + status + ", externalSubAccountId="
				+ externalSubAccountId + ", links=" + links + "]";
	}

}
