package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.AddressType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerRoleAddressBookEntry {

	private AddressType type;
	private Address address;

	public CustomerRoleAddressBookEntry() {

	}

	public AddressType getType() {
		return type;
	}

	public void setType(AddressType type) {
		this.type = type;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "CustomerRoleAddressBookEntry [type=" + type + ", address=" + address + "]";
	}

}
