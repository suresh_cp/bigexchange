package com.fnz.bigexchange.domains;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerInstructionAddressBook {

	private List<CustomerInstructionAddressBookEntry> results;
	private List<Link> links;

	public CustomerInstructionAddressBook() {

	}

	public List<CustomerInstructionAddressBookEntry> getResults() {
		return results;
	}

	public void setResults(List<CustomerInstructionAddressBookEntry> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "CustomerInstructionAddressBook [results=" + results + ", links=" + links + "]";
	}

}
