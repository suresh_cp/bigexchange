package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.InstructionStatus;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BuyInstruction {

	private Long id;
	private String hierarchyId;
	private List<Allocations> allocations;
	private DepositInstruction payment;
	private InstructionStatus status;
	private Boolean includeChargesInAmount;
	private List<AcceptedContractualTerms> acceptedContractualTerms;
	private List<Link> links;

	public BuyInstruction() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public List<Allocations> getAllocations() {
		return allocations;
	}

	public void setAllocations(List<Allocations> allocations) {
		this.allocations = allocations;
	}

	public DepositInstruction getPayment() {
		return payment;
	}

	public void setPayment(DepositInstruction payment) {
		this.payment = payment;
	}

	public InstructionStatus getStatus() {
		return status;
	}

	public void setStatus(InstructionStatus status) {
		this.status = status;
	}

	public Boolean getIncludeChargesInAmount() {
		return includeChargesInAmount;
	}

	public void setIncludeChargesInAmount(Boolean includeChargesInAmount) {
		this.includeChargesInAmount = includeChargesInAmount;
	}

	public List<AcceptedContractualTerms> getAcceptedContractualTerms() {
		return acceptedContractualTerms;
	}

	public void setAcceptedContractualTerms(List<AcceptedContractualTerms> acceptedContractualTerms) {
		this.acceptedContractualTerms = acceptedContractualTerms;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "BuyInstruction [id=" + id + ", hierarchyId=" + hierarchyId + ", allocations=" + allocations
				+ ", payment=" + payment + ", status=" + status + ", includeChargesInAmount=" + includeChargesInAmount
				+ ", acceptedContractualTerms=" + acceptedContractualTerms + ", links=" + links + "]";
	}

}
