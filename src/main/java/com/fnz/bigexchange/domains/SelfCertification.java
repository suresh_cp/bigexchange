package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SelfCertification {

	private String taxComplianceNumberType;
	private String taxComplianceNumber;
	private String countryCode;

	public SelfCertification() {

	}

	public String getTaxComplianceNumberType() {
		return taxComplianceNumberType;
	}

	public void setTaxComplianceNumberType(String taxComplianceNumberType) {
		this.taxComplianceNumberType = taxComplianceNumberType;
	}

	public String getTaxComplianceNumber() {
		return taxComplianceNumber;
	}

	public void setTaxComplianceNumber(String taxComplianceNumber) {
		this.taxComplianceNumber = taxComplianceNumber;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public String toString() {
		return "SelfCertification [taxComplianceNumberType=" + taxComplianceNumberType + ", taxComplianceNumber="
				+ taxComplianceNumber + ", countryCode=" + countryCode + "]";
	}

}
