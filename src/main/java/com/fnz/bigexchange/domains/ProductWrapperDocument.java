package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.StaticDocumentCategory;
import com.fnz.bigexchange.enums.WrapperType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductWrapperDocument {

	private WrapperType wrapperType; // m
	private StaticDocumentCategory category; // m
	private Document document;
	private String externalUrl;

	public ProductWrapperDocument() {

	}

	public WrapperType getWrapperType() {
		return wrapperType;
	}

	public void setWrapperType(WrapperType wrapperType) {
		this.wrapperType = wrapperType;
	}

	public StaticDocumentCategory getCategory() {
		return category;
	}

	public void setCategory(StaticDocumentCategory category) {
		this.category = category;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public String getExternalUrl() {
		return externalUrl;
	}

	public void setExternalUrl(String externalUrl) {
		this.externalUrl = externalUrl;
	}

	@Override
	public String toString() {
		return "ProductWrapperDocument [wrapperType=" + wrapperType + ", category=" + category + ", document="
				+ document + ", externalUrl=" + externalUrl + "]";
	}

}
