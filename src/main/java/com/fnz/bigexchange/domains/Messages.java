package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Messages {

	private List<Message> results;
	private List<Link> links;

	public Messages() {

	}

	public List<Message> getResults() {
		return results;
	}

	public void setResults(List<Message> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "Messages [results=" + results + ", links=" + links + "]";
	}

}
