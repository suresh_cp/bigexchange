package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FundCharges {
	private MoneyContract initialCharge;
	private MoneyContract anualManagementCharge;
	private MoneyContract additionalFundExpenses;
	private MoneyContract totalFundCost;
	private MoneyContract discount;

	public FundCharges() {

	}

	public MoneyContract getInitialCharge() {
		return initialCharge;
	}

	public void setInitialCharge(MoneyContract initialCharge) {
		this.initialCharge = initialCharge;
	}

	public MoneyContract getAnualManagementCharge() {
		return anualManagementCharge;
	}

	public void setAnualManagementCharge(MoneyContract anualManagementCharge) {
		this.anualManagementCharge = anualManagementCharge;
	}

	public MoneyContract getAdditionalFundExpenses() {
		return additionalFundExpenses;
	}

	public void setAdditionalFundExpenses(MoneyContract additionalFundExpenses) {
		this.additionalFundExpenses = additionalFundExpenses;
	}

	public MoneyContract getTotalFundCost() {
		return totalFundCost;
	}

	public void setTotalFundCost(MoneyContract totalFundCost) {
		this.totalFundCost = totalFundCost;
	}

	public MoneyContract getDiscount() {
		return discount;
	}

	public void setDiscount(MoneyContract discount) {
		this.discount = discount;
	}

	@Override
	public String toString() {
		return "Charges [initialCharge=" + initialCharge + ", anualManagementCharge=" + anualManagementCharge
				+ ", additionalFundExpenses=" + additionalFundExpenses + ", totalFundCost=" + totalFundCost
				+ ", discount=" + discount + "]";
	}

}
