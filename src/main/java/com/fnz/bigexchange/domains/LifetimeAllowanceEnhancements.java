package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LifetimeAllowanceEnhancements {

	/// check whether these are dynamic to change according to year
	private String type;
	private boolean grantedPrior2012;
	private boolean grantedPrior2014;
	private boolean grantedPrior2016;
	private boolean grantedPost2016;
	private String certificateNumber;
	private Double enhancementFactor;

	public LifetimeAllowanceEnhancements() {

	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isGrantedPrior2012() {
		return grantedPrior2012;
	}

	public void setGrantedPrior2012(boolean grantedPrior2012) {
		this.grantedPrior2012 = grantedPrior2012;
	}

	public boolean isGrantedPrior2014() {
		return grantedPrior2014;
	}

	public void setGrantedPrior2014(boolean grantedPrior2014) {
		this.grantedPrior2014 = grantedPrior2014;
	}

	public boolean isGrantedPrior2016() {
		return grantedPrior2016;
	}

	public void setGrantedPrior2016(boolean grantedPrior2016) {
		this.grantedPrior2016 = grantedPrior2016;
	}

	public boolean isGrantedPost2016() {
		return grantedPost2016;
	}

	public void setGrantedPost2016(boolean grantedPost2016) {
		this.grantedPost2016 = grantedPost2016;
	}

	public String getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	public Double getEnhancementFactor() {
		return enhancementFactor;
	}

	public void setEnhancementFactor(Double enhancementFactor) {
		this.enhancementFactor = enhancementFactor;
	}

	@Override
	public String toString() {
		return "LifetimeAllowanceEnhancements [type=" + type + ", grantedPrior2012=" + grantedPrior2012
				+ ", grantedPrior2014=" + grantedPrior2014 + ", grantedPrior2016=" + grantedPrior2016
				+ ", grantedPost2016=" + grantedPost2016 + ", certificateNumber=" + certificateNumber
				+ ", enhancementFactor=" + enhancementFactor + "]";
	}

}
