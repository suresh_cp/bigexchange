package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.OrderType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Amounts {

	private OrderType type;
	private MoneyContract value;
	private MoneyContract totalAmount;
	private Double quantity;
	private MoneyContract unitPrice;

	public Amounts() {

	}

	public OrderType getType() {
		return type;
	}

	public void setType(OrderType type) {
		this.type = type;
	}

	public MoneyContract getValue() {
		return value;
	}

	public void setValue(MoneyContract value) {
		this.value = value;
	}

	public MoneyContract getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(MoneyContract totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public MoneyContract getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(MoneyContract unitPrice) {
		this.unitPrice = unitPrice;
	}

	@Override
	public String toString() {
		return "Amounts [type=" + type + ", value=" + value + ", totalAmount=" + totalAmount + ", quantity=" + quantity
				+ ", unitPrice=" + unitPrice + "]";
	}

}
