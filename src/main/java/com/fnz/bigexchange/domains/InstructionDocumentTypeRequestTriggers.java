package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.DocumentRequestStatus;

@JsonIgnoreProperties(ignoreUnknown = true)

public class InstructionDocumentTypeRequestTriggers {

	private Long id;
	private DocumentRequestStatus status;
	private Long createdDocumentId;
	private Long documentTypeId;
	private List<Link> links;

	public InstructionDocumentTypeRequestTriggers() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DocumentRequestStatus getStatus() {
		return status;
	}

	public void setStatus(DocumentRequestStatus status) {
		this.status = status;
	}

	public Long getCreatedDocumentId() {
		return createdDocumentId;
	}

	public void setCreatedDocumentId(Long createdDocumentId) {
		this.createdDocumentId = createdDocumentId;
	}

	public Long getDocumentTypeId() {
		return documentTypeId;
	}

	public void setDocumentTypeId(Long documentTypeId) {
		this.documentTypeId = documentTypeId;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "DocumentRequest [id=" + id + ", status=" + status + ", createdDocumentId=" + createdDocumentId
				+ ", documentTypeId=" + documentTypeId + ", links=" + links + "]";
	}

}
