package com.fnz.bigexchange.domains;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketSettings {

	private Boolean isOpen;
	private OpeningHours openingHours;
	private Date quoteAcceptPeriod;

	public MarketSettings() {

	}

	public Boolean getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(Boolean isOpen) {
		this.isOpen = isOpen;
	}

	public OpeningHours getOpeningHours() {
		return openingHours;
	}

	public void setOpeningHours(OpeningHours openingHours) {
		this.openingHours = openingHours;
	}

	public Date getQuoteAcceptPeriod() {
		return quoteAcceptPeriod;
	}

	public void setQuoteAcceptPeriod(Date quoteAcceptPeriod) {
		this.quoteAcceptPeriod = quoteAcceptPeriod;
	}

	@Override
	public String toString() {
		return "MarketSettings [isOpen=" + isOpen + ", openingHours=" + openingHours + ", quoteAcceptPeriod="
				+ quoteAcceptPeriod + "]";
	}

}
