package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NameSubAccountInstruction {

	private String full;
	private String medium;
	private String shorter;
	private String preferred;

	public NameSubAccountInstruction() {

	}

	public String getFull() {
		return full;
	}

	public void setFull(String full) {
		this.full = full;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public String getShorter() {
		return shorter;
	}

	public void setShorter(String shorter) {
		this.shorter = shorter;
	}

	public String getPreferred() {
		return preferred;
	}

	public void setPreferred(String preferred) {
		this.preferred = preferred;
	}

	@Override
	public String toString() {
		return "NameSubAccountInstruction [full=" + full + ", medium=" + medium + ", shorter=" + shorter
				+ ", preferred=" + preferred + "]";
	}

}
