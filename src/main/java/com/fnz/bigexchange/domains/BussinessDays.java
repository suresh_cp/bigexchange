package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BussinessDays {

	private List<Date> results;
	private List<Link> links;

	public BussinessDays() {

	}

	public List<Date> getResults() {
		return results;
	}

	public void setResults(List<Date> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "CashBalances [results=" + results + ", links=" + links + "]";
	}

}
