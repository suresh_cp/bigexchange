package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.InstructionStatus;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegularBuyInstruction {

	private Long id;
	private String hierarchyId;
	private List<Allocations> allocations;
	private RegularDepositInstruction payment;
	private InstructionStatus status;
	private List<AcceptedContractualTerms> acceptedContractualTerms;
	private Boolean includeChargesInAmount;
	private List<Link> links;

	public RegularBuyInstruction() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public List<Allocations> getAllocations() {
		return allocations;
	}

	public void setAllocations(List<Allocations> allocations) {
		this.allocations = allocations;
	}

	public RegularDepositInstruction getPayment() {
		return payment;
	}

	public void setPayment(RegularDepositInstruction payment) {
		this.payment = payment;
	}

	public InstructionStatus getStatus() {
		return status;
	}

	public void setStatus(InstructionStatus status) {
		this.status = status;
	}

	public List<AcceptedContractualTerms> getAcceptedContractualTerms() {
		return acceptedContractualTerms;
	}

	public void setAcceptedContractualTerms(List<AcceptedContractualTerms> acceptedContractualTerms) {
		this.acceptedContractualTerms = acceptedContractualTerms;
	}

	public boolean isIncludeChargesInAmount() {
		return includeChargesInAmount;
	}

	public void setIncludeChargesInAmount(boolean includeChargesInAmount) {
		this.includeChargesInAmount = includeChargesInAmount;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "RegularBuyInstruction [id=" + id + ", hierarchyId=" + hierarchyId + ", allocations=" + allocations
				+ ", payment=" + payment + ", status=" + status + ", acceptedContractualTerms="
				+ acceptedContractualTerms + ", includeChargesInAmount=" + includeChargesInAmount + ", links=" + links
				+ "]";
	}

}
