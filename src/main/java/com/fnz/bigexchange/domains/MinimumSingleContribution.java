package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.Currency;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MinimumSingleContribution {

	private Currency currency;
	private Double value;

	public MinimumSingleContribution() {

	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "MinimumSingleContribution [currency=" + currency + ", value=" + value + "]";
	}

}
