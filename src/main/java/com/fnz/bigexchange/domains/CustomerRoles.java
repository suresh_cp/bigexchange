package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerRoles {

	private List<CustomerRoleDetails> results;
	private List<Link> links;

	public CustomerRoles() {

	}

	public List<CustomerRoleDetails> getResults() {
		return results;
	}

	public void setResults(List<CustomerRoleDetails> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "CustomerRoles [results=" + results + ", links=" + links + "]";
	}

}
