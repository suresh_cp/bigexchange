package com.fnz.bigexchange.domains;

public class FundDetail {
	private Long id;
	private String asin;
	private String fundName;
	private Integer riskRating;
	private String fundType;
	private String fundComapany;

	public FundDetail(Long id, String asin, String fundName, Integer riskRating, String fundType, String fundComapany) {
		super();
		this.id = id;
		this.asin = asin;
		this.fundName = fundName;
		this.riskRating = riskRating;
		this.fundType = fundType;
		this.fundComapany = fundComapany;
	}

	public FundDetail() {
		super();
	}

	public String getAsin() {
		return asin;
	}

	public void setAsin(String asin) {
		this.asin = asin;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public Integer getRiskRating() {
		return riskRating;
	}

	public void setRiskRating(Integer riskRating) {
		this.riskRating = riskRating;
	}

	public String getFundType() {
		return fundType;
	}

	public void setFundType(String fundType) {
		this.fundType = fundType;
	}

	public String getFundComapany() {
		return fundComapany;
	}

	public void setFundComapany(String fundComapany) {
		this.fundComapany = fundComapany;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
