package com.fnz.bigexchange.domains;

import java.util.List;

public class CustomerTitles {

	private List<CustomerTitle> results;
	private List<Link> links;

	public CustomerTitles() {

	}

	public List<CustomerTitle> getResults() {
		return results;
	}

	public void setResults(List<CustomerTitle> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "CustomerTitles [results=" + results + ", links=" + links + "]";
	}

}
