package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmploymentDetails {

	private String employmentStatus;
	private Employer employer;
	private String jobTitle;
	private String employeeReferenceNumber;
	private String costCentre;
	private String departmentCode;
	private String category;
	private String occupation;
	private Boolean isLeaver;
	private Boolean reportOnPersonalAccountDealing;
	private AnnualEarnings annualEarnings;
	private List<Link> links;

	public EmploymentDetails() {

	}

	public String getEmploymentStatus() {
		return employmentStatus;
	}

	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}

	public Employer getEmployerObject() {
		return employer;
	}

	public void setEmployerObject(Employer employerObject) {
		this.employer = employerObject;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getEmployeeReferenceNumber() {
		return employeeReferenceNumber;
	}

	public void setEmployeeReferenceNumber(String employeeReferenceNumber) {
		this.employeeReferenceNumber = employeeReferenceNumber;
	}

	public String getCostCentre() {
		return costCentre;
	}

	public void setCostCentre(String costCentre) {
		this.costCentre = costCentre;
	}

	public String getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public boolean isLeaver() {
		return isLeaver;
	}

	public void setLeaver(boolean isLeaver) {
		this.isLeaver = isLeaver;
	}

	public boolean isReportOnPersonalAccountDealing() {
		return reportOnPersonalAccountDealing;
	}

	public void setReportOnPersonalAccountDealing(boolean reportOnPersonalAccountDealing) {
		this.reportOnPersonalAccountDealing = reportOnPersonalAccountDealing;
	}

	public AnnualEarnings getAnnualEarningsObject() {
		return annualEarnings;
	}

	public void setAnnualEarningsObject(AnnualEarnings annualEarningsObject) {
		this.annualEarnings = annualEarningsObject;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "EmploymentDetails [employmentStatus=" + employmentStatus + ", employerObject=" + employer
				+ ", jobTitle=" + jobTitle + ", employeeReferenceNumber=" + employeeReferenceNumber + ", costCentre="
				+ costCentre + ", departmentCode=" + departmentCode + ", category=" + category + ", occupation="
				+ occupation + ", isLeaver=" + isLeaver + ", reportOnPersonalAccountDealing="
				+ reportOnPersonalAccountDealing + ", annualEarningsObject=" + annualEarnings + ", links=" + links
				+ "]";
	}

}
