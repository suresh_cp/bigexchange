package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MinimumRegularContributionsByFrequencyCollection {

	private List<MinimumRegularContributionsByFrequency> Results;
	private List<Link> Links;

	public MinimumRegularContributionsByFrequencyCollection() {

	}

	public List<MinimumRegularContributionsByFrequency> getResults() {
		return Results;
	}

	public void setResults(List<MinimumRegularContributionsByFrequency> results) {
		Results = results;
	}

	public List<Link> getLinks() {
		return Links;
	}

	public void setLinks(List<Link> links) {
		Links = links;
	}

	@Override
	public String toString() {
		return "MinimumRegularContributionsByFrequencyCollection [Results=" + Results + ", Links=" + Links + "]";
	}

}
