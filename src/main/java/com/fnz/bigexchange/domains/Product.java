package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.Currency;
import com.fnz.bigexchange.enums.ProductType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {

	private Long ProductId;
	private String Name;
	private String Sedol;
	private String Isin;
	private String Ric;
	private String Cusip;
	private Currency Currency;
	private ProductType ProductCode;
	private String CitiCode;
	private String Type;
	private String AssetClass;
	private Sector SectorObject;
	private Long RiskLevelId;
	private List<Link> Links;

	public Product() {

	}

	public Long getProductId() {
		return ProductId;
	}

	public void setProductId(Long productId) {
		ProductId = productId;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getSedol() {
		return Sedol;
	}

	public void setSedol(String sedol) {
		Sedol = sedol;
	}

	public String getIsin() {
		return Isin;
	}

	public void setIsin(String isin) {
		Isin = isin;
	}

	public String getRic() {
		return Ric;
	}

	public void setRic(String ric) {
		Ric = ric;
	}

	public String getCusip() {
		return Cusip;
	}

	public void setCusip(String cusip) {
		Cusip = cusip;
	}

	public Currency getCurrency() {
		return Currency;
	}

	public void setCurrency(Currency currency) {
		Currency = currency;
	}

	public ProductType getProductCode() {
		return ProductCode;
	}

	public void setProductCode(ProductType productCode) {
		ProductCode = productCode;
	}

	public String getCitiCode() {
		return CitiCode;
	}

	public void setCitiCode(String citiCode) {
		CitiCode = citiCode;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public String getAssetClass() {
		return AssetClass;
	}

	public void setAssetClass(String assetClass) {
		AssetClass = assetClass;
	}

	public Sector getSectorObject() {
		return SectorObject;
	}

	public void setSectorObject(Sector sectorObject) {
		SectorObject = sectorObject;
	}

	public Long getRiskLevelId() {
		return RiskLevelId;
	}

	public void setRiskLevelId(Long riskLevelId) {
		RiskLevelId = riskLevelId;
	}

	public List<Link> getLinks() {
		return Links;
	}

	public void setLinks(List<Link> links) {
		Links = links;
	}

	@Override
	public String toString() {
		return "Product [ProductId=" + ProductId + ", Name=" + Name + ", Sedol=" + Sedol + ", Isin=" + Isin + ", Ric="
				+ Ric + ", Cusip=" + Cusip + ", Currency=" + Currency + ", ProductCode=" + ProductCode + ", CitiCode="
				+ CitiCode + ", Type=" + Type + ", AssetClass=" + AssetClass + ", SectorObject=" + SectorObject
				+ ", RiskLevelId=" + RiskLevelId + ", Links=" + Links + "]";
	}

}
