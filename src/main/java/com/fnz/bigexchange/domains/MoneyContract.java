package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.Currency;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MoneyContract {

	private Currency Currency;
	private Double Value;

	public MoneyContract() {

	}

	public Currency getCurrency() {
		return Currency;
	}

	public void setCurrency(Currency currency) {
		Currency = currency;
	}

	public Double getValue() {
		return Value;
	}

	public void setValue(Double value) {
		Value = value;
	}

	@Override
	public String toString() {
		return "MoneyContract [Currency=" + Currency + ", Value=" + Value + "]";
	}

}
