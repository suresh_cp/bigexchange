package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerIdentifiers {

	private List<Passport> passports;
	private List<TaxIdentifier> taxIdentifiers;
	private List<NationalIdentifier> nationalIdentifiers;
	private List<ConcatIdentifier> concatIdentifiers;

	public CustomerIdentifiers() {

	}

	public List<Passport> getPassports() {
		return passports;
	}

	public void setPassports(List<Passport> passports) {
		this.passports = passports;
	}

	public List<TaxIdentifier> getTaxIdentifiers() {
		return taxIdentifiers;
	}

	public void setTaxIdentifiers(List<TaxIdentifier> taxIdentifiers) {
		this.taxIdentifiers = taxIdentifiers;
	}

	public List<NationalIdentifier> getNationalIdentifiers() {
		return nationalIdentifiers;
	}

	public void setNationalIdentifiers(List<NationalIdentifier> nationalIdentifiers) {
		this.nationalIdentifiers = nationalIdentifiers;
	}

	public List<ConcatIdentifier> getConcatIdentifiers() {
		return concatIdentifiers;
	}

	public void setConcatIdentifiers(List<ConcatIdentifier> concatIdentifiers) {
		this.concatIdentifiers = concatIdentifiers;
	}

	@Override
	public String toString() {
		return "CustomerIdentifiers [passports=" + passports + ", taxIdentifiers=" + taxIdentifiers
				+ ", nationalIdentifiers=" + nationalIdentifiers + ", concatIdentifiers=" + concatIdentifiers + "]";
	}

}
