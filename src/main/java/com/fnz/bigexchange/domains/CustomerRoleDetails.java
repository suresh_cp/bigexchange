package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.CustomerRoleType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerRoleDetails {

	private Boolean isPrimary;
	private CustomerRoleType role;
	private String hierarchyId;
	private String accountId;
	private List<Link> links;

	public CustomerRoleDetails() {

	}

	public Boolean getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public CustomerRoleType getRole() {
		return role;
	}

	public void setRole(CustomerRoleType role) {
		this.role = role;
	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "CustomerRoleDetails [isPrimary=" + isPrimary + ", role=" + role + ", hierarchyId=" + hierarchyId
				+ ", accountId=" + accountId + ", links=" + links + "]";
	}

}
