package com.fnz.bigexchange.domains;

import java.util.List;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.BankAccountType;
import com.fnz.bigexchange.enums.DistributionDividendPreference;
import com.fnz.bigexchange.enums.DistributionFrequency;
import com.fnz.bigexchange.enums.InstructionStatus;
import com.fnz.bigexchange.enums.WrapperType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubAccountInstruction {

	private Long workItemId;
	private String hierarchyId;
	private String ownerHierarchyId;
	private InstructionStatus instructionStatus;
	private NameSubAccountInstruction name;
	private WrapperType type;
	private DistributionDividendPreference distributionDividendPreference;
	private DistributionFrequency externalPaymentFrequency;
	private Long bankAccountId;
	private BankAccountType bankAccountIdType;
	private List<AcceptedContractualTerms> acceptedContractualTerms;
	private Date firstPaymentDate;
	private Double minimumAmount;
	private Boolean dividendReinvestmentPlanApplied;
	private List<Link> links;

	public SubAccountInstruction() {

	}

	public Long getWorkItemId() {
		return workItemId;
	}

	public void setWorkItemId(Long workItemId) {
		this.workItemId = workItemId;
	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public String getOwnerHierarchyId() {
		return ownerHierarchyId;
	}

	public void setOwnerHierarchyId(String ownerHierarchyId) {
		this.ownerHierarchyId = ownerHierarchyId;
	}

	public InstructionStatus getInstructionStatus() {
		return instructionStatus;
	}

	public void setInstructionStatus(InstructionStatus instructionStatus) {
		this.instructionStatus = instructionStatus;
	}

	public NameSubAccountInstruction getName() {
		return name;
	}

	public void setName(NameSubAccountInstruction name) {
		this.name = name;
	}

	public WrapperType getType() {
		return type;
	}

	public void setType(WrapperType type) {
		this.type = type;
	}

	public DistributionDividendPreference getDistributionDividendPreference() {
		return distributionDividendPreference;
	}

	public void setDistributionDividendPreference(DistributionDividendPreference distributionDividendPreference) {
		this.distributionDividendPreference = distributionDividendPreference;
	}

	public DistributionFrequency getExternalPaymentFrequency() {
		return externalPaymentFrequency;
	}

	public void setExternalPaymentFrequency(DistributionFrequency externalPaymentFrequency) {
		this.externalPaymentFrequency = externalPaymentFrequency;
	}

	public Long getBankAccountId() {
		return bankAccountId;
	}

	public void setBankAccountId(Long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}

	public BankAccountType getBankAccountIdType() {
		return bankAccountIdType;
	}

	public void setBankAccountIdType(BankAccountType bankAccountIdType) {
		this.bankAccountIdType = bankAccountIdType;
	}

	public List<AcceptedContractualTerms> getAcceptedContractualTerms() {
		return acceptedContractualTerms;
	}

	public void setAcceptedContractualTerms(List<AcceptedContractualTerms> acceptedContractualTerms) {
		this.acceptedContractualTerms = acceptedContractualTerms;
	}

	public Date getFirstPaymentDate() {
		return firstPaymentDate;
	}

	public void setFirstPaymentDate(Date firstPaymentDate) {
		this.firstPaymentDate = firstPaymentDate;
	}

	public Double getMinimumAmount() {
		return minimumAmount;
	}

	public void setMinimumAmount(Double minimumAmount) {
		this.minimumAmount = minimumAmount;
	}

	public Boolean getDividendReinvestmentPlanApplied() {
		return dividendReinvestmentPlanApplied;
	}

	public void setDividendReinvestmentPlanApplied(Boolean dividendReinvestmentPlanApplied) {
		this.dividendReinvestmentPlanApplied = dividendReinvestmentPlanApplied;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "SubAccountInstruction [workItemId=" + workItemId + ", hierarchyId=" + hierarchyId
				+ ", ownerHierarchyId=" + ownerHierarchyId + ", instructionStatus=" + instructionStatus
				+ ", nameObject=" + name + ", type=" + type + ", distributionDividendPreference="
				+ distributionDividendPreference + ", externalPaymentFrequency=" + externalPaymentFrequency
				+ ", bankAccountId=" + bankAccountId + ", bankAccountIdType=" + bankAccountIdType
				+ ", acceptedContractualTerms=" + acceptedContractualTerms + ", firstPaymentDate=" + firstPaymentDate
				+ ", minimumAmount=" + minimumAmount + ", dividendReinvestmentPlanApplied="
				+ dividendReinvestmentPlanApplied + ", links=" + links + "]";
	}

}
