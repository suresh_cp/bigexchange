package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.CountryCode;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Address {

	private String buildingName;
	private String buildingNumber;
	private String streetName;
	private String state;
	private String unitNumber;
	private String suburb;
	private String line1;
	private String line2;
	private String line3;
	private String line4;
	private String line5;
	private String line6;
	private String town;
	private String postCode;
	private CountryCode countryCode;
	private Long templateId;
	private Boolean isPostReturned;

	public Address() {

	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getBuildingNumber() {
		return buildingNumber;
	}

	public void setBuildingNumber(String buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public String getSuburb() {
		return suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getLine3() {
		return line3;
	}

	public void setLine3(String line3) {
		this.line3 = line3;
	}

	public String getLine4() {
		return line4;
	}

	public void setLine4(String line4) {
		this.line4 = line4;
	}

	public String getLine5() {
		return line5;
	}

	public void setLine5(String line5) {
		this.line5 = line5;
	}

	public String getLine6() {
		return line6;
	}

	public void setLine6(String line6) {
		this.line6 = line6;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public CountryCode getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(CountryCode countryCode) {
		this.countryCode = countryCode;
	}

	public Long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}

	public boolean isPostReturned() {
		return isPostReturned;
	}

	public void setPostReturned(Boolean isPostReturned) {
		this.isPostReturned = isPostReturned;
	}

	@Override
	public String toString() {
		return "Address [buildingName=" + buildingName + ", buildingNumber=" + buildingNumber + ", streetName="
				+ streetName + ", state=" + state + ", unitNumber=" + unitNumber + ", suburb=" + suburb + ", line1="
				+ line1 + ", line2=" + line2 + ", line3=" + line3 + ", line4=" + line4 + ", line5=" + line5 + ", line6="
				+ line6 + ", town=" + town + ", postCode=" + postCode + ", countryCode=" + countryCode + ", templateId="
				+ templateId + ", isPostReturned=" + isPostReturned + "]";
	}

}
