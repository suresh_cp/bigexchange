package com.fnz.bigexchange.domains;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TaxYearAllowance {

	private TaxYear year;
	private MoneyContract used;
	private MoneyContract scheduledReqularPayments;
	private MoneyContract remaining;
	private MoneyContract withdrawals;
	private MoneyContract total;
	private Date firstSubscriptionDate;

	public TaxYearAllowance() {

	}

	public TaxYear getYear() {
		return year;
	}

	public void setYear(TaxYear year) {
		this.year = year;
	}

	public MoneyContract getUsed() {
		return used;
	}

	public void setUsed(MoneyContract used) {
		this.used = used;
	}

	public MoneyContract getScheduledReqularPayments() {
		return scheduledReqularPayments;
	}

	public void setScheduledReqularPayments(MoneyContract scheduledReqularPayments) {
		this.scheduledReqularPayments = scheduledReqularPayments;
	}

	public MoneyContract getRemaining() {
		return remaining;
	}

	public void setRemaining(MoneyContract remaining) {
		this.remaining = remaining;
	}

	public MoneyContract getWithdrawals() {
		return withdrawals;
	}

	public void setWithdrawals(MoneyContract withdrawals) {
		this.withdrawals = withdrawals;
	}

	public MoneyContract getTotal() {
		return total;
	}

	public void setTotal(MoneyContract total) {
		this.total = total;
	}

	public Date getFirstSubscriptionDate() {
		return firstSubscriptionDate;
	}

	public void setFirstSubscriptionDate(Date firstSubscriptionDate) {
		this.firstSubscriptionDate = firstSubscriptionDate;
	}

	@Override
	public String toString() {
		return "TaxYearAllowance [year=" + year + ", used=" + used + ", scheduledReqularPayments="
				+ scheduledReqularPayments + ", remaining=" + remaining + ", withdrawals=" + withdrawals + ", total="
				+ total + ", firstSubscriptionDate=" + firstSubscriptionDate + "]";
	}

}
