package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ExchangeTradedInstrumentPrices {

	private Price askPrice;
	private Price bidPrice;
	private Price midPrice;
	private Price lastClosePrice;

	public ExchangeTradedInstrumentPrices() {

	}

	public Price getAskPrice() {
		return askPrice;
	}

	public void setAskPrice(Price askPrice) {
		this.askPrice = askPrice;
	}

	public Price getBidPrice() {
		return bidPrice;
	}

	public void setBidPrice(Price bidPrice) {
		this.bidPrice = bidPrice;
	}

	public Price getMidPrice() {
		return midPrice;
	}

	public void setMidPrice(Price midPrice) {
		this.midPrice = midPrice;
	}

	public Price getLastClosePrice() {
		return lastClosePrice;
	}

	public void setLastClosePrice(Price lastClosePrice) {
		this.lastClosePrice = lastClosePrice;
	}

	@Override
	public String toString() {
		return "ExchangeTradedInstrumentPrices [askPrice=" + askPrice + ", bidPrice=" + bidPrice + ", midPrice="
				+ midPrice + ", lastClosePrice=" + lastClosePrice + "]";
	}

}
