package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TaxRegulationDetails {

	private SelfCertification selfCertification;
	private String regulationType;

	public TaxRegulationDetails() {

	}

	public SelfCertification getSelfCertificationObject() {
		return selfCertification;
	}

	public void setSelfCertificationObject(SelfCertification selfCertificationObject) {
		this.selfCertification = selfCertificationObject;
	}

	public String getRegulationType() {
		return regulationType;
	}

	public void setRegulationType(String regulationType) {
		this.regulationType = regulationType;
	}

	@Override
	public String toString() {
		return "TaxRegulationDetails [selfCertificationObject=" + selfCertification + ", regulationType="
				+ regulationType + "]";
	}

}
