package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.FileExtension;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DocumentFormat {
	private String mediaType;
	private FileExtension fileExtension;

	public DocumentFormat() {

	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public FileExtension getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(FileExtension fileExtension) {
		this.fileExtension = fileExtension;
	}

	@Override
	public String toString() {
		return "DocumentFormat [mediaType=" + mediaType + ", fileExtension=" + fileExtension + "]";
	}

}
