package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.CountryCode;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConcatIdentifier {

	private CountryCode country;
	private String value;

	public ConcatIdentifier() {

	}

	public CountryCode getCountry() {
		return country;
	}

	public void setCountry(CountryCode country) {
		this.country = country;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "ConcatIdentifier [country=" + country + ", value=" + value + "]";
	}

}
