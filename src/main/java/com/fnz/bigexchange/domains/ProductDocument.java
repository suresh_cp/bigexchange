package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.ProductDocumentType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDocument {

	private ProductDocumentType Type;
	private Long ProductId;
	private Document PlatformDocumentObject;
	private String ExternalUrl;
	private List<Link> links;

	public ProductDocument() {

	}

	public ProductDocumentType getType() {
		return Type;
	}

	public void setType(ProductDocumentType type) {
		Type = type;
	}

	public Long getProductId() {
		return ProductId;
	}

	public void setProductId(Long productId) {
		ProductId = productId;
	}

	public Document getPlatformDocumentObject() {
		return PlatformDocumentObject;
	}

	public void setPlatformDocumentObject(Document platformDocumentObject) {
		PlatformDocumentObject = platformDocumentObject;
	}

	public String getExternalUrl() {
		return ExternalUrl;
	}

	public void setExternalUrl(String externalUrl) {
		ExternalUrl = externalUrl;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "ProductDocument [Type=" + Type + ", ProductId=" + ProductId + ", PlatformDocumentObject="
				+ PlatformDocumentObject + ", ExternalUrl=" + ExternalUrl + ", links=" + links + "]";
	}

}
