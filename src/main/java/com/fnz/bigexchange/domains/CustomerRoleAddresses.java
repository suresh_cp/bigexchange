package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

public class CustomerRoleAddresses {

	private List<CustomerRoleAddressBookEntry> results;
	private List<Link> links;

	public CustomerRoleAddresses() {

	}

	public List<CustomerRoleAddressBookEntry> getResults() {
		return results;
	}

	public void setResults(List<CustomerRoleAddressBookEntry> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "CustomerRoleAddresses [results=" + results + ", links=" + links + "]";
	}

}
