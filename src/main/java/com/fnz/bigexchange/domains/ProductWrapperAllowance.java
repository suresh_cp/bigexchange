package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductWrapperAllowance {

	private Allowance allowance;
	private TaxYear year;

	public ProductWrapperAllowance() {

	}

	public Allowance getAllowance() {
		return allowance;
	}

	public void setAllowance(Allowance allowance) {
		this.allowance = allowance;
	}

	public TaxYear getYear() {
		return year;
	}

	public void setYear(TaxYear year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "ProductWrapperAllowance [allowance=" + allowance + ", year=" + year + "]";
	}

}
