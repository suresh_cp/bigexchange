package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

public class InstructionDocumentRequests {

	private List<DocumentRequest> results;
	private List<Link> links;

	public InstructionDocumentRequests() {

	}

	public List<DocumentRequest> getResults() {
		return results;
	}

	public void setResults(List<DocumentRequest> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "InstructionDocumentRequests [results=" + results + ", links=" + links + "]";
	}

}
