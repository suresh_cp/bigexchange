package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.ProductLocation;
import com.fnz.bigexchange.enums.ProductTransactionType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductTransaction {

	private Long transactionId;
	private SubAccount subAccount;
	private Product product;
	private Double Quantity;
	private String narrative;
	private MoneyContract cost;
	private MoneyContract fXCost;
	private ProductTransactionType transactionType;
	private ProductLocation location;
	private Date transactionDate;
	private String transactionSource;
	private Long transactionSourceId;
	private String transferProvider;
	private MoneyContract value;
	private String orderDirection;
	private List<Link> Links;

	public ProductTransaction() {

	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public SubAccount getSubAccount() {
		return subAccount;
	}

	public void setSubAccount(SubAccount subAccount) {
		this.subAccount = subAccount;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getQuantity() {
		return Quantity;
	}

	public void setQuantity(Double quantity) {
		Quantity = quantity;
	}

	public String getNarrative() {
		return narrative;
	}

	public void setNarrative(String narrative) {
		this.narrative = narrative;
	}

	public MoneyContract getCost() {
		return cost;
	}

	public void setCost(MoneyContract cost) {
		this.cost = cost;
	}

	public MoneyContract getfXCost() {
		return fXCost;
	}

	public void setfXCost(MoneyContract fXCost) {
		this.fXCost = fXCost;
	}

	public ProductTransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(ProductTransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public ProductLocation getLocation() {
		return location;
	}

	public void setLocation(ProductLocation location) {
		this.location = location;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionSource() {
		return transactionSource;
	}

	public void setTransactionSource(String transactionSource) {
		this.transactionSource = transactionSource;
	}

	public Long getTransactionSourceId() {
		return transactionSourceId;
	}

	public void setTransactionSourceId(Long transactionSourceId) {
		this.transactionSourceId = transactionSourceId;
	}

	public String getTransferProvider() {
		return transferProvider;
	}

	public void setTransferProvider(String transferProvider) {
		this.transferProvider = transferProvider;
	}

	public MoneyContract getValue() {
		return value;
	}

	public void setValue(MoneyContract value) {
		this.value = value;
	}

	public String getOrderDirection() {
		return orderDirection;
	}

	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}

	public List<Link> getLinks() {
		return Links;
	}

	public void setLinks(List<Link> links) {
		Links = links;
	}

	@Override
	public String toString() {
		return "ProductTransaction [transactionId=" + transactionId + ", subAccount=" + subAccount + ", product="
				+ product + ", Quantity=" + Quantity + ", narrative=" + narrative + ", cost=" + cost + ", fXCost="
				+ fXCost + ", transactionType=" + transactionType + ", location=" + location + ", transactionDate="
				+ transactionDate + ", transactionSource=" + transactionSource + ", transactionSourceId="
				+ transactionSourceId + ", transferProvider=" + transferProvider + ", value=" + value
				+ ", orderDirection=" + orderDirection + ", Links=" + Links + "]";
	}

}
