package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.BankAccountType;
import com.fnz.bigexchange.enums.DepositMethod;
import com.fnz.bigexchange.enums.InstructionStatus;
import com.fnz.bigexchange.enums.PaymentSource;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DepositInstruction {

	private Long id;
	private MoneyContract amount;
	private DepositMethod method;
	private String sourceHierarchyId;
	private InstructionStatus status;
	private String hierarchyId;
	private Boolean taxRelief;
	private Long bankAccountId;
	private BankAccountType bankAccountIdType;
	private Date paymentDate;
	private Boolean notifyWhenFundsReceived;
	private PaymentSource paymentSource;
	private List<AcceptedContractualTerms> acceptedContractualTerms;
	private List<Link> links;

	public DepositInstruction() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MoneyContract getAmount() {
		return amount;
	}

	public void setAmount(MoneyContract amount) {
		this.amount = amount;
	}

	public DepositMethod getMethod() {
		return method;
	}

	public void setMethod(DepositMethod method) {
		this.method = method;
	}

	public String getSourceHierarchyId() {
		return sourceHierarchyId;
	}

	public void setSourceHierarchyId(String sourceHierarchyId) {
		this.sourceHierarchyId = sourceHierarchyId;
	}

	public InstructionStatus getStatus() {
		return status;
	}

	public void setStatus(InstructionStatus status) {
		this.status = status;
	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public Boolean getTaxRelief() {
		return taxRelief;
	}

	public void setTaxRelief(Boolean taxRelief) {
		this.taxRelief = taxRelief;
	}

	public Long getBankAccountId() {
		return bankAccountId;
	}

	public void setBankAccountId(Long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}

	public BankAccountType getBankAccountIdType() {
		return bankAccountIdType;
	}

	public void setBankAccountIdType(BankAccountType bankAccountIdType) {
		this.bankAccountIdType = bankAccountIdType;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Boolean getNotifyWhenFundsReceived() {
		return notifyWhenFundsReceived;
	}

	public void setNotifyWhenFundsReceived(Boolean notifyWhenFundsReceived) {
		this.notifyWhenFundsReceived = notifyWhenFundsReceived;
	}

	public PaymentSource getPaymentSource() {
		return paymentSource;
	}

	public void setPaymentSource(PaymentSource paymentSource) {
		this.paymentSource = paymentSource;
	}

	public List<AcceptedContractualTerms> getAcceptedContractualTerms() {
		return acceptedContractualTerms;
	}

	public void setAcceptedContractualTerms(List<AcceptedContractualTerms> acceptedContractualTerms) {
		this.acceptedContractualTerms = acceptedContractualTerms;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "DepositInstruction [id=" + id + ", amount=" + amount + ", method=" + method + ", sourceHierarchyId="
				+ sourceHierarchyId + ", status=" + status + ", hierarchyId=" + hierarchyId + ", taxRelief=" + taxRelief
				+ ", bankAccountId=" + bankAccountId + ", bankAccountIdType=" + bankAccountIdType + ", paymentDate="
				+ paymentDate + ", notifyWhenFundsReceived=" + notifyWhenFundsReceived + ", paymentSource="
				+ paymentSource + ", acceptedContractualTerms=" + acceptedContractualTerms + ", links=" + links + "]";
	}

}
