package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubAccountHolding {

	private String hierarchyId;
	private Double quantity;
	private Double availableQuantity;
	private Long productId;
	private String productCode;
	private MoneyContract marketValue;
	private List<Link> links;

	public SubAccountHolding() {

	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getAvailableQuantity() {
		return availableQuantity;
	}

	public void setAvailableQuantity(Double availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public MoneyContract getMarketValue() {
		return marketValue;
	}

	public void setMarketValue(MoneyContract marketValue) {
		this.marketValue = marketValue;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "SubAccountHolding [hierarchyId=" + hierarchyId + ", quantity=" + quantity + ", availableQuantity="
				+ availableQuantity + ", productId=" + productId + ", productCode=" + productCode + ", marketValue="
				+ marketValue + ", links=" + links + "]";
	}

}
