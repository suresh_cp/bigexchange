package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown =true)
public class Enhanced {

	private Boolean hasProtection;
	private String certificateNumber;
	private Double entitlement;

	public Enhanced() {
		
	}
	public boolean isHasProtection() {
		return hasProtection;
	}

	public void setHasProtection(Boolean hasProtection) {
		this.hasProtection = hasProtection;
	}

	public String getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	public Double getEntitlement() {
		return entitlement;
	}

	public void setEntitlement(Double entitlement) {
		this.entitlement = entitlement;
	}

	@Override
	public String toString() {
		return "Enhanced [hasProtection=" + hasProtection + ", certificateNumber=" + certificateNumber
				+ ", entitlement=" + entitlement + "]";
	}

}
