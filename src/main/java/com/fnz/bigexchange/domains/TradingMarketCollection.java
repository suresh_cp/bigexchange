package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TradingMarketCollection {

	private List<Market> results;
	private List<Link> links;

	public List<Market> getResults() {
		return results;
	}

	public void setResults(List<Market> results) {
		this.results = results;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "TradingMarketCollection [results=" + results + ", links=" + links + "]";
	}

}
