package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.AccountType;
import com.fnz.bigexchange.enums.InstructionStatus;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountInstruction {

	private Long workItemId;
	private String hierarchyId;
	private InstructionStatus status;
	private AccountType type;
	private String parentHierarchyId;
	private NameSubAccountInstruction accountName; // instead of account name changed to SATI same domain
	private String externalAccountId;
	private AccountRegistrationProperties accountRegistrationProperties;
	private List<AcceptedContractualTerms> acceptedContractualTerms;
	private List<Link> links;

	public AccountInstruction() {

	}

	public Long getWorkItemId() {
		return workItemId;
	}

	public void setWorkItemId(Long workItemId) {
		this.workItemId = workItemId;
	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public InstructionStatus getStatus() {
		return status;
	}

	public void setStatus(InstructionStatus status) {
		this.status = status;
	}

	public AccountType getType() {
		return type;
	}

	public void setType(AccountType type) {
		this.type = type;
	}

	public String getParentHierarchyId() {
		return parentHierarchyId;
	}

	public void setParentHierarchyId(String parentHierarchyId) {
		this.parentHierarchyId = parentHierarchyId;
	}

	public NameSubAccountInstruction getAccountName() {
		return accountName;
	}

	public void setAccountName(NameSubAccountInstruction accountName) {
		this.accountName = accountName;
	}

	public String getExternalAccountId() {
		return externalAccountId;
	}

	public void setExternalAccountId(String externalAccountId) {
		this.externalAccountId = externalAccountId;
	}

	public AccountRegistrationProperties getAccountRegistrationPropertiesObject() {
		return accountRegistrationProperties;
	}

	public void setAccountRegistrationPropertiesObject(AccountRegistrationProperties accountRegistrationProperties) {
		this.accountRegistrationProperties = accountRegistrationProperties;
	}

	public List<AcceptedContractualTerms> getAcceptedContractualTerms() {
		return acceptedContractualTerms;
	}

	public void setAcceptedContractualTerms(List<AcceptedContractualTerms> acceptedContractualTerms) {
		this.acceptedContractualTerms = acceptedContractualTerms;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "AccountInstruction [workItemId=" + workItemId + ", hierarchyId=" + hierarchyId + ", status=" + status
				+ ", type=" + type + ", parentHierarchyId=" + parentHierarchyId + ", accountName=" + accountName
				+ ", externalAccountId=" + externalAccountId + ", accountRegistrationProperties="
				+ accountRegistrationProperties + ", acceptedContractualTerms=" + acceptedContractualTerms + ", links="
				+ links + "]";
	}

}
