package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.ThreadType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Thread {

	private Long Id;
	private String Title;
	private Boolean IsRead;
	private Boolean IsReplied;
	private Date LastMessageOn;
	private ThreadType Type;
	private String Category;
	private String HierarchyId;
	private Long AlertDefinitionId;
	private List<Link> links;

	public Thread() {

	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public Boolean getIsRead() {
		return IsRead;
	}

	public void setIsRead(Boolean isRead) {
		IsRead = isRead;
	}

	public Boolean getIsReplied() {
		return IsReplied;
	}

	public void setIsReplied(Boolean isReplied) {
		IsReplied = isReplied;
	}

	public Date getLastMessageOn() {
		return LastMessageOn;
	}

	public void setLastMessageOn(Date lastMessageOn) {
		LastMessageOn = lastMessageOn;
	}

	public ThreadType getType() {
		return Type;
	}

	public void setType(ThreadType type) {
		Type = type;
	}

	public String getCategory() {
		return Category;
	}

	public void setCategory(String category) {
		Category = category;
	}

	public String getHierarchyId() {
		return HierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		HierarchyId = hierarchyId;
	}

	public Long getAlertDefinitionId() {
		return AlertDefinitionId;
	}

	public void setAlertDefinitionId(Long alertDefinitionId) {
		AlertDefinitionId = alertDefinitionId;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "Thread [Id=" + Id + ", Title=" + Title + ", IsRead=" + IsRead + ", IsReplied=" + IsReplied
				+ ", LastMessageOn=" + LastMessageOn + ", Type=" + Type + ", Category=" + Category + ", HierarchyId="
				+ HierarchyId + ", AlertDefinitionId=" + AlertDefinitionId + ", links=" + links + "]";
	}

}
