package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountRegistrationProperties {

	private String taxAccounting;
	private String incomePayment;

	public AccountRegistrationProperties() {

	}

	public String getTaxAccounting() {
		return taxAccounting;
	}

	public void setTaxAccounting(String taxAccounting) {
		this.taxAccounting = taxAccounting;
	}

	public String getIncomePayment() {
		return incomePayment;
	}

	public void setIncomePayment(String incomePayment) {
		this.incomePayment = incomePayment;
	}

	@Override
	public String toString() {
		return "AccountRegistrationProperties [taxAccounting=" + taxAccounting + ", incomePayment=" + incomePayment
				+ "]";
	}
}
