package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductValuation {

	private Long productId;
	private String productCode;
	private MoneyContract unitPrice;
	private String label;
	private Double allocation;
	private MoneyContract value;
	private List<Link> links;

	public ProductValuation() {

	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public MoneyContract getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(MoneyContract unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Double getAllocation() {
		return allocation;
	}

	public void setAllocation(Double allocation) {
		this.allocation = allocation;
	}

	public MoneyContract getValue() {
		return value;
	}

	public void setValue(MoneyContract value) {
		this.value = value;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "ProductValuation [productId=" + productId + ", productCode=" + productCode + ", unitPrice=" + unitPrice
				+ ", label=" + label + ", allocation=" + allocation + ", value=" + value + ", links=" + links + "]";
	}

}
