package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PortfolioPerformance {

	private MoneyContract paymentsIn;
	private MoneyContract paymentsOut;
	private MoneyContract inSpecieTransfersIn;
	private MoneyContract inSpecieTransfersOut;
	private MoneyContract openingValue;
	private MoneyContract closingValue;
	private MoneyContract netGain;
	private MoneyContract realisedGain;
	private MoneyContract unrealisedGain;
	private MoneyContract grossInterest;
	private MoneyContract netInterest;
	private MoneyContract changeInAccruedInterest;
	private MoneyContract grossDividends;
	private MoneyContract netDividends;
	private Double netReturn;
	private Double grossReturn;
	private MoneyContract tax;
	private MoneyContract charges;
	private MoneyContract rebates;
	private List<Link> links;

	public PortfolioPerformance() {

	}

	public MoneyContract getPaymentsIn() {
		return paymentsIn;
	}

	public void setPaymentsIn(MoneyContract paymentsIn) {
		this.paymentsIn = paymentsIn;
	}

	public MoneyContract getPaymentsOut() {
		return paymentsOut;
	}

	public void setPaymentsOut(MoneyContract paymentsOut) {
		this.paymentsOut = paymentsOut;
	}

	public MoneyContract getInSpecieTransfersIn() {
		return inSpecieTransfersIn;
	}

	public void setInSpecieTransfersIn(MoneyContract inSpecieTransfersIn) {
		this.inSpecieTransfersIn = inSpecieTransfersIn;
	}

	public MoneyContract getInSpecieTransfersOut() {
		return inSpecieTransfersOut;
	}

	public void setInSpecieTransfersOut(MoneyContract inSpecieTransfersOut) {
		this.inSpecieTransfersOut = inSpecieTransfersOut;
	}

	public MoneyContract getOpeningValue() {
		return openingValue;
	}

	public void setOpeningValue(MoneyContract openingValue) {
		this.openingValue = openingValue;
	}

	public MoneyContract getClosingValue() {
		return closingValue;
	}

	public void setClosingValue(MoneyContract closingValue) {
		this.closingValue = closingValue;
	}

	public MoneyContract getNetGain() {
		return netGain;
	}

	public void setNetGain(MoneyContract netGain) {
		this.netGain = netGain;
	}

	public MoneyContract getRealisedGain() {
		return realisedGain;
	}

	public void setRealisedGain(MoneyContract realisedGain) {
		this.realisedGain = realisedGain;
	}

	public MoneyContract getUnrealisedGain() {
		return unrealisedGain;
	}

	public void setUnrealisedGain(MoneyContract unrealisedGain) {
		this.unrealisedGain = unrealisedGain;
	}

	public MoneyContract getGrossInterest() {
		return grossInterest;
	}

	public void setGrossInterest(MoneyContract grossInterest) {
		this.grossInterest = grossInterest;
	}

	public MoneyContract getNetInterest() {
		return netInterest;
	}

	public void setNetInterest(MoneyContract netInterest) {
		this.netInterest = netInterest;
	}

	public MoneyContract getChangeInAccruedInterest() {
		return changeInAccruedInterest;
	}

	public void setChangeInAccruedInterest(MoneyContract changeInAccruedInterest) {
		this.changeInAccruedInterest = changeInAccruedInterest;
	}

	public MoneyContract getGrossDividends() {
		return grossDividends;
	}

	public void setGrossDividends(MoneyContract grossDividends) {
		this.grossDividends = grossDividends;
	}

	public MoneyContract getNetDividends() {
		return netDividends;
	}

	public void setNetDividends(MoneyContract netDividends) {
		this.netDividends = netDividends;
	}

	public Double getNetReturn() {
		return netReturn;
	}

	public void setNetReturn(Double netReturn) {
		this.netReturn = netReturn;
	}

	public Double getGrossReturn() {
		return grossReturn;
	}

	public void setGrossReturn(Double grossReturn) {
		this.grossReturn = grossReturn;
	}

	public MoneyContract getTax() {
		return tax;
	}

	public void setTax(MoneyContract tax) {
		this.tax = tax;
	}

	public MoneyContract getCharges() {
		return charges;
	}

	public void setCharges(MoneyContract charges) {
		this.charges = charges;
	}

	public MoneyContract getRebates() {
		return rebates;
	}

	public void setRebates(MoneyContract rebates) {
		this.rebates = rebates;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "PortfolioPerformance [paymentsIn=" + paymentsIn + ", paymentsOut=" + paymentsOut
				+ ", inSpecieTransfersIn=" + inSpecieTransfersIn + ", inSpecieTransfersOut=" + inSpecieTransfersOut
				+ ", openingValue=" + openingValue + ", closingValue=" + closingValue + ", netGain=" + netGain
				+ ", realisedGain=" + realisedGain + ", unrealisedGain=" + unrealisedGain + ", grossInterest="
				+ grossInterest + ", netInterest=" + netInterest + ", changeInAccruedInterest="
				+ changeInAccruedInterest + ", grossDividends=" + grossDividends + ", netDividends=" + netDividends
				+ ", netReturn=" + netReturn + ", grossReturn=" + grossReturn + ", tax=" + tax + ", charges=" + charges
				+ ", rebates=" + rebates + ", links=" + links + "]";
	}

}
