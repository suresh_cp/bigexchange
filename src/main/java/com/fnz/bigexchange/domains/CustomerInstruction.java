package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.InstructionStatus;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerInstruction {

	private Long workItemId;
	private String hierarchyId;
	private InstructionStatus status;
	private PersonalDetails personalDetails;
	private CustomerContactDetails contactDetails;
	private String externalCustomerId;
	private String externalUserId;
	private String userId;
	private Boolean antiMoneyLaunderingVetted;
	private AmlDetails amlDetails;
	private Boolean createUser;
	private String parentHierarchyid;
	private TaxRegulationDetails taxRegulationDetails;
	private EmploymentDetails employmentDetails;
	private PensionProtection pensionProtection;

	public CustomerInstruction() {

	}

	public Long getWorkItemId() {
		return workItemId;
	}

	public void setWorkItemId(Long workItemId) {
		this.workItemId = workItemId;
	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public InstructionStatus getStatus() {
		return status;
	}

	public void setStatus(InstructionStatus status) {
		this.status = status;
	}

	public PersonalDetails getPersonalDetails() {
		return personalDetails;
	}

	public void setPersonalDetails(PersonalDetails personalDetails) {
		this.personalDetails = personalDetails;
	}

	public CustomerContactDetails getContactDetails() {
		return contactDetails;
	}

	public void setContactDetails(CustomerContactDetails contactDetails) {
		this.contactDetails = contactDetails;
	}

	public String getExternalCustomerId() {
		return externalCustomerId;
	}

	public void setExternalCustomerId(String externalCustomerId) {
		this.externalCustomerId = externalCustomerId;
	}

	public String getExternalUserId() {
		return externalUserId;
	}

	public void setExternalUserId(String externalUserId) {
		this.externalUserId = externalUserId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public boolean isAntiMoneyLaunderingVetted() {
		return antiMoneyLaunderingVetted;
	}

	public void setAntiMoneyLaunderingVetted(Boolean antiMoneyLaunderingVetted) {
		this.antiMoneyLaunderingVetted = antiMoneyLaunderingVetted;
	}

	public AmlDetails getAmlDetails() {
		return amlDetails;
	}

	public void setAmlDetails(AmlDetails amlDetails) {
		this.amlDetails = amlDetails;
	}

	public boolean isCreateUser() {
		return createUser;
	}

	public void setCreateUser(Boolean createUser) {
		this.createUser = createUser;
	}

	public String getParentHierarchyid() {
		return parentHierarchyid;
	}

	public void setParentHierarchyid(String parentHierarchyid) {
		this.parentHierarchyid = parentHierarchyid;
	}

	public TaxRegulationDetails getTaxRegulationDetails() {
		return taxRegulationDetails;
	}

	public void setTaxRegulationDetails(TaxRegulationDetails taxRegulationDetails) {
		this.taxRegulationDetails = taxRegulationDetails;
	}

	public EmploymentDetails getEmploymentDetails() {
		return employmentDetails;
	}

	public void setEmploymentDetails(EmploymentDetails employmentDetails) {
		this.employmentDetails = employmentDetails;
	}

	public PensionProtection getPensionProtection() {
		return pensionProtection;
	}

	public void setPensionProtection(PensionProtection pensionProtection) {
		this.pensionProtection = pensionProtection;
	}

	@Override
	public String toString() {
		return "CustomerInstruction [workItemId=" + workItemId + ", hierarchyId=" + hierarchyId + ", status=" + status
				+ ", personalDetails=" + personalDetails + ", contactDetails=" + contactDetails
				+ ", externalCustomerId=" + externalCustomerId + ", externalUserId=" + externalUserId + ", userId="
				+ userId + ", antiMoneyLaunderingVetted=" + antiMoneyLaunderingVetted + ", amlDetails=" + amlDetails
				+ ", createUser=" + createUser + ", parentHierarchyid=" + parentHierarchyid + ", taxRegulationDetails="
				+ taxRegulationDetails + ", employmentDetails=" + employmentDetails + ", pensionProtection="
				+ pensionProtection + "]";
	}

}
