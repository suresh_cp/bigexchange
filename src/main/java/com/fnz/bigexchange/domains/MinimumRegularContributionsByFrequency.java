package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.Frequency;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MinimumRegularContributionsByFrequency {

	private MoneyContract AmountObject;
	private Frequency Frequency;
	private List<Link> Links;

	public MinimumRegularContributionsByFrequency() {

	}

	public MoneyContract getAmountObject() {
		return AmountObject;
	}

	public void setAmountObject(MoneyContract amountObject) {
		AmountObject = amountObject;
	}

	public Frequency getFrequency() {
		return Frequency;
	}

	public void setFrequency(Frequency frequency) {
		Frequency = frequency;
	}

	public List<Link> getLinks() {
		return Links;
	}

	public void setLinks(List<Link> links) {
		Links = links;
	}

	@Override
	public String toString() {
		return "MinimumRegularContributionsByFrequency [AmountObject=" + AmountObject + ", Frequency=" + Frequency
				+ ", Links=" + Links + "]";
	}

}
