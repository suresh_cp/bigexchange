package com.fnz.bigexchange.domains;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.OrderExecutionType;
import com.fnz.bigexchange.enums.OrderStatus;
import com.fnz.bigexchange.enums.OrderType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Order {

	private Long id;
	private Product product;
	private SubAccount subAccount;
	private OrderType type;
	private OrderStatus status;
	private String buyOrSell;
	private Amounts amounts;
	private Date dateCreated;
	private Date dateAuthorised;
	private Date dateCompleted;
	private Date dateCancelled;
	private OrderExecutionType executionType;

	public Order() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public SubAccount getSubAccount() {
		return subAccount;
	}

	public void setSubAccount(SubAccount subAccount) {
		this.subAccount = subAccount;
	}

	public OrderType getType() {
		return type;
	}

	public void setType(OrderType type) {
		this.type = type;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public String getBuyOrSell() {
		return buyOrSell;
	}

	public void setBuyOrSell(String buyOrSell) {
		this.buyOrSell = buyOrSell;
	}

	public Amounts getAmounts() {
		return amounts;
	}

	public void setAmounts(Amounts amounts) {
		this.amounts = amounts;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateAuthorised() {
		return dateAuthorised;
	}

	public void setDateAuthorised(Date dateAuthorised) {
		this.dateAuthorised = dateAuthorised;
	}

	public Date getDateCompleted() {
		return dateCompleted;
	}

	public void setDateCompleted(Date dateCompleted) {
		this.dateCompleted = dateCompleted;
	}

	public Date getDateCancelled() {
		return dateCancelled;
	}

	public void setDateCancelled(Date dateCancelled) {
		this.dateCancelled = dateCancelled;
	}

	public OrderExecutionType getExecutionType() {
		return executionType;
	}

	public void setExecutionType(OrderExecutionType executionType) {
		this.executionType = executionType;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", product=" + product + ", subAccount=" + subAccount + ", type=" + type
				+ ", status=" + status + ", buyOrSell=" + buyOrSell + ", amounts=" + amounts + ", dateCreated="
				+ dateCreated + ", dateAuthorised=" + dateAuthorised + ", dateCompleted=" + dateCompleted
				+ ", dateCancelled=" + dateCancelled + ", executionType=" + executionType + "]";
	}

}
