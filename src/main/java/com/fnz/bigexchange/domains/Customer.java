package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.CustomerStatus;

@JsonIgnoreProperties(ignoreUnknown = true)

public class Customer {

	private Long customerId;
	private CustomerStatus status;
	private PersonalDetails personalDetails;
	private String externalCustomerId;
	private String hierarchyId;
	private Long userId;
	private List<Link> links;

	public Customer() {

	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public CustomerStatus getStatus() {
		return status;
	}

	public void setStatus(CustomerStatus status) {
		this.status = status;
	}

	public PersonalDetails getPersonalDetails() {
		return personalDetails;
	}

	public void setPersonalDetails(PersonalDetails personalDetails) {
		this.personalDetails = personalDetails;
	}

	public String getExternalCustomerId() {
		return externalCustomerId;
	}

	public void setExternalCustomerId(String externalCustomerId) {
		this.externalCustomerId = externalCustomerId;
	}

	public String getHierarchyId() {
		return hierarchyId;
	}

	public void setHierarchyId(String hierarchyId) {
		this.hierarchyId = hierarchyId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", status=" + status + ", personalDetails=" + personalDetails
				+ ", externalCustomerId=" + externalCustomerId + ", hierarchyId=" + hierarchyId + ", userId=" + userId
				+ ", links=" + links + "]";
	}

}
