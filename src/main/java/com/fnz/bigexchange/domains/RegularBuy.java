package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegularBuy {

	private Long id;
	private RegularBuyDeposit payment;
	private List<Allocations> allocations;
	private List<Link> links;

	public RegularBuy() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RegularBuyDeposit getPayment() {
		return payment;
	}

	public void setPayment(RegularBuyDeposit payment) {
		this.payment = payment;
	}

	public List<Allocations> getAllocations() {
		return allocations;
	}

	public void setAllocations(List<Allocations> allocations) {
		this.allocations = allocations;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "RegularBuy [id=" + id + ", payment=" + payment + ", allocations=" + allocations + ", links=" + links
				+ "]";
	}

}
