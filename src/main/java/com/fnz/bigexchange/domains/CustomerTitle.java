package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerTitle {

	private Long Id;
	private String Name;
	List<Link> Links;

	public CustomerTitle() {

	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public List<Link> getLinks() {
		return Links;
	}

	public void setLinks(List<Link> links) {
		Links = links;
	}

	@Override
	public String toString() {
		return "CustomerTitle [Id=" + Id + ", Name=" + Name + ", Links=" + Links + "]";
	}

}
