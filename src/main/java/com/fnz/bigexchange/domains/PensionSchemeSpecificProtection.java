package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PensionSchemeSpecificProtection {

	private Boolean hasProtection;
	private Boolean isBlockTransferProtected;
	private Long protectedRetirementAge;
	private Double aDayFundValue;
	private Double PCLSEntitlement;

	public PensionSchemeSpecificProtection() {

	}

	public Boolean isHasProtection() {
		return hasProtection;
	}

	public void setHasProtection(Boolean hasProtection) {
		this.hasProtection = hasProtection;
	}

	public Boolean isBlockTransferProtected() {
		return isBlockTransferProtected;
	}

	public void setBlockTransferProtected(Boolean isBlockTransferProtected) {
		this.isBlockTransferProtected = isBlockTransferProtected;
	}

	public Long getProtectedRetirementAge() {
		return protectedRetirementAge;
	}

	public void setProtectedRetirementAge(Long protectedRetirementAge) {
		this.protectedRetirementAge = protectedRetirementAge;
	}

	public Double getaDayFundValue() {
		return aDayFundValue;
	}

	public void setaDayFundValue(Double aDayFundValue) {
		this.aDayFundValue = aDayFundValue;
	}

	public Double getPCLSEntitlement() {
		return PCLSEntitlement;
	}

	public void setPCLSEntitlement(Double pCLSEntitlement) {
		PCLSEntitlement = pCLSEntitlement;
	}

	@Override
	public String toString() {
		return "PensionSchemeSpecificProtection [hasProtection=" + hasProtection + ", isBlockTransferProtected="
				+ isBlockTransferProtected + ", protectedRetirementAge=" + protectedRetirementAge + ", aDayFundValue="
				+ aDayFundValue + ", PCLSEntitlement=" + PCLSEntitlement + "]";
	}

}
