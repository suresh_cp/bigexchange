package com.fnz.bigexchange.domains;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.MarketStatus;
import com.fnz.bigexchange.enums.ProductPriceType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LatestProductUnitPriceCollection {

	private Long productId;
	private MoneyContract price; // price <monetcontract>
	private Price bid;
	private Price ask;
	private Price mid;
	private Price lastClose;
	private Date requestedAt;
	private Date date;
	private Date lastUpdated;
	private ProductPriceType priceType;
	private MarketStatus marketStatus;

	public LatestProductUnitPriceCollection() {

	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public MoneyContract getPrice() {
		return price;
	}

	public void setPrice(MoneyContract price) {
		this.price = price;
	}

	public Price getBid() {
		return bid;
	}

	public void setBid(Price bid) {
		this.bid = bid;
	}

	public Price getAsk() {
		return ask;
	}

	public void setAsk(Price ask) {
		this.ask = ask;
	}

	public Price getMid() {
		return mid;
	}

	public void setMid(Price mid) {
		this.mid = mid;
	}

	public Price getLastClose() {
		return lastClose;
	}

	public void setLastClose(Price lastClose) {
		this.lastClose = lastClose;
	}

	public Date getRequestedAt() {
		return requestedAt;
	}

	public void setRequestedAt(Date requestedAt) {
		this.requestedAt = requestedAt;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public ProductPriceType getPriceType() {
		return priceType;
	}

	public void setPriceType(ProductPriceType priceType) {
		this.priceType = priceType;
	}

	public MarketStatus getMarketStatus() {
		return marketStatus;
	}

	public void setMarketStatus(MarketStatus marketStatus) {
		this.marketStatus = marketStatus;
	}

	@Override
	public String toString() {
		return "LatestProductUnitPriceCollection [productId=" + productId + ", price=" + price + ", bid=" + bid
				+ ", ask=" + ask + ", mid=" + mid + ", lastClose=" + lastClose + ", requestedAt=" + requestedAt
				+ ", date=" + date + ", lastUpdated=" + lastUpdated + ", priceType=" + priceType + ", marketStatus="
				+ marketStatus + "]";
	}

}
