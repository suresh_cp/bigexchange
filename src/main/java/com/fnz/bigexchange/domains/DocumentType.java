package com.fnz.bigexchange.domains;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.DocumentOriginType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DocumentType {

	private Long id;
	private String name;
	private DocumentOriginType documentOriginType;
	private List<Link> links;

	public DocumentType() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DocumentOriginType getDocumentOriginType() {
		return documentOriginType;
	}

	public void setDocumentOriginType(DocumentOriginType documentOriginType) {
		this.documentOriginType = documentOriginType;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "DocumentType [id=" + id + ", name=" + name + ", documentOriginType=" + documentOriginType + ", links="
				+ links + "]";
	}

}
