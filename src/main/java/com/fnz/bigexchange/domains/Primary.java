package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Primary {
	private Boolean hasProtection;
	private String certificateNumber;
	private Double enhancementFactor;
	private Double protectionAmount;

	public Primary() {

	}

	public Boolean isHasProtection() {
		return hasProtection;
	}

	public void setHasProtection(Boolean hasProtection) {
		this.hasProtection = hasProtection;
	}

	public String getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	public Double getEnhancementFactor() {
		return enhancementFactor;
	}

	public void setEnhancementFactor(Double enhancementFactor) {
		this.enhancementFactor = enhancementFactor;
	}

	public Double getProtectionAmount() {
		return protectionAmount;
	}

	public void setProtectionAmount(Double protectionAmount) {
		this.protectionAmount = protectionAmount;
	}

	@Override
	public String toString() {
		return "Primary [hasProtection=" + hasProtection + ", certificateNumber=" + certificateNumber
				+ ", enhancementFactor=" + enhancementFactor + ", protectionAmount=" + protectionAmount + "]";
	}

}
