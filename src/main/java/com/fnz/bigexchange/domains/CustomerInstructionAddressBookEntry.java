package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.AddressType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerInstructionAddressBookEntry {

	private Long addressBookEntryId;
	private AddressType type;
	private Address address;

	public CustomerInstructionAddressBookEntry() {

	}

	public Long getAddressBookEntryId() {
		return addressBookEntryId;
	}

	public void setAddressBookEntryId(Long addressBookEntryId) {
		this.addressBookEntryId = addressBookEntryId;
	}

	public AddressType getType() {
		return type;
	}

	public void setType(AddressType type) {
		this.type = type;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "CustomerInstructionAddressBookEntry [addressBookEntryId=" + addressBookEntryId + ", type=" + type
				+ ", address=" + address + "]";
	}

}
