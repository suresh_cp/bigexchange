package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.CountryCode;
import com.fnz.bigexchange.enums.Gender;
import com.fnz.bigexchange.enums.MaritalStatus;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerPersonalDetails {

	private Name name;
	private MaritalStatus maritalStatus;
	private Gender gender;
	private List<GovernmentIdentifier> governmentIdentifiers;
	private Date dateOfBirth;
	private CountryCode nationality;
	private CountryCode countryOfResidency;
	private String townOfBirth;
	private List<CustomerCountry> countries;
	CustomerIdentifiers customerIdentifiers;
	private List<Link> links;

	public CustomerPersonalDetails() {

	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public MaritalStatus getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(MaritalStatus maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public List<GovernmentIdentifier> getGovernmentIdentifiers() {
		return governmentIdentifiers;
	}

	public void setGovernmentIdentifiers(List<GovernmentIdentifier> governmentIdentifiers) {
		this.governmentIdentifiers = governmentIdentifiers;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public CountryCode getNationality() {
		return nationality;
	}

	public void setNationality(CountryCode nationality) {
		this.nationality = nationality;
	}

	public CountryCode getCountryOfResidency() {
		return countryOfResidency;
	}

	public void setCountryOfResidency(CountryCode countryOfResidency) {
		this.countryOfResidency = countryOfResidency;
	}

	public String getTownOfBirth() {
		return townOfBirth;
	}

	public void setTownOfBirth(String townOfBirth) {
		this.townOfBirth = townOfBirth;
	}

	public List<CustomerCountry> getCountries() {
		return countries;
	}

	public void setCountries(List<CustomerCountry> countries) {
		this.countries = countries;
	}

	public CustomerIdentifiers getCustomerIdentifiers() {
		return customerIdentifiers;
	}

	public void setCustomerIdentifiers(CustomerIdentifiers customerIdentifiers) {
		this.customerIdentifiers = customerIdentifiers;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "CustomerPersonalDetails [name=" + name + ", maritalStatus=" + maritalStatus + ", gender=" + gender
				+ ", governmentIdentifiers=" + governmentIdentifiers + ", dateOfBirth=" + dateOfBirth + ", nationality="
				+ nationality + ", countryOfResidency=" + countryOfResidency + ", townOfBirth=" + townOfBirth
				+ ", countries=" + countries + ", customerIdentifiers=" + customerIdentifiers + ", links=" + links
				+ "]";
	}

}
