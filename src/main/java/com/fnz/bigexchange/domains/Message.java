package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Message {

	private Long id;
	private String body;
	private User sender;
	private Date sentOn;
	private String Category;
	private Boolean IsRead;
	private List<Integer> attachedDocumentIds;;
	private List<Link> links;

	public Message() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public Date getSentOn() {
		return sentOn;
	}

	public void setSentOn(Date sentOn) {
		this.sentOn = sentOn;
	}

	public String getCategory() {
		return Category;
	}

	public void setCategory(String category) {
		Category = category;
	}

	public Boolean getIsRead() {
		return IsRead;
	}

	public void setIsRead(Boolean isRead) {
		IsRead = isRead;
	}

	public List<Integer> getAttachedDocumentIds() {
		return attachedDocumentIds;
	}

	public void setAttachedDocumentIds(List<Integer> attachedDocumentIds) {
		this.attachedDocumentIds = attachedDocumentIds;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "Message [id=" + id + ", body=" + body + ", sender=" + sender + ", sentOn=" + sentOn + ", Category="
				+ Category + ", IsRead=" + IsRead + ", attachedDocumentIds=" + attachedDocumentIds + ", links=" + links
				+ "]";
	}

}
