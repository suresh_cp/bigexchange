package com.fnz.bigexchange.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.RatingType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FundRating {

	private Long id;
	private RatingType type;
	private String label;
	private Long ratingValue;

	public FundRating() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RatingType getType() {
		return type;
	}

	public void setType(RatingType type) {
		this.type = type;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Long getRatingValue() {
		return ratingValue;
	}

	public void setRatingValue(Long ratingValue) {
		this.ratingValue = ratingValue;
	}

	@Override
	public String toString() {
		return "FundRating [id=" + id + ", type=" + type + ", label=" + label + ", ratingValue=" + ratingValue + "]";
	}

}
