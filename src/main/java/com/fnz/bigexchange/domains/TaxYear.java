package com.fnz.bigexchange.domains;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.CountryCode;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TaxYear {

	private CountryCode countryCode;
	private Date startDate;
	private Date endDate;

	public TaxYear() {

	}

	public CountryCode getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(CountryCode countryCode) {
		this.countryCode = countryCode;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "TaxYear [countryCode=" + countryCode + ", startDate=" + startDate + ", endDate=" + endDate + "]";
	}

}
