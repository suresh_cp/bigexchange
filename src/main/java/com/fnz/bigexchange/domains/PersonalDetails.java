package com.fnz.bigexchange.domains;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fnz.bigexchange.enums.CountryCode;
import com.fnz.bigexchange.enums.Gender;
import com.fnz.bigexchange.enums.MaritalStatus;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonalDetails {

	private Name name;
	private MaritalStatus maritalStatus;
	private Gender gender;
	private List<GovernmentIdentifier> governmentIdentifiers;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date dateOfBirth;
	private CountryCode nationality;
	private CountryCode countryOfResidency;
	private String townOfBirth;
	private List<CustomerCountry> countries;
	private CustomerIdentifiers customerIdentifiers;
	private List<Link> links;

	public PersonalDetails() {

	}
	
	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public MaritalStatus getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(MaritalStatus maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public List<GovernmentIdentifier> getGovernmentIdentifiers() {
		return governmentIdentifiers;
	}

	public void setGovernmentIdentifiers(List<GovernmentIdentifier> governmentIdentifiers) {
		this.governmentIdentifiers = governmentIdentifiers;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public CountryCode getNationality() {
		return nationality;
	}

	public void setNationality(CountryCode nationality) {
		this.nationality = nationality;
	}

	public CountryCode getCountryOfResidency() {
		return countryOfResidency;
	}

	public void setCountryOfResidency(CountryCode countryOfResidency) {
		this.countryOfResidency = countryOfResidency;
	}

	public String getTownOfBirth() {
		return townOfBirth;
	}

	public void setTownOfBirth(String townOfBirth) {
		this.townOfBirth = townOfBirth;
	}

	public List<CustomerCountry> getCountries() {
		return countries;
	}

	public void setCountries(List<CustomerCountry> customerCountry) {
		this.countries = customerCountry;
	}

	public CustomerIdentifiers getCustomerIdentifiers() {
		return customerIdentifiers;
	}

	public void setCustomerIdentifiers(CustomerIdentifiers customerIdentifiers) {
		this.customerIdentifiers = customerIdentifiers;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "PersonalDetails [name=" + name + ", maritalStatus=" + maritalStatus + ", gender=" + gender
				+ ", governmentIdentifiers=" + governmentIdentifiers + ", dateOfBirth=" + dateOfBirth + ", nationality="
				+ nationality + ", countryOfResidency=" + countryOfResidency + ", townOfBirth=" + townOfBirth
				+ ", Countries=" + countries + ", customerIdentifiersObject=" + customerIdentifiers + ", links="
				+ links + "]";
	}

}
